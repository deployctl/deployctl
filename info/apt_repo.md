## [Hosting a simple APT repository on CentOS](http://troubleshootingrange.blogspot.sg/2012/09/hosting-simple-apt-repository-on-centos.html)



This page describes how to host a custom apt repository on a CentOS or RHEL box. For instance, maybe you want to build some custom packages and make them available to both 32 and 64 bit Debian hosts in your network. Why would you host this on a CentOS box in the first place? Maybe your company hosts repos for multiple distros and your server of choice for hosting all of them happens to be CentOS / RHEL. Please note, I'm not talking about hosting a mirror of an official repo, just a simple repo for custom packages inside your organisation. This howto doesn't nearly cover the complexities of a full repo, just enough to get up and running.

A lot of documentation out there refers to 'reprepro' which looks like it does a lot for you, but is mostly made available for Debian servers only. There are no doubt rpm versions but I like to stick to what's in the official repos or EPEL. This method uses packages available in EPEL with a single shell script to do the tricky stuff which is actually not that tricky. Once you know what you're doing, creating and managing a basic repo is fairly straightforward.

For this example I'm going to create a repo with both i386 and amd64 packages. I'm creating a single repository called "stable" which all distros will pull from, but you can call it whatever you want. You may also want separate ones for squeeze, lenny, etch, etc. depending on what you're putting in there.

Blue text with a # in front indicates something to run on the command line.

### Part 1: CentOS packages for managing apt repositories

Install the following from EPEL using yum
```
dpkg
dpkg-devel
gnupg2 (probably already installed)
```

### Part 2: The filesystem

Create the following:
```
# mkdir -p /var/www/apt/pool/main
# mkdir -p /var/www/apt/dists/stable/main/binary-i386
# mkdir -p /var/www/apt/dists/stable/main/binary-amd64
```

### Part 3: The packages

Drop every single package you want to host into /var/www/apt/pool/main/

That's right, all i386, amd64 and "any" packages go into a single directory (you can split into subdirectories if you really want) and we'll run dpkg-scanpackages (part of dpkg-devel) which then finds all the relevant deb packages in the pool and generates separate index files for the two architectures called Packages which is then also zipped up.

### Part 4: Generate GPG key

If you skip this step then you will get the following warning when trying to install a package:
WARNING: The following packages cannot be authenticated!
You will also have to type in 'y' when prompted which causes problems with automated installs. There is a flag to skip authenticity checks but it's not hard to do it properly.

# gpg --gen-key
(Fill in in a nice name for your repo when prompted, and provide an email address)

Next, get the UID:
# gpg --list-keys
Output will look similar to this. The UID is the part after the slash:

/root/.gnupg/pubring.gpg
------------------------
pub   2048R/CE123456 2012-09-02 [expires: 2022-08-31]
uid                  My Example Repository <support@example.com>

Now export the public key and import it into a key ring which can be downloaded on the clients:
# gpg --export -a CE123456 > ~/repo.key
# gpg --no-default-keyring --keyring /var/www/apt/myrepo.gpg --import ~/repo.key
# rm ~/repo.key

### Part 5: Create the Repository

This little script will pull everything together. A run-down of what it does:

Generates a dummy override file in /tmp/ which is put there purely to get rid of annoying errors while re-indexing. This can really be left out.
Runs dpkg-scanpackages which generates main/binary-(ARCH)/Packages based on packages it finds under /var/www/apt/pool/main
Zips Packages into a .gz and .bz2 format so clients can pick their favourite. Not 100% necessary to do both.
After all architectures are indexed, a main Release file is created under /var/www/apt/dists/stable/ which contains MD5 and SHA hashes of every single file inside the binary folder. This is used to verify authenticity after Release.gpg is verified.
Signs the main Release file creating Release.gpg
`/var/www/apt/reindex_stable.sh` (change value of GPG_NAME)

```
#!/bin/bash

GPG_NAME=CE123456
REPONAME=stable
VERSION=6.0

for bindir in `find dists/${REPONAME} -type d -name "binary*"`; do
arch=`echo $bindir|cut -d"-" -f 2`
echo "Processing ${bindir} with arch ${arch}"

overrides_file=/tmp/overrides
package_file=$bindir/Packages
release_file=$bindir/Release

# Create simple overrides file to stop warnings
cat /dev/null > $overrides_file
for pkg in `ls pool/main/ | grep -E "(all|${arch})\.deb"`; do
pkg_name=`/usr/bin/dpkg-deb -f pool/main/${pkg} Package`
echo "${pkg_name} Priority extra" >> $overrides_file
done

# Index of packages is written to Packages which is also zipped
dpkg-scanpackages -a ${arch} pool/main $overrides_file > $package_file
# The line above is also commonly written as:
# dpkg-scanpackages -a ${arch} pool/main /dev/null > $package_file
gzip -9c $package_file > ${package_file}.gz
bzip2 -c $package_file > ${package_file}.bz2

# Cleanup
rm $overrides_file
done

# Release info goes into Release & Release.gpg which includes an md5 & sha1 hash of Packages.*
# Generate & sign release file
cd dists/${REPONAME}
cat > Release <<ENDRELEASE
Suite: ${REPONAME}
Version: ${VERSION}
Component: main
Origin: My Company
Label: My Example Repo
Architecture: i386 amd64
Date: `date`
ENDRELEASE

# Generate hashes
echo "MD5Sum:" >> Release
for hashme in `find main -type f`; do
md5=`openssl dgst -md5 ${hashme}|cut -d" " -f 2`
size=`stat -c %s ${hashme}`
echo " ${md5} ${size} ${hashme}" >> Release
done
echo "SHA1:" >> Release
for hashme in `find main -type f`; do
sha1=`openssl dgst -sha1 ${hashme}|cut -d" " -f 2`
size=`stat -c %s ${hashme}`
echo " ${sha1} ${size} ${hashme}" >> Release
done

# Sign!
gpg --yes -u $GPG_NAME --sign -bao Release.gpg Release
cd -
```
After running this script, you should end up with a directory structure that looks a lot like this:

```
/var/www/apt/
/var/www/apt/dists
/var/www/apt/dists/stable
/var/www/apt/dists/stable/Release
/var/www/apt/dists/stable/Release.gpg
/var/www/apt/dists/stable/main
/var/www/apt/dists/stable/main/binary-i386
/var/www/apt/dists/stable/main/binary-i386/Packages.gz
/var/www/apt/dists/stable/main/binary-i386/Packages.bz2
/var/www/apt/dists/stable/main/binary-i386/Packages
/var/www/apt/dists/stable/main/binary-amd64
/var/www/apt/dists/stable/main/binary-amd64/Packages.gz
/var/www/apt/dists/stable/main/binary-amd64/Packages.bz2
/var/www/apt/dists/stable/main/binary-amd64/Packages
/var/www/apt/pool
/var/www/apt/pool/main
/var/www/apt/pool/main/mynoarchpackage_1.0.0-1_all.deb
/var/www/apt/pool/main/mypackage_1.0.0-1_i386.deb
/var/www/apt/pool/main/mypackage_1.0.0-1_amd64.deb
/var/www/apt/reindex_stable.sh
/var/www/apt/myrepo.gpg
```

### Part 6: Apache

Install apache and create the following /etc/httpd/conf.d/apt.conf and restart.
```
<VirtualHost *:80>
ServerName apt.example.com
DocumentRoot /var/www/apt
<Directory /var/www/apt>
Order allow,deny
Allow from all
Options +Indexes
</Directory>
</VirtualHost>
```

### Part 7: Client Config

The last part is to get your servers to use this repo. This involves providing a sources.list entry and the public key.
Add apt source:

    # echo "deb http://apt.example.com/ stable main" > /etc/apt/sources.list.d/myrepo.list

Get the GPG key:

    # cd /etc/apt/trusted.gpg.d/
    # wget http://apt.example.com/myrepo.gpg

Do an apt-get update and you're good to go.


