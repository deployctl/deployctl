#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "../src/error.h"

/*------------------------------------------------------------------------
 * extract the version Number
 *
 * returns 0 on valid
 *------------------------------------------------------------------------*/
int ci_extract_server_version(const char* CI_SERVER_VERSION,int *major_v,int*minor_v,int *revision_v)
{
	//setdebug();
	
	const char s[2] = ".";
	char *token;
	int count=0;
	char * str=strdup(CI_SERVER_VERSION);
	char * org__str_save=str; //save original str for free
	/* get the first token */
	debug( "\nversion str %s\n", str );
	//token = strtok(str, s);
	char * saveptr=NULL;
	/* get the first token */
	token = strtok_r(str, s,&saveptr);
	
	/* walk through other tokens */
	while( token != NULL && count<3 )
	{
		
		debug( "version token %d: %s\n",count ,token );
		char * errCheck=NULL;
		int i = (int)strtol(token, &errCheck,(10));
		
		if(errCheck != token)
			debug( "value round %d: %d\n",count,i);
		else
			debug("value round %d: --\n",count);
		
		if (count==0 && major_v)
		{
			if(errCheck != token)
				*major_v=i;
			else
				*major_v=-1;
		}
		else if (count==1&& minor_v)
		{
			if(errCheck != token)
				*minor_v=i;
			else
				*minor_v=-1;
		}
		else if (count==2&& revision_v)
		{
			if(errCheck != token)
				*revision_v=i;
			else
				*revision_v=-1;
		}
		count++;
		token = strtok_r(NULL, s,&saveptr);
	}
	if (org__str_save)free(org__str_save);
	return 0;
}

int main()
{
   char str[80] = "10.0.1";
   const char s[2] = ".";
   char *token;
   char * saveptr=NULL;
   int count=0;
   /* get the first token */
   token = strtok_r(str, s,&saveptr);
	
   /* walk through other tokens */
   while( token != NULL )
   {
      //printf( " %s\n", token );
      char * errCheck=NULL;
      int i = (int)strtol(token, &errCheck,(10));
      
      if(errCheck != token)
         printf( "%d: %d\n",count,i);
      else
         printf( "%d: --\n",count);
      count++;
      token = strtok_r(NULL, s,&saveptr);
   }
	int ma,mi,rv;
	int ress=ci_extract_server_version("10.0.0", &ma, &mi, &rv);
   
   return(0);
}