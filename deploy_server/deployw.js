/*
 deployw.js
 Created by Danny Goossen, Gioxa Ltd on 11/4/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

var last_tag_release="";
var target_deploy="";
var release_tag=0;

function bytesToSize(bytes) {
   var sizes = ['B', 'k', 'M', 'G', 'T'];
   if (bytes == 0) return '0B';
   var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
   var t=bytes / Math.pow(1024, i);
   var f= Math.floor(t);
   var g=0;
   if (f < 10) {
	   g= f+ Math.round((t-f)*10,2) / 10;
   }
   else {
	   g= Math.round(bytes / Math.pow(1024, i), 2);
   }
   return g + sizes[i];
};

var releasediv =(function() {
   var _div;
   function makerelease(data,tag) {
      var line,thistagdiv,filesdiv;
      var tagstr="";
      if (data.tag) {
         tagstr=data.tag.name;
      }
      else {
         tagstr="No Tag Data";
      }
      if (tag==0){
         thistagdiv = '<hr><div id="div_'+tagstr+'" class="latest_tag_div">';
      }
      else {
         thistagdiv = '<div id="div_'+tagstr+'">';
      }
      var latest_mark="";
      if ((last_tag_release != null) && (last_tag_release.valueOf() == tagstr.valueOf())) { latest_mark=" <small>(latest)</small>"; }
      if (data.tag) {
         if ( release_tag==1){
            thistagdiv += '<h2 class="tag_header"><a class="content-link" href="'+data.tag.link+'"><span class="glyphicon glyphicon-tag"></span> '  + tagstr + '</a>'+ latest_mark +'</h2>';
         }
         else {
            thistagdiv += '<h3 class="tag_header"><a class="content-link" href="'+data.tag.link+'">'  + tagstr + '</a>'+ latest_mark +'</h3>';
         }
      }
      if (data.deployed.login) // gitlab v10
      {
         var link="";
         link = project_info.link.replace(new RegExp(project_info.path + '$'), data.deployed.login);
         thistagdiv +='<p> Deployed by <a class="content-link" href="' + link + '"><strong>' + data.deployed.name + '</strong></a> ';
      }
      else
      { // name is GITLAB_USER_EMAIL so chop it(pre gitlab v10)
        var name=data.deployed.name.split('@')[0].split('.')[0]
        thistagdiv += '<p>Deployed by <strong>'+ name +'</strong>';
      }
      if (data.deployed.date) {
         var localDate = new Date(Date.parse(data.deployed.date));
         thistagdiv +=', <strong><time class="timeago" datetime="'+data.deployed.date+'">'+localDate+'</time></strong>';
      }

      if (data.deployed.pipeline_id) {
         thistagdiv +=', <br />with pipeline <a class="content-link" href="' + project_info.link + '/pipelines/' + data.deployed.pipeline_id + '"><strong>#' + data.deployed.pipeline_id + '</strong></a> ';
      }
      if (data.deployed.job_id) {
         thistagdiv +=' and job <a class="content-link" href="' + project_info.link + '/builds/' + data.deployed.job_id + '"><strong>#' + data.deployed.job_id + '</strong></a> ';
      }
      thistagdiv +=' for commit <a class="content-link" href="' + data.commit.link + '"><strong>#' + data.commit.name.substring(0,8) + '</strong></a>. </p> ';
      thistagdiv +='<hr class="internal_tag_div">'
      if (data.tag_info)
      {
         thistagdiv += '<p>'+ data.tag_info.message +' </p>';
         if (data.tag_info.notes_html)
         {
            thistagdiv +='<h4 class="release_sub"; >Release Notes:</h4><div class="release_notes" >' + data.tag_info.notes_html + '</div>';
         }
      }
      if (data.links)
      {
         var link_href='';
         filesdiv =  '';
         if (data.links.length>0)
         {
            filesdiv += '<h4 class="release_sub">Link'
            if (data.links.length>1){
               filesdiv += 's';
            }
            filesdiv += ': </h4>';
            filesdiv += '<div class="filelist" >';
            //filesdiv += '   <ul style="list-style-type:disc">';
            for (var i = 0; i < data.links.length; i++)
            {
               if (data.links[i].link.startsWith("http://") || data.links[i].link.startsWith("https://")){
                 link_href=data.links[i].link;
               } else {
                 link_href=download_url + data.links[i].link;
               }

               filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"> <a class="content-link" href="'+ link_href + '"> <span class="glyphicon glyphicon-link"></span> ' + data.links[i].name + ' </a></div>';
              if (data.links[i].metric) {
               filesdiv +='<div style="display: inline-block; float: right; "><p class="text-info" >'+data.links[i].metric+'</p></div>';
              }
              filesdiv +='</div>'
            }
            filesdiv += ' <hr> '; //' </ul>' ;
            filesdiv += '</div>';
         }
         thistagdiv += filesdiv;
      }
      if (data.files)
      {
         filesdiv =  '';
         if (data.files.length>0)
         {
            filesdiv += '<h4  class="release_sub">Download';
            if (data.files.length>1) {
               filesdiv += 's';
            }
            filesdiv += ': </h4>';
            filesdiv += '<div class="filelist" >';
            //filesdiv += '   <ul style="list-style-type:disc">';
            for (var i = 0; i < data.files.length; i++)
            {
              var contentview="";
            //if (data.files[i].content) {
            //	contentview= '<span class="glyphicon glyphicon-eye-open" style="margin-left: 16px; margin-top: 10px; color:#1abc9c; font-size: 16px;"></span>';
            //}
               filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"> <a class="content-link" href="'+ download_url + data.files[i].link + '"> <span class="glyphicon glyphicon-download-alt"></span>  ' + data.files[i].name + '</a>'+contentview+'</div>'
               if (data.files[i].size) {
                  filesdiv +='<div style="display: inline-block; float: right; " ><p class="text-info" >'+bytesToSize(data.files[i].size)+'</p></div>';
               }
               filesdiv +='</div><div id="content_view'+i+'"></div>';
            }
            filesdiv += ' <hr> '; //' </ul>' ;
            filesdiv += '</div>';
         }
         thistagdiv += filesdiv;
      }
      thistagdiv += '<hr></div>' ; // tag div
      return thistagdiv;
   }
   return{
      config: function( ) {
      //_div = $('#releases');
      _div = document.getElementById("releases");
      _div.innerHTML = "";
      return this;
   },
   load: function(releases) {
      _div.innerHTML += makerelease(releases[0],0);
      console.log("# of releases:" + releases.length);
      var old_tags="";
      if (releases.length > 1)
      {
      old_tags='<br><div id="showhide" class="showhidebutton"></div>';
      old_tags+='<div class="old_tag" >';
      for (var i = 1; i < releases.length; i++)
      {
      old_tags+= makerelease(releases[i],i);
      }
      _div.innerHTML +=old_tags + '</div><br>';
      }
      return this;
      },
      clear: function(){
      _div.children('tbody').empty();
      return this;
      }
   };
}());

var repodiv =(function() {
   var _div;
   function makerepo(data) {
      var line,thistagdiv,filesdiv;

	  thistagdiv='<hr class="internal_tag_div">';
              
              if (data.deployed.login) // gitlab v10
              {
              var link="";
              link = project_info.link.replace(new RegExp(project_info.path + '$'), data.deployed.login);
              thistagdiv +='<p> Deployed by <a class="content-link" href="' + link + '"><strong>' + data.deployed.name + '</strong></a> ';
              }
              else
              { // name is GITLAB_USER_EMAIL so chop it(pre gitlab v10)
              var name=data.deployed.name.split('@')[0].split('.')[0]
              thistagdiv += '<p>Deployed by <strong>'+ name +'</strong>';
              }
         
              
              
      if (data.deployed.date) {
         var localDate = new Date(Date.parse(data.deployed.date));
         thistagdiv +=', <strong><time class="timeago" datetime="'+data.deployed.date+'">'+localDate+'</time></strong>';
      }

      if (data.deployed.pipeline_id) {
         thistagdiv +=', <br />with pipeline <a class="content-link" href="' + project_info.link + '/pipelines/' + data.deployed.pipeline_id + '"><strong>#' + data.deployed.pipeline_id + '</strong></a> ';
      }
      if (data.deployed.job_id) {
         thistagdiv +=' and job <a class="content-link" href="' + project_info.link + '/builds/' + data.deployed.job_id + '"><strong>#' + data.deployed.job_id + '</strong></a> ';
      }
      thistagdiv +=' for commit <a class="content-link" href="' + data.commit.link + '"><strong>#' + data.commit.name.substring(0,8) + '</strong></a>. <p> ';
      thistagdiv +='<hr class="internal_tag_div">'
      if (data.description)
      {
            thistagdiv +='<h4 class="release_sub"; >Description:</h4><div class="release_notes" >' + data.description + '</div>';
      }
      if (data.projects)
      {
         var link_href='';
         filesdiv =  '';
         if (data.projects.length>0)
         {
            filesdiv += '<h4 class="release_sub">Autorized Project'
            if (data.projects.length>1){
               filesdiv += 's';
            }
            filesdiv += ': </h4>';
            filesdiv += '<div class="filelist" >';
            //filesdiv += '   <ul style="list-style-type:disc">';
            for (var i = 0; i < data.projects.length; i++)
            {


               filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"> <a class="content-link" href="'+ data.projects[i] + '"> <span class="glyphicon glyphicon-link"></span> ' + data.projects[i] + ' </a></div>';
              filesdiv +='</div>'
            }
            filesdiv += ' <hr> '; //' </ul>' ;
            filesdiv += '</div>';
         }
         thistagdiv += filesdiv;
      }
      if (data.environments)
      {
         var link_href='';
         filesdiv =  '';
         if (data.environments.length>0)
         {
            filesdiv += '<h4 class="release_sub">Allowed deploy-environment'
            if (data.environments.length>1){
               filesdiv += 's';
            }
            filesdiv += ': </h4>';
            filesdiv += '<div class="filelist" >';
            //filesdiv += '   <ul style="list-style-type:disc">';
            for (var i = 0; i < data.environments.length; i++)
            {
              filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"><span class="glyphicon glyphicon-asterisk"></span>  ' + data.environments[i] + ' </div>';
              filesdiv +='</div>'
            }
            filesdiv += ' <hr> '; //' </ul>' ;
            filesdiv += '</div>';
         }
         thistagdiv += filesdiv;
      }
   if (data.repos)
   {
      var link_href='';
      filesdiv =  '';
      if (data.endpoints.length>0)
      {
         filesdiv += '<h4 class="release_sub">Configured Repository'
         if (data.endpoints.length>1)
         {
           filesdiv += '\'s';
         }
         filesdiv += ': </h4>';
         filesdiv += '<div class="filelist" >';
         //filesdiv += '   <ul style="list-style-type:disc">';
         for (var i = 0; i < data.endpoints.length; i++)
         {
            for (var key in data.endpoints[i])
            {
               if (data.endpoints[i].hasOwnProperty(key))
               {
                  var endpoint=key;
                  var values=data.endpoints[i][key];
                  link_href=download_url + endpoint;
                  var output = '';
                  var item_info='';
                  //console.log(values[2] + typeof values[2])
                  if (typeof values[2] == 'string')
                  {
                    output += values[2];
                  }
                  else
                  {
                     for (var property in values[2])
                     {
                        output += property;
                        if (typeof values[2][property] == 'string')
                          item_info +=' ( ' + values[2][property] +' )';
                         else
                         {
                             var len_arr=values[2][property].length;
                             item_info = ' ( ';
                             for (var x=0;x<len_arr;x++)
                             {
                               if (x>0)item_info +=' / ';
                               item_info += values[2][property][x]
                             }
                             item_info +=' )';
                         }
                     }
                  }
                  var item_display=values[0] + ' -  ' + values[1]+ ' - ' + output;
                  filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"> <a class="content-link" href="'+ link_href + '"> <span class="glyphicon glyphicon-link"></span> '+ item_display + ' </a><small>'+item_info+'</small></div>';
                  filesdiv +='</div>'
               }
            }
         }
         filesdiv += ' <hr> '; //' </ul>' ;
         filesdiv += '</div>';
      }
      thistagdiv += filesdiv;
   }
/*
      if (data.files)
      {
         filesdiv =  '';
         if (data.files.length>0)
         {
            filesdiv += '<h4  class="release_sub">Download';
            if (data.files.length>1) {
               filesdiv += 's';
            }
            filesdiv += ': </h4>';
            filesdiv += '<div class="filelist" >';
            //filesdiv += '   <ul style="list-style-type:disc">';
            for (var i = 0; i < data.files.length; i++)
            {
              var contentview="";
            //if (data.files[i].content) {
            //	contentview= '<span class="glyphicon glyphicon-eye-open" style="margin-left: 16px; margin-top: 10px; color:#1abc9c; font-size: 16px;"></span>';
            //}
               filesdiv +='<hr><div style="display:inline-block; width:100%"><div  style="display: inline-block;float: left;"> <a class="content-link" href="'+ download_url + data.files[i].link + '"> <span class="glyphicon glyphicon-download-alt"></span>  ' + data.files[i].name + '</a>'+contentview+'</div>'
               if (data.files[i].size) {
                  filesdiv +='<div style="display: inline-block; float: right; " ><p class="text-info" >'+bytesToSize(data.files[i].size)+'</p></div>';
               }
               filesdiv +='</div><div id="content_view'+i+'"></div>';
            }
            filesdiv += ' <hr> '; //' </ul>' ;
            filesdiv += '</div>';
         }
         thistagdiv += filesdiv;
      }
	  */
      thistagdiv += '<hr></div>' ; // tag div
      return thistagdiv;
   }
   return{
      config: function( ) {
      //_div = $('#releases');
      _div = document.getElementById("releases");
      _div.innerHTML = "";
      return this;
   },
   load: function(repodata) {
      _div.innerHTML += makerepo(repodata);
      console.log("# of releases:" + releases.length);
      var old_tags="";
      return this;
      },
      clear: function(){
      _div.children('tbody').empty();
      return this;
      }
   };
}());

var button_beg = '<button id="button" class="btn btn-primary" onclick="showhide()">';
var button_end = '</button>';
//var show_button = '+', hide_button = '-';
var show_button = 'Older Releases   <span class="glyphicon glyphicon-eye-open"></span>', hide_button = 'Less Releases <span class="glyphicon glyphicon-eye-close"></span>';
function showhide() {
   $('.old_tag>div').toggleClass('displaynone');

   var showhide = document.getElementById( "showhide" );

   var hidevalue=button_beg + hide_button + button_end;

   if ( showhide.innerHTML == hidevalue ) {
      showhide.innerHTML = button_beg + show_button + button_end;
   } else {
      showhide.innerHTML = hidevalue;
   }
}
function setup_button( status ) {
   if ( status == 'show' ) {
      button = hide_button;
   } else {
      button = show_button;
   }
   var showhide = document.getElementById( "showhide" );
   showhide.innerHTML = button_beg + button + button_end;
}
function setup_project_info (project,version) {
   //var viewportWidth = $(window).width();
   document.getElementById( "title" ).innerHTML='<h1><a class="header-link" id="header" href="' + project.link +'">'+project.name+'</a></h1>';
   document.getElementById( "footer" ).innerHTML='Deployed with <a class="header-link" href="https://www.deployctl.com">www.deployctl.com</a> ('+version+')' ;
   document.getElementById( "info" ).innerHTML ='<div style="display:inline;float: left; "><h4><a class="content-link" href="' + project.link +'">'+project.path+'</a></h4></div>';
   // display latest TAG if Release or Releases, cannot for others!!!!
   if (target.indexOf("Release") != -1){
      document.getElementById( "info" ).innerHTML+='<div style="display:inline;float: right;"><h4><a class="content-link" href="/latest" ><img  src="/tag.svg"></a></h4></div>';
      release_tag=1;
      if (target.indexOf("Releases") == -1){
         // if Release, add link to releases
         document.getElementById( "info_up" ).innerHTML='<h4><a class="content-link" href="/" ><i class="glyphicon glyphicon-tags"></i> All Releases</a></h4>';

      }
   }
   if (target.indexOf("Releases") == -1){
   }
   var split_target=target.split('/');
   if (split_target[1])
   {
      document.getElementById( "target" ).innerHTML='<h2>' + split_target[0]+'<small><span style = "display: block;">'+split_target[1]+'</span></small></h2>';
   }
   else {
      document.getElementById( "target" ).innerHTML='<h2>' +target+'</h2>';
   }
}

window.onload = function () {

   try {
      target_deploy=target
   }catch(err) {
      target_deploy="";
   }

   // project info
   try {
      test=project_info;
      test=deployctl_version;
   } catch(err) {
      if (standalone) document.getElementById( "info" ).innerHTML="<h3>Failed to get project info<h3>";
   }

   try {
      if (standalone) setup_project_info(project_info,deployctl_version);
   } catch(err) {; }

   if (target_deploy==='Repository')
   { // Repository specifics
	   try { // paint the Repository
          var dt = repodiv.config();
          dt.load(RepoData);
       } catch(err) {; }
   }
   else
   { // Release(s) specifics
      try {
         last_tag_release=last_tag;
      } catch(err) { last_tag_release=""; }

      try {
         test=ReleaseData;
      } catch(err) { document.getElementById( "releases" ).innerHTML="<h3>No Release info<h3><p>Check page load errors</p>"; }

      try {
         if (target.indexOf("Release") != -1) release_tag=1;} catch(err) {; }

      try { // paint the release
         var dt = releasediv.config();
         dt.load(ReleaseData.releases);
         if ( ReleaseData.releases.length > 1){
            setup_button( 'show' );
            showhide();
         }
      } catch(err) {; }
   }
   jQuery("time.timeago").timeago();
}
