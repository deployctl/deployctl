#!/bin/bash
repo_url=@@u@@
repo_name=@@n@@
PGP_repo_check=@@p@@
PGP_repo_check=@@q@@

unknown_os ()
{
echo "Unfortunately, your operating system distribution and version are not supported by this script."
echo
echo "You can override the OS detection by setting os= and dist= prior to running this script."
echo "You can find a list of supported OSes and distributions on ${repo_url}"
echo
echo "For example, to force CentOS 6 x86_64: os=el dist=6 basearch=x86_64 ./repo.rpm.sh"
echo
exit 1
}

curl_check ()
{
   echo "Checking for curl..."
   if command -v curl > /dev/null; then
      echo "Detected curl..."
   else
      echo "Installing curl..."
      if command -v zypper > /dev/null; then
        zypper -n curl
      elif command -v dnf > /dev/null; then
        dnf install -y curl
      else
        yum install -d0 -e0 -y curl
      fi
   fi
}


detect_os ()
{
if [[ ( -z "${os}" ) && ( -z "${dist}" ) && ( -z "${basearch}" ) ]]; then
    if [ -e /etc/os-release ]; then
        . /etc/os-release
        os=${ID}
        if [ "${os}" = "centos" ]; then
              dist=`echo ${VERSION_ID}`
              os="el"
        elif [ "${os}" = "scientific" ]; then
            dist=`echo ${VERSION_ID}`
            os="el"
        elif [ "${os}" = "fedora" ]; then
              dist=`echo ${VERSION_ID}`
              os="fc"
        elif [ "${os}" = "sles" ]; then
            dist=`echo ${VERSION_ID}`
            os="sl"
        else
            dist=`echo ${VERSION_ID} | awk -F '.' '{ print $1 }'`
        fi

    elif [ `which lsb_release 2>/dev/null` ]; then
        # get major version (e.g. '5' or '6')
        dist=`lsb_release -r | cut -f2 | awk -F '.' '{ print $1 }'`

        # get os (e.g. 'centos', 'redhatenterpriseserver', etc)
        os=`lsb_release -i | cut -f2 | awk '{ print tolower($1) }'`

    elif [ -e /etc/oracle-release ]; then
        dist=`cut -f5 --delimiter=' ' /etc/oracle-release | awk -F '.' '{ print $1 }'`
        os='ol'
    elif [ -e /etc/fedora-release ]; then
        dist=`cut -f3 --delimiter=' ' /etc/fedora-release`
        os='fc'
    elif [ -e /etc/redhat-release ]; then
        os_hint=`cat /etc/redhat-release  | awk '{ print tolower($1) }'`
        if [ "${os_hint}" = "centos" ]; then
            dist=`cat /etc/redhat-release | awk '{ print $3 }' | awk -F '.' '{ print $1 }'`
            os='el'
        elif [ "${os_hint}" = "scientific" ]; then
            dist=`cat /etc/redhat-release | awk '{ print $4 }' | awk -F '.' '{ print $1 }'`
            os='el' # todo el6 ==el6 centos
        else
            dist=`cat /etc/redhat-release  | awk '{ print tolower($7) }' | cut -f1 --delimiter='.'`
            os='rh'
        fi
    else
        aws=`grep -q Amazon /etc/issue`
        if [ "$?" = "0" ]; then
            dist='6'
            os='el'
        else
            unknown_os
        fi
    fi
fi

if [[ ( -n "${os}" ) &&  ( ( "${os}" == "opensuse" ) || ( "${os}" = "sles" ) ) ]]; then
# zypper
   dist=`echo "${VERSION_ID}"`
   [[ -z "${dist}" && -e /etc/SuSE-release ]] && dist=`echo $(head -n 1 /etc/SuSE-release) | awk '{print $2}' | cut -d "(" -f2 | cut -d ")" -f1`
  # Try with zypper target os first to extract basearch
   targetos=`zypper targetos`
   [[ -z "${basearch}" ]] && basearch=${targetos##*-}
  # 13.x has empty response on zypper targetos, extract from first line of /etc/SuSE-release
   [[ -z "${basearch}" && -e /etc/SuSE-release ]] && basearch=`echo $(head -n 1 /etc/SuSE-release) | awk '{print $3}' | cut -d "(" -f2 | cut -d ")" -f1`
   osdist="${NAME}"
   # Tumbleweed has a long date stamp as VERSION_ID, so set dist to ""
   [[ ${#dist} > 5 ]] && dist=""
   [[ -n "${dist}" ]] && osdist="${osdist} ${dist}"

   # replace all " " by %20 for url
     # but test replace \%20 or %20
   test=" "
   test="${test// /\%20}"
   if [[ ${test:1:1} == "%" ]] ; then osdist="${osdist// /%20}"; else osdist="${osdist// /\%20}"; fi

   echo "osdist=${osdist}"
elif [[ ( -n "${os}" ) &&  ( "${os}" == "fc" ) ]]; then
# DNF
  [[ -z "${basearch}" ]] && basearch=`python3 -c 'import dnf; db = dnf.dnf.Base(); print(db.conf.substitutions["basearch"]);'`
  os="${os// /}"
  dist="${dist// /}"
  osdist="$os$dist"
else
# YUM
  os="${os// /}"
  dist="${dist// /}"
  [[ -z "${basearch}" ]] && basearch=`python -c 'import yum, pprint; yb = yum.YumBase(); yb.doConfigSetup(init_plugins=False); print(yb.conf.yumvar["basearch"]);'`
  osdist="$os$dist"
fi

if [[ ( -z "${osdist}" ) || ( -z "${basearch}" ) ]]; then
    unknown_os
fi

echo "Detected operating system as ${os} ${dist}, basearch: ${basearch}"
}

finalize_yum_repo ()
{

if [[ ( -n ${PGP_repo_check} && "${PGP_repo_check}" == "1" ) || ( -n ${PGP_repo_check} && "${PGP_repo_check}" == "1" ) ]]; then
 if yum -q list installed pygpgme --disablerepo="$repo_name"  &>/dev/null; then
   echo "pygpgme package already Installed"
 else
   echo "Installing pygpgme to verify GPG signatures..."
   yum install -y pygpgme --disablerepo="$repo_name"
   pypgpme_check=`rpm -qa | grep -qw pygpgme`
   if [ "$?" != "0" ]; then
      echo
      echo "WARNING: "
      echo "The pygpgme package could not be installed. This means GPG verification is not possible for any RPM installed on your system. "
      echo "To fix this, add a repository with pygpgme. Usualy, the EPEL repository for your system will have this. "
      echo "More information: https://fedoraproject.org/wiki/EPEL#How_can_I_use_these_extra_packages.3F"
      echo

    # set the repo_gpgcheck option to 0
      sed -i'' 's/repo_gpgcheck=1/repo_gpgcheck=0/' "/etc/yum.repos.d/$repo_name.repo"
   fi
 fi
fi

if yum -q list installed yum-utils --disablerepo="$repo_name"  &>/dev/null; then
  echo "yum-utils package already Installed"
else
  echo "Installing yum-utils..."
  yum install -y yum-utils --disablerepo="$repo_name"
fi
yum_utils_check=`rpm -qa | grep -qw yum-utils`
if [ "$?" != "0" ]; then
  echo
  echo "WARNING: "
  echo "The yum-utils package could not be installed. This means you may not be able to install source RPMs or use other yum features."
  echo
fi

echo "Generating yum cache for ${repo_name}..."
yum -q makecache -y --disablerepo='*' --enablerepo="$repo_name"
}

finalize_zypper_repo ()
{
zypper --gpg-auto-import-keys refresh "$repo_name"
}

main ()
{
detect_os
curl_check


yum_base_repo_url="$repo_url/rpm/$osdist/$basearch"
yum_check_repo_url="$repo_url/rpm/$osdist/$basearch/repodata/repomd.xml"

if [[ ( "${os}" == "sles" ) ||  ( "${os}" == "opensuse" ) ]]; then
  yum_repo_path="/etc/zypp/repos.d/$repo_name.repo"
else
  yum_repo_path="/etc/yum.repos.d/$repo_name.repo"
fi

[[ ( -z "${PGP_repo_check}" ) || ( "${PGP_repo_check}" == "@@p@@" ) ]] && PGP_repo_check=0
[[ ( -z "${PGP_check}" ) || ( "${PGP_check}" == "@@q@@" ) ]] && PGP_check=0

cat > $yum_repo_path <<EOF
[$repo_name]
name=$repo_name
baseurl=$yum_base_repo_url
repo_gpgcheck=$PGP_repo_check
gpgcheck=$PGP_check
enabled=1
gpgkey=$repo_url/gpgkey
sslverify=1
sslcacert=/etc/pki/tls/certs/ca-bundle.crt
metadata_expire=300
EOF


echo "Check existence of repository : ${yum_base_repo_url}"

curl --head -L -sSf "${yum_check_repo_url}" > /dev/null
curl_exit_code=$?

if [ "$curl_exit_code" = "22" ]; then
echo
echo
echo -n "Unable to reach repomd.xml from: "
echo "${yum_base_repo_url}"
echo
echo "Please check if your OS is configured"
echo "\t @ {$repo_url}"
echo "OR"
echo "Detection of your os failed:"
echo "You can override the OS detection by setting os= and dist= prior to running this script."
echo
echo "For example, to force CentOS 6: os=el dist=6 ./repo.rpm.sh"
echo
[ -e $yum_repo_path ] && rm $yum_repo_path
exit 1
elif [ "$curl_exit_code" = "35" -o "$curl_exit_code" = "60" ]; then
echo
echo "curl is unable to connect to ${repo_url} over TLS when running: "
echo "    ${yum_check_repo_url}"
echo
echo "This is usually due to one of two things:"
echo
echo " 1.) Missing CA root certificates (make sure the ca-certificates package is installed)"
echo " 2.) An old version of libssl. Try upgrading libssl on your system to a more recent version"
echo
[ -e $yum_repo_path ] && rm $yum_repo_path
exit 1
elif [ "$curl_exit_code" -gt "0" ]; then
echo
echo "Unable to run: "
echo "    curl --head -L -sSf ${yum_base_repo_url}"
echo
echo "Double check your curl installation and try again."
[ -e $yum_repo_path ] && rm $yum_repo_path
exit 1
else
echo "done."
fi

if [ "${os}" = "sl" ] || [ "${os}" = "opensuse" ]; then
finalize_zypper_repo
else
finalize_yum_repo
fi

echo
echo "The repository is setup! You can now install packages."
}

main
