//
//  debug_release.c
//  deployctl
//
//  Created by Danny Goossen on 25/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "../src/utils.h"
#include "../src/release_html.h"
#include "../src/cJSON_deploy.h"
#include "../tests/utils.h"

int main()
{

      cJSON * dirlist2=NULL;
      char filesPATH[1024];
      char basePATH[1024];
      char linkPATH[1024];
      sprintf(filesPATH,"/1-1-1/files");
      sprintf(basePATH,"./test_check_JSON_link_list");
      _rmdir(basePATH,"");
      char full_filesPATH[1024];
      sprintf(full_filesPATH,"/public%s",filesPATH);
      _mkdir(basePATH,full_filesPATH);
      
      const char * data[]={"link123,53%","http://link123","https://link123","/link123\n",NULL};
      const char * links[]={"/1-1-1/files/link123","http://link123","https://link123","/link123",NULL};
      const char * metrics[]={"53%","","","",""};
      
      write_test_file(basePATH, full_filesPATH, "file1.txt");
      write_test_file(basePATH, full_filesPATH, "link_0.url", data[0]);
      write_test_file(basePATH, full_filesPATH, "link_1.url", data[1]);
      write_test_file(basePATH, full_filesPATH, "link_2.url", data[2]);
      write_test_file(basePATH, full_filesPATH, "link_3.url", data[3]);
      
      // Do the work
      dirlist2 = JSON_link_list(basePATH,filesPATH );
      
      //ck_assert_ptr_ne(dirlist2,NULL);
      debug("dirlist2 Pointer %p\n",dirlist2);
      cJSON_IsArray(dirlist2);
      //ck_assert_int_ne(cJSON_IsArray(dirlist2),0);
      //ck_assert_int_eq( cJSON_GetArraySize(dirlist2),4);
      
      // loop results
      cJSON * pos=NULL;
      const char * name=NULL;
      const char * link=NULL;
      const char * metric=NULL;
      int link_no=0;
      cJSON_ArrayForEach(pos,dirlist2)
      {
         name = cJSON_get_key(pos,"name");
         metric=cJSON_get_key(pos,"metric");
         link=  cJSON_get_key(pos,"link");
         
        // ck_assert_int_eq(strlen(name), 9);
         link_no=name[5]-'0';
         fprintf(stderr,"Links:%d, %s >%s< == >%s< , metric %s\n",link_no,name,link,links[link_no], metric);
         //ck_assert_str_eq(link,links[link_no]);
         
         //if (strlen(metrics[link_no])>0)
           // ck_assert_str_eq(metric,metrics[link_no]);
         //else
            //ck_assert_ptr_eq(metric,NULL);
      }
      
      // delete results
      cJSON_Delete(dirlist2);
      
      
      
      // error conditions: non existing dirlist
      dirlist2=JSON_dir_list("/mynonexistingdir","" );
      //ck_assert_ptr_eq(dirlist2,NULL);
      
      // error NULL parameters
      dirlist2=JSON_dir_list(NULL,NULL);
      //ck_assert_ptr_eq(dirlist2,NULL);
      _rmdir(basePATH,"");
 
}