/*
 deployd.c
 Created by Danny Goossen, Gioxa Ltd on 18/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

//TODO need nginx ports for cube and oc integration

#include "deployd.h"






// TODO put this in config and sed





// choice to keep parameter global, so signal handler can useit too.
static parameter_t * parameters=NULL;

/*------------------------------------------------------------------------
*
* Signal handler
*
*------------------------------------------------------------------------*/

static void signal_handler(sig)
int sig;
{
   alert ("\nEXIT: Signal %d : %s !!!\n",sig,strsignal(sig));
   switch(sig) {
      case SIGHUP:
      {
         parameters->hubsignal=1;
         debug ("set parameters->hubsignal\n ");
         break;
      }
      case SIGSEGV:
      {
		  parameters->exitsignal=1;
		  debug ("set parameters->exitsignal\n ");
		  usleep(3000000);
          exit(-1);
      }
      case SIGSTOP:
      case SIGQUIT:
      case SIGINT:
      case SIGTERM:
      {
         parameters->exitsignal=1;
         debug ("set parameters->exitsignal\n ");
         break;
      }
   }
// LCOV_EXCL_START
// should never come here, and if, makes nod difference
}
// LCOV_EXCL_STOP

/*------------------------------------------------------------------------
 *
 * Call to this function will daemonize it
 *
 *------------------------------------------------------------------------*/

void daemon_it(void)
{
   int i,lfp;
   char str[10];
   pid_t pid;

   if(getppid()==1)
   {   // should never happen
      error("already a service\n");exit(EXIT_FAILURE); /* already a daemon */
   }
   //test before daemon if we can lock the pid file
   lfp=open(parameters->pidfile, O_RDWR|O_CREAT,0640);
   if (lfp<0)
   {
      error("Cannot open pid file\n");
      exit(1); /* can not open */
   }
   if (lockf(lfp,F_TLOCK,0)<0)
   {
      error("deployctl already running in background, exit\n");
      exit(1); /* can not lock */
   }
   close(lfp);

   /* Fork off the parent process */
   pid = fork();

   /* An error occurred */
   if (pid < 0)
   {
      error("error on Fork\n");exit(EXIT_FAILURE);
   }
   /* Success: Let the parent terminate */
   if (pid > 0)
   {
      //sleep a bit yo get nice prompt
       usleep(100000); exit(EXIT_SUCCESS);
   }

   int setgid(gid_t gid);
   umask(0x027); /* set newly created file permissions */
   //chdir("/root"); /* change running directory */

   /* On success: The child process becomes session leader */
   if (setsid() < 0)
   {
      error("problem on setsid on fork\n");exit(EXIT_FAILURE);
   }
   lfp=open(parameters->pidfile, O_RDWR|O_CREAT,0640);
   if (lfp<0)
   {
      error("Cannot open pid file\n");exit(1); /* can not open */
   }
   if (lockf(lfp,F_TLOCK,0)<0)
   {
      error("deployctl already running in background, exit\n");exit(1); /* can not lock */
   }
   sprintf(str,"%d\n",getpid());
   write(lfp,str,strlen(str)); /* record pid to lockfile */
   if (parameters->verbose)
   {
      info(" Starting deployd in background w pid: %s\n",str);
      info(" - type 'ps axj | grep deployd' to see if it is running or \r\n");
      info(" - type 'sudo tail -f /var/log/messages | grep deployd => LINUX' to see the system log for deployd and \r\n");
      info(" - type (sudo) cat %s xargs kill to stop the background deployd. Have funn....\r\n\r\n",parameters->pidfile);
   }
   setlogmask (LOG_UPTO (LOG_DEBUG));
   openlog (APP_NAME,LOG_CONS | LOG_PID | LOG_NDELAY | LOG_PERROR, LOG_USER);
  setsyslog();
    /* Close out the standard file descriptors */
   close(STDIN_FILENO);

   close(STDOUT_FILENO);
   close(STDERR_FILENO);

   i=open("/dev/null",O_RDWR); dup(i); dup(i); /* handle standart I/O to dev null*/

  /* first instance continues */
   signal(SIGTSTP,SIG_IGN); /* ignore tty signals */
   signal(SIGTTOU,SIG_IGN); /* ignore stdout signals */
   signal(SIGTTIN,SIG_IGN); /* ignore stdin signals */

   info( "START and running w pid to : %s",str);
}

/*------------------------------------------------------------------------
 *
 * initialize the signal redirections
 *
 *------------------------------------------------------------------------*/

void init_signals()
{
   signal(SIGHUP,&signal_handler); /* catch hangup signal */
   signal(SIGTERM,&signal_handler); /* catch kill signal */
   signal(SIGINT,&signal_handler); /* catch kill signal */
   signal(SIGSTOP,&signal_handler); /* catch kill signal */
   signal(SIGQUIT,&signal_handler); /* catch kill signal */
   signal(SIGSEGV,&signal_handler); /* catch kill signal */

}

void show_usage( int exitcode)
{
      error("Usage: deployd [--options]\n\n");
      error("     --options=value:\n");
      error("version        : Shows the version\n");
      error("help           : Shows the version and usage\n");
      error("verbose        : turns on verbose output mode\n");
      error("quiet          : quiet down output\n");
      error("debug          : debug output\n");
      error("daemon         : runs deployd in background as a service\n");
      error("exec           : disable daemon\n");
//      error("sock_name=sock : specifies on which unix socket we're listening\n");
      error("pid=file.pid   : specifies the pid  file to be used when daemon\n");
//      error("secret=secret  : specifies the secret command\n");
//      error("timeout=10     : specifies the timeout in secondsfor a command\n");
      error("\n");
      error("Defaults: verbose    = %d\n",   VERBOSE_YN);
      error("          daemon     = %d\n",   DAEMONIZE_YN);
//      error("          name       = %s\n",   SOCK_NAME);
      error("          pid        = %s\n",  PID_FILE);
//      error("          secret     = %s\n",  SECRET);
      error("\n");
      exit(exitcode);
}

/*------------------------------------------------------------------------
* int process_options(int argc, char **argv, tb_parameter_t *parameter)
*
* Process the program line options and set the parameters accordingly
*------------------------------------------------------------------------*/
int process_options(int argc, char **argv, parameter_t *parameter)
{

   struct option long_options[] = {
      { "verbose",    0, NULL, 'v' },
      { "quiet",      0, NULL, 'q' },
      { "sock_name",    1, NULL, 'n' },
      { "daemon",     0, NULL, 'd' },
      { "exec",       0, NULL, 'e' },
      { "pid",        1, NULL, 'i' },
      { "help",       0, NULL, 'j' },
      { "version",    0, NULL, 'V' },
      { "secret",     1, NULL, 's' },
      { "debug",      0, NULL, 'D' },
      { "uid_user",   1, NULL, 'u' },
      { "gid_user",   1, NULL, 'g' },
      { "current_usr",0, NULL, 'C' },
      { "config",         1, NULL, 'c' },
      { "prefix"     ,1, NULL, 'p' },
      { "timeout"    ,1, NULL, 't' },
      { NULL,         0, NULL, 0 } };

   int           which;
   optind=1;  // reset if called again, needed for fullcheck as we call twice (server and main)
   // ref : http://man7.org/linux/man-pages/man3/getopt.3.html
   if (argc>1)
   {
      /* for each option found */
      while ((which = getopt_long(argc, argv, "+", long_options, NULL)) > 0) {

         /* depending on which option we got */
         switch (which) {
               /* --verbose    : enter verbose mode for debugging */
            case 'v':
            {
               if (parameter->quiet)
               {
                  error("Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
               {
                  parameter->verbose = 1;
               }
            }
               break;
            case 'q':
            {
               if (parameter->verbose)
               {
                  error("Invalid option, choose quiet or verbose\n");
                  return -1;
               }
               else
                  parameter->quiet = 1;
            }
            break;
               /* --v4         : disable IPv6 mode */
            case 'n': {
               parameter->socket_name =malloc(strlen(optarg)+1);
               sprintf(parameter->socket_name,"%s",optarg);
            //   printf( " Host Name %s \n",parameter->socket_name );
            }
            break;
            case 'u': {
               parameter->uid_user =malloc(strlen(optarg)+1);
               sprintf(parameter->uid_user,"%s",optarg);
            }
            break;
            case 'g': {
               parameter->gid_user =malloc(strlen(optarg)+1);
               sprintf(parameter->gid_user,"%s",optarg);
            }
            break;
            case 'p': {
                  parameter->prefix =malloc(strlen(optarg)+1);
                  sprintf(parameter->prefix,"%s",optarg);
            }
            break;
            case 's': {

               parameter->secret =malloc(strlen(optarg)+1);
               sprintf(parameter->secret,"%s",optarg);
            //   printf( " Host Name %s \n",parameter->secret );
            }
               break;

            case 'i': {
               parameter-> pidfile=malloc(strlen(optarg)+1);
               sprintf(parameter->pidfile,"%s",optarg);
               //printf( " pid-file > %s\n",parameter->pidfile );
            }
            break;
            case 't': {
               parameter->timeout   = atoi(optarg);
            }
            break;
            case 'd':
            {
               if (parameter->exec)
               {
                  error("Invalid option, choose daemon or exec\n");
                  return -1;
               }
               else
               {
                  parameter->daemon = 1;
               }
            }
               break;
            case 'e':
            {
               if (parameter->daemon)
               {
                  error("Invalid option, choose daemon or exec\n");
                  return -1;
               }
               else
               {
                  parameter->exec = 1;
               }
            }
               break;
            case 'D': {
               parameter->debug = 1;
               break;
            }
            case 'C': {
               parameter->current_user = 1;
               break;
            }
            case 'c': {
               parameter->config_file=calloc (strlen(optarg)+1,1);
               sprintf(parameter->config_file,"%s",optarg);
            }
               break;
            case 'V':
            {
               error("%s\n",PACKAGE_VERSION);
               exit(0);
            }
            case 'j': {
               error("\ndeloyd Git Version: %s\n \n",PACKAGE_VERSION);
               show_usage(0);
            }
               /* otherwise    : display usage information */
            default:
               ;
               show_usage(1);
         }
      }
   }
   // if NULL then no option was given and we default define the dir where tblastd was started !!!!!!
   //value = getenv(name);
   return optind;
}
/*------------------------------------------------------------------------
 * void rset_missing_parms(tb_parameter_t *parameter)
 *
 * set the parameters to default if not in commandline or config
 *------------------------------------------------------------------------*/
void set_missing_parms(parameter_t *parameter)
{
   parameter->hubsignal=0;
   parameter->exitsignal=0;
   if (parameter->pidfile == NULL)
   {
      parameter->pidfile=calloc(strlen(PID_FILE)+1,1);
      sprintf(parameter->pidfile,"%s",PID_FILE);
   }
   if (parameter->config_file == NULL)
   {
      parameter->config_file=calloc(strlen(CONFIG_FILE)+1,1);
      sprintf(parameter->config_file,"%s",CONFIG_FILE);
   }
   if (parameter->socket_name == NULL)
   {
      parameter->socket_name=calloc(strlen(SOCK_NAME)+1,1);
      sprintf(parameter->socket_name,"%s",SOCK_NAME);
   }
   if (parameter->secret == NULL)
   {
      parameter->secret=calloc(strlen(SECRET)+1,1);
      sprintf(parameter->secret,"%s",SECRET);
   }
   if (parameter->prefix == NULL)
   {
      parameter->prefix=calloc(strlen(PREFIX)+1,1);
      sprintf(parameter->prefix,"%s",PREFIX);
   }
   if (parameter->gid_user == NULL)
   {
      parameter->gid_user=calloc(strlen(GIDUSER)+1,1);
      sprintf(parameter->gid_user,"%s",GIDUSER);
   }
   if (parameter->uid_user == NULL)
   {
      parameter->uid_user=calloc(strlen(UIDUSER)+1,1);
      sprintf(parameter->uid_user,"%s",UIDUSER);
   }

   if (parameter->timeout==0) parameter->timeout  = TIME_OUT_CHILD;

   if (! parameter->verbose && !parameter->quiet) parameter->verbose = VERBOSE_YN;
   if (! parameter->daemon && ! parameter->exec) parameter->daemon = DAEMONIZE_YN;
   parameter->testprefix=calloc(strlen("")+1,1);
   sprintf(parameter->testprefix,"%s","");
   return;
}



void exit_this( int this_error)
{
   if (this_error!=EADDRINUSE && this_error!=EADDRNOTAVAIL)
   {
      if (parameters->daemon) remove(parameters->pidfile);
   }
   if (parameters) free(parameters);
   thread_cleanup(); //TODO move this to main
   exit(1);
}
// ********************************************************************
//          MAIN
// ********************************************************************

int main(int argc, char ** argv)
{
    parameters= calloc(sizeof(parameter_t),1);
      if(geteuid() != 0)
    {
      parameters->current_user=1;
    }
    else
    {
      parameters->current_user=0;
    }
    if ((optind=process_options(argc, argv,parameters)) < 0)
    {
        error(" *** ERRORin command line options, exit \n");
        return (-1);
    }
    if (argc > 1 && optind <= argc) {
        alert("ignoring non option arguments\n");
    }
    set_missing_parms(parameters);

    if (parameters->verbose) setverbose();
    if (parameters->debug) setdebug();

    if (parameters->daemon)
    {
        info("going to fork\n");
        daemon_it();
    }
    if (parameters->current_user==0)
    {
      alert("running as root, not recommended!\n");
    }
     init_signals();
     thread_setup(); //TODO move this to main
     curl_global_init(CURL_GLOBAL_ALL); //TODO move this to main
     git_libgit2_init();
   //git_threads_init();

   // check for jobs and execute!!! yah
   runners(parameters);
   curl_global_cleanup(); //TODO move this to main
   thread_cleanup(); //TODO move this to main
   git_libgit2_shutdown();
   if (parameters->daemon)
   {
      int i;
      for (i=getdtablesize();i>=0;--i)
      {
         close(i); /* close all descriptors */
      }
      remove(parameters->pidfile);
   }
   alert("exit deployctl\n");
   // TODO free parameters!!!

   return 0;
}
