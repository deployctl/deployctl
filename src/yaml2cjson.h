//
//  yaml2cjson.h
//  deployctl
//
//  Created by Danny Goossen on 10/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__yaml2cjson__
#define __deployctl__yaml2cjson__

#include <stdio.h>

cJSON * yaml_sting_2_cJSON (void * opaque ,const char * yaml);

#endif /* defined(__deployctl__yaml2cjson__) */
