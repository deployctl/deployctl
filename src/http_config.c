/*
 http_config.c
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



#include "deployd.h"

// TODO, read from /opt/config/nginx/....config.in
// and regex the hostname
// For now staic compiled here

const char* def_http_conf=

"server {\n" \
"   listen 80;\n  listen [::]:80;\n" \
"   server_name %s;\n" \
"   server_tokens off;\n" \
"   client_max_body_size 0;\n" \
"   index index.html; \n" \
"   root %s/public;\n"\
"   error_page 404 /html_error/404.html; \n" \
"   location / {try_files $uri $uri/ =404;}\n" \
"   %s" \
"   location /html_error/ { root /opt/deploy/config; }\n" \
"   location ^~ /.well-known { root /opt/deploy/var; }\n" \
"   }\n";

const char* def_https_conf=
"server {\n" \
"   listen 80;\n  listen [::]:80;\n" \
"   server_name %s;\n" \
"   server_tokens off;\n" \
"   client_max_body_size 0;\n" \
"   index index.html; \n" \
"   root %s/public;\n" \
"   error_page 404 /html_error/404.html; \n" \
"   location / { return 301 https://%s$request_uri; }\n" \
"   location /html_error/ { root /opt/deploy/config; }\n" \
"   location ^~ /.well-known { root /opt/deploy/var; }\n" \
"   }\n" \
"server {\n" \
"   listen 443 ssl http2;\n  listen [::]:443 ssl http2;\n" \
"   server_name %s;\n" \
"   server_tokens off;\n" \
"   client_max_body_size 0;\n" \
"   ssl on;\n" \
"   ssl_certificate /opt/deploy/.acme.sh/%s/fullchain.cer;\n" \
"   ssl_certificate_key /opt/deploy/.acme.sh/%s/%s.key;\n" \
"   ssl_ciphers 'ECDHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES128-SHA256:ECDHE-RSA-AES256-SHA:ECDHE-RSA-AES128-SHA:ECDHE-RSA-DES-CBC3-SHA:AES256-GCM-SHA384:AES128-GCM-SHA256:AES256-SHA256:AES128-SHA256:AES256-SHA:AES128-SHA:DES-CBC3-SHA:!aNULL:!eNULL:!EXPORT:!DES:!MD5:!PSK:!RC4';\n" \
"   ssl_prefer_server_ciphers on;\n" \
"   ssl_protocols  TLSv1 TLSv1.1 TLSv1.2;\n" \
"   ssl_session_cache  builtin:1000  shared:SSL:10m;\n" \
"   ssl_session_timeout  5m;\n" \
"   index index.html;\n" \
"   root %s/public;\n"\
"   error_page 404 /html_error/404.html;\n"\
"   location / {try_files $uri $uri/ =404;}\n" \
"   %s" \
"   location /html_error/ {root /opt/deploy/config;}\n" \
"   }\n";

const char * repo_conf_options=
 "   location ~ ^/(rpm|deb)/([0-9a-zA-Z\\x20\\.]+)/([0-9a-z\\_]+) {autoindex on;}" \
 "   location ~* ^/(deb|rpm)/ {return 301 $scheme://$server_name/;}";

/*------------------------------------------------------------------------
 * url_check,
 * writes the commit tag in basePATH/public alias baseHREF
 * and performs a CURL to baseHref
 * to verify if baseHREF points to basePATH/public
 * Returns 0 on success
 *------------------------------------------------------------------------*/
int url_check(void * opaque, char * basePATH,char * baseHREF)
{ // write http config for production environment
   char * newarg[11];
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;

   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;

   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;

   char filepath[1024];
   char * commit_sha=cJSON_GetObjectItem(env_json, "CI_COMMIT_SHA")->valuestring;
   // ***** write index.html of the tag
   newarg[0]="write_file";
   newarg[1]=filepath;
   sprintf(filepath, "%s/public/.commit_sha.txt",basePATH);
   newarg[2]=commit_sha;
   newarg[3]=NULL;

  Write_dyn_trace(trace, yellow,"  Check url:");
  Write_dyn_trace(trace, cyan," %s\n",baseHREF);

   Write_dyn_trace_pad(trace, none,75,"+ validate domain ...");


   debug("writing %s %d bytes\n",newarg[1],strlen(newarg[2]));
   exitcode=cmd_write(opaque);
   if (exitcode)
   {
      debug("failed writing hash to %s\n",newarg[1]);
      Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: Failed to write validation hash\n");
   }
   if (!exitcode)
   {
      usleep(500000);// wait till file change propagated
      exitcode=url_verify( commit_sha,baseHREF,trace);
   }
   //Restore arguments
   ((data_exchange_t *)opaque)->paramlist=saved_args;
   return exitcode;
}

/*------------------------------------------------------------------------
 * delete http config file in basePATH
 *------------------------------------------------------------------------*/
int delete_http_config(void * opaque,char * basePATH)
{
   char * newarg[4];
    char config_file_name[1024];
    // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
    // env json

   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;

   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
    sprintf((char *)config_file_name,"%s/server.conf",basePATH);
   newarg[0]="/bin/rm";
   newarg[1]="-f";
   newarg[2]=(char *)config_file_name;
   newarg[3]=NULL;

   Write_dyn_trace(trace, none,"+ Remove new offending config-file\n");

   update_details(trace);
   debug("cmd: %s %s %s \n",newarg[0],newarg[1],newarg[2]);

   int temp=cmd_exec(opaque);

   if (temp) {debug("Failed to delete config %s\n",newarg[2]);}
   ((data_exchange_t *)opaque)->paramlist=saved_args;
   return exitcode;
}

/*------------------------------------------------------------------------
 * write_http_config for http or https with http to https redirect
 * write as root:nginx
 * returns 0 on succes
 *------------------------------------------------------------------------*/
int write_http_config(void * opaque, int is_https,char * basePATH, char * server_name)
{ // write http config for production environment
    char * newarg[4];
    char config_file_name[1024];
    char config_file[4096];
    // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;


   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;

   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;

    sprintf((char *)config_file_name,"%s/server.conf",basePATH);
   const char * conf_options=NULL;
   const char def_conf_options[]="#\n";
   conf_options=def_conf_options;

   if (is_https>1) conf_options=repo_conf_options;

   if (is_https==1||is_https==3)
   {
      sprintf((char *)config_file,def_https_conf,server_name,basePATH,server_name,server_name,server_name,server_name,server_name,basePATH,conf_options);
      newarg[0]="+ write_config_https...";

   }
   else //if (is_https==0|| is_https==2 )
   {
       sprintf((char *)config_file,def_http_conf,server_name,basePATH,conf_options);
      newarg[0]="+ write_config_http...";

   }
   

       // ***** write config
    newarg[1]=(char *)config_file_name;
    newarg[2]=(char *)config_file;
    newarg[3]=NULL;
    Write_dyn_trace_pad(trace, none,75,"%s",newarg[0]);
   debug("writing %s %d bytes\n",newarg[1],strlen(newarg[2]));
    exitcode=cmd_write(opaque);

    if (!exitcode)
    {
        debug("wrote %s, %d bytes\n",newarg[1],strlen(newarg[2]));
       Write_dyn_trace(trace, green,"[OK]\n");
    }
   else
   {
      debug("ERROR: Writing  %s, %d bytes\n",newarg[1],strlen(newarg[2]));
       Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: writing new config-file\n");
   }
   update_details(trace);
	((data_exchange_t *)opaque)->paramlist=saved_args;
    return exitcode;
}

/*------------------------------------------------------------------------
 * check_namespace
 * read the basePATH/.namespace file
 * should contain the project_url from previous deployment
 * If that matches the current project url, it returns 0
 * if no content or no file it returns also 0
 *------------------------------------------------------------------------*/
int check_namespace(void * opaque,char * filepath)
{
   int result=0;
   char namespace_file_name[1024];
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

   cJSON * env_json=((data_exchange_t *)opaque)->env_json;
   char * project_url=cJSON_get_key(env_json, "CI_PROJECT_URL");
   sprintf((char *)namespace_file_name,"%s/.namespace",filepath);

   Write_dyn_trace_pad(trace, none,75,"+ Check name space ...");

   FILE *f = fopen(namespace_file_name, "r");
   if (f == NULL)
   {// doesn't exist, so ok
       Write_dyn_trace(trace, green,"[OK]\n");

      return 0;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   result=strcmp(mem.memory,project_url);

   if (result)
   {
      debug("ERR: namespace : \n => %s\n != \n =>%s\n",project_url,mem.memory);
      Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: project_url : \n =>%s\n != \n =>%s\n",project_url,mem.memory);

      exitcode=1;
   }
   else
   {
      Write_dyn_trace(trace, green,"[OK]\n");
        exitcode=0;
   }
    update_details(trace);
   // TODO
   // to avoid deliberate spoofing of namespaces and does to sabotage somebody else release:
   // check if CI_REPOSITORY_URL points to the same repo as CI_PROJECT_URL
   // then  git ls-remote CI_REPOSITORY_URL if ok, at least we got access, better to access with API, but doesn't work if not public repo.

   // for now, unintentional overwrite protection is provided, room for discussion and brainstorm
   return exitcode;
}

/*------------------------------------------------------------------------
 * write_namespace, or locks current directory on this project
 * writes the project_url in basePATH/.namespace
 * return 0 on success
 *------------------------------------------------------------------------*/
int write_namespace(void * opaque, char * filepath )
{
   char * newarg[4];
   char namespace_file_name[1024];
   // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

   // env json
   cJSON * env_json=((data_exchange_t *)opaque)->env_json;

   // no need to set environment for individual commands
   ((data_exchange_t *)opaque)->needenvp=0;

   void * saved_args=((data_exchange_t *)opaque)->paramlist;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;

   char * project_url=cJSON_get_key(env_json, "CI_PROJECT_URL");

   newarg[0]="write_name_space";
   newarg[1]=(char *)namespace_file_name;
   newarg[2]=(char *)project_url;
   newarg[3]=NULL;
    Write_dyn_trace_pad(trace, none,75,"+ Lock name space ...");

   sprintf((char *)namespace_file_name,"%s/.namespace",filepath);
   exitcode=cmd_write(opaque);
   if (!exitcode)
   {
      debug("wrote namespace %s: %s\n",newarg[1],newarg[2]);
   }
   if (!exitcode)
     Write_dyn_trace(trace, green,"[OK]\n");
   else
     Write_dyn_trace(trace, red,"[FAILED]\n");

   ((data_exchange_t *)opaque)->paramlist=saved_args;

   update_details(trace);
   return exitcode;
}
