/*
 commands.c
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#include "deployd.h"



static const command_t commandlist[] = {
    //  to do read this from a json config file
    //                                                                                           INTERNAL        PASS     AS
    //    Request         =====>          Command                                                  CMD           ENV    USER
    { "version",       { NULL     , NULL , NULL                                   , NULL}   , &version        ,0   , NULL },
    { "list",          { NULL     , NULL , NULL                                   , NULL}   , &list           ,0   , NULL },
    { "sleep1",        { "/bin/sh" , "-c" , "sleep 1"                             , NULL}   , &cmd_exec       ,0   , NULL },
    { "sleep3",        { "/bin/sh" , "-c" , "sleep 3"                             , NULL}   , &cmd_exec       ,0   , NULL },
    { "env",           { NULL                                                     , NULL}   , &cmd_env        ,1   , NULL },
    { "env_clnt",      { "/bin/sh" , "-c" , "env"                                 , NULL}   , &cmd_exec       ,1   , NULL },
    { "reload",        {"/bin/systemctl" , "reload" , "nginx.service"             , NULL}   , &cmd_exec       ,0   , NULL },
    { "verify",        {"/bin/sh" , "-c" , "/sbin/nginx -t"                       , NULL}   , &cmd_exec       ,0   , NULL },
    { "static",        { NULL     , NULL ,  NULL                                  , NULL}   , &cmd_static     ,1   , NULL },
    { "release",       { NULL     , NULL ,  NULL                                  , NULL}   , &cmd_release    ,1   , NULL },
    { "delete",        { NULL     , NULL ,  NULL                                  , NULL}   , &cmd_delete     ,1   , NULL },
    { "repo_config",    { NULL     , NULL ,  NULL                                 , NULL}   , &cmd_repo_config ,1   , NULL },
    { "repo_add",       { NULL     , NULL ,  NULL                                 , NULL}   , &cmd_repo_add    ,1   , NULL },
    { "status",        {"/bin/systemctl" , "status" , "nginx.service"             , NULL}   , &cmd_exec       ,0   , NULL },
    { "tree",          {NULL,                                                       NULL}   , &cmd_tree       ,0   , NULL },
    { "colortest",     {NULL,                                                       NULL}   , &cmd_color_test ,0   , NULL },
    { "ls",        {"ls -la", NULL}   , &exec_color       ,0   , NULL },
    { NULL,            {NULL      , NULL ,  NULL                                  , NULL}   , NULL            ,0   , NULL } // end point for traversing
};

/*------------------------------------------------------------------------
 * int exec_command(const char * this_command,cJSON *env_json,feedback_t * feedback)
 *
 * executes a command from the commandlist, can be called from anywhere.
 *------------------------------------------------------------------------*/
int exec_command(const char * this_command,cJSON *env_json, struct trace_Struct *trace,parameter_t * parameters)
{
   int exitcode=0;
    uidguid_t uidgid_root_nginx;
    if (!parameters->current_user)
      {
        if (get_gid(&uidgid_root_nginx.gid, parameters->gid_user))uidgid_root_nginx.gid=0 ;
        if (get_uid(&uidgid_root_nginx.uid,parameters->uid_user))uidgid_root_nginx.uid=0;
     }
      else
      {
         uidgid_root_nginx.gid=getgid();
         uidgid_root_nginx.uid= geteuid() ;
      }

    int have_env=0;
    debug("command=%s ....\n ",this_command);
    if (env_json) have_env=1; else have_env=0;

    int id=0;
    command_t * walk=(command_t *)&commandlist[id];
    char ** paramlist=NULL;

    int needenvp=0;
    paramlist=NULL;
    while (walk->request)
    {
        if (strcmp(this_command, walk->request)==0)
        {
            paramlist=(char **)walk->parmList;
            needenvp=walk->do_envp;
            break;
        }
        id++;
        walk=(command_t *)&commandlist[id];
    }
    if (paramlist && ( (needenvp && have_env ) || ! needenvp))
    {
        // set structure
        data_exchange_t temp;
        temp.paramlist=walk->parmList;
        temp.parameters=parameters;
        temp.env_json=env_json;
        temp.needenvp=walk->do_envp;
        temp.this_command=walk->request;
        temp.timeout=parameters->timeout;
        temp.gid=uidgid_root_nginx.gid;
        temp.uid=uidgid_root_nginx.uid;
        temp.current_user=parameters->current_user;
        temp.trace=trace;
        if (!temp.current_user && setgid(temp.gid)) { error("could not set gid\n"); return 1;}
        if (!temp.current_user && setuid(temp.uid)) { error("could not set uid\n"); return 1;}

        // user=7 rwx , group=7 r--, other: ---
        umask( S_IRWXO ); /* set newly created file permissions */

        //if (temp.paramlist[0]!=NULL) // predef command
        //    Write_dyn_trace(trace, cyan, "+ %s: %s %s %s\n",this_command,walk->parmList[0],walk->parmList[1],walk->parmList[2]);
        // exec
        exitcode=walk->internal_command(&temp);
    }
    else
    {
        if (!paramlist) // info for deployd for all
            Write_dyn_trace(trace, red,"Error: Unknown command: %s\n",this_command);
        else if (needenvp && have_env) Write_dyn_trace(trace, red,"Error parsing env for command: %s\n",this_command);
        else
            Write_dyn_trace(trace, red,"Error: no environment for command: %s\n",this_command);
        exitcode=1;
    }
   if (!exitcode) debug("[OK]\n");
    update_details(trace);
    return exitcode;
}

/*------------------------------------------------------------------------
 * Internal Command : list => list the availleble commands for the client
 *------------------------------------------------------------------------*/

int list(void * opaque)
{
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
    // traverse list and output while doing it
    int id=0;
    struct command_t * walk=(command_t *)&commandlist[id];
    while (walk->request)
    {
        if ((walk)->parmList[2])
            Write_dyn_trace(trace, none,"%s ==> %s \n", (walk)->request, (walk)->parmList[2]);
        else
            Write_dyn_trace(trace, none,"%s ==> %s \n", (walk)->request, "internal");

        id++;
        walk=(command_t *)&commandlist[id];
    }
    return 0;
}


/*------------------------------------------------------------------------
 * Internal Command : version => returns the this deployd git-version
 *------------------------------------------------------------------------*/
int version(void * opaque)
{
    // feedback buffer
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   // output version
    Write_dyn_trace(trace, none,"%s\n", PACKAGE_VERSION);

    return 0;
}
