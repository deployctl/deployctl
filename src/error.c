/*
 cJSON_deploy.h
 Created by Danny Goossen, Gioxa Ltd on 7/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
// LOGGING or OUTPUT with DEBUG/ALERT/INFO/ERROR/ SOCKETERROR
#include <syslog.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <strings.h>
#include <ctype.h>
#include "error.h"

static int syslog_yn=0;
static int verbose_yn=0;
static int debug_yn=0;


void setsyslog()
{
    syslog_yn=1;
}
int getsyslog()
{
  int r=syslog_yn;
    return(r);
}
void clrsyslog()
{
    syslog_yn=0;
}

void setverbose()
{
    verbose_yn=1;
}
int getverbose()
{
    return(verbose_yn);
}
void clrverbose()
{
    verbose_yn=0;
}

void setdebug()
{
    debug_yn=1;
}
int getdebug()
{
    return(debug_yn);
}
void clrdebug()
{
    debug_yn=0;
}

int error( char *message, ...)
{
    va_list varg;
    va_start (varg, message);
    if (syslog_yn)
        vsyslog (LOG_ERR,message,varg);
    else
        vfprintf(stderr, message,varg);

    va_end(varg);
    return 0;
}

int alert( char *message, ...)
{

        va_list varg;
        va_start (varg, message);

        if (syslog_yn)
            vsyslog (LOG_ALERT,message,varg);
        else
            vfprintf(stderr, message,varg);

        va_end(varg);

    return 0;
}

int info( char *message, ...)
{
    if (verbose_yn)
    {
        va_list varg;
        va_start (varg, message);
        if (syslog_yn)
            vsyslog (LOG_NOTICE,message,varg);
        else
            vfprintf(stdout, message,varg);

        va_end(varg);
    }
    return 0;
}
int debug( char *message, ...)
{
    if (debug_yn)
    {
        va_list varg;
        va_start (varg, message);
        if (syslog_yn)
            vsyslog (LOG_NOTICE,message,varg);
        else
            vfprintf(stdout, message,varg);

        va_end(varg);
    }
    return 0;
}

int sock_error(char * msg)
{
    int e=errno;

    if (syslog_yn)
        syslog( LOG_ERR,"%s, sock_err:#%d : %s", msg, e, strerror(e) );
    else
        fprintf(stderr,"%s, sock_err:#%d : %s\n", msg, e, strerror(e) );

    return e; // return error Num in case we need it later
}

void sock_error_no(char * msg,int e)
{
    if (syslog_yn)
        syslog( LOG_ERR,"%s, sock_err:#%d : %s", msg, e, strerror(e) );
    else
        fprintf(stderr,"%s, sock_err:#%d : %s\n", msg, e, strerror(e) );

    return; // return error Num in case we need it later
}

int hex_dump_msg(const char*buf,ssize_t size_peek,const char *message, ...)
{
    int i,j;
    char *datapeek=(char*) buf;
    char * li= calloc(size_peek, 6);
    va_list varg;
   // pthread_mutex_lock(&info_mutex);
    if (li)
    {
        va_start (varg, message);
        vsnprintf(li, 6*size_peek , message, varg);

        //sprintf(li,"%s \n",msg);
        for ( i=0;i<size_peek;i++)
        {
            if ((i%32)==0)
            {
                if (i) sprintf(li+strlen(li)," ");
                if (i) for (j=(i-32);(j!=i);j++)
                {
                    if (isprint(datapeek[j]))
                        sprintf(li+strlen(li),"%c",datapeek[j]);
                    else
                        sprintf(li+strlen(li),".");
                }
                if (i!=0) sprintf(li+strlen(li),"\n");
            }
            sprintf(li+strlen(li),"%02x ",(u_int8_t)datapeek[i]);
        }
        if ((i%32))
        {
            if (i) for (j=(i%32);(j!=32);j++) sprintf(li+strlen(li),".. ");
            if (i) for (j=(i-(i%32));(j!=i);j++)
            {
                if (isprint(datapeek[j]))
                    sprintf(li+strlen(li),"%c",datapeek[j]);
                else
                    sprintf(li+strlen(li),".");
            }
            //if (i!=0)sprintf(li+strlen(li),"\n");
        }
        //printf("%02x ",(u_int8_t)datapeek[i]);
        va_end(varg);
        if ((i%32)) sprintf(li+strlen(li),"\n");

        //sprintf(li+strlen(li),""); // TODO find other solution to add \0

            fprintf(stderr, "%s",li);
        //printf("%s",li);

        free(li);
    }
    //pthread_mutex_unlock(&info_mutex);
    return 1;
}
