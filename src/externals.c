/*
  externals.c
  Created by Danny Goossen, Gioxa Ltd on 22/3/17.

MIT License

Copyright (c) 2017 deployctl, Gioxa Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

\
#include "deployd.h"




/*----------------------------------------------------------------------------------
 * url_download curl to baseHREF/.commitsh and compaire content to commit_sha
 * returns 0 on success
 *-----------------------------------------------------------------------------------*/
int url_repo_base_path  (char ** basepath,char * baseHREF,struct trace_Struct * trace)
{
   int exitcode=0;
   char request[1024];
   sprintf(request, "%s/repo.json",baseHREF);
   debug("preprepare curl for url %s\n",request);
   CURL *curl = curl_easy_init();
   CURLcode res;
   if(curl)
   {
      // check url
      struct MemoryStruct chunk;

      chunk.memory = malloc(1);  // will be grown as needed by the realloc above
      chunk.size = 0;    // no data at this point

      debug("curl easy setup for url >%s<\n",(char*)request);
      curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
      curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
      curl_easy_setopt(curl, CURLOPT_FRESH_CONNECT, 1L);
      curl_easy_setopt(curl, CURLOPT_FORBID_REUSE,1L);

      res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);
      debug("curl result: %d\n",res);
      if (res!=CURLE_OK )
      {
         Write_dyn_trace(trace, red,"\nERROR: Failed to get %s\n",request);
         debug("ERROR: Failed to get %s\n",request);
         exitcode=1;
      }
      else
      {
         cJSON * repo=NULL;
         char * base=NULL;
         if (chunk.memory){
         	repo=cJSON_Parse(chunk.memory);
            if (repo) base=cJSON_get_key(repo, "base_path");
         }
         if (!(base && strlen(base)>0))
         {
            Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: check FAILED for %s\n",request);

            exitcode=1;
         }
         else
         {
            *basepath=strdup(base);
            Write_dyn_trace(trace, green,"[OK]\n");
         }
         if (repo) cJSON_Delete(repo);
      }
      free(chunk.memory);
      return exitcode;
   }
   return exitcode=1;
}


/*----------------------------------------------------------------------------------
 * url_verify curl to baseHREF/.commitsh and compaire content to commit_sha
 * returns 0 on success
 *-----------------------------------------------------------------------------------*/
int url_verify(char * commit_sha,char * baseHREF,struct trace_Struct * trace)
{ // write http config for production environment
   int  exitcode=0;
   char request[1024];
   sprintf(request, "%s/.commit_sha.txt",baseHREF);
   debug("preprepare curl for url %s\n",request);
   CURL *curl = curl_easy_init();
   CURLcode res;
   if(curl)
   {
      // check url
      struct MemoryStruct chunk;

      chunk.memory = malloc(1);  // will be grown as needed by the realloc above
      chunk.size = 0;    // no data at this point

      debug("curl easy setup for url >%s<\n",(char*)request);
      curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
      curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
      curl_easy_setopt(curl, CURLOPT_FRESH_CONNECT, 1L);
      curl_easy_setopt(curl, CURLOPT_FORBID_REUSE,1L);

      res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);
      debug("curl result: %d\n",res);
      if (res!=CURLE_OK )
      {
         Write_dyn_trace(trace, red,"\nERROR: Failed to get %s\n",request);
         debug("ERROR: Failed to get %s\n",request);
         free(chunk.memory);
         exitcode=1;
      }
      else
      {
         size_t len=strlen(commit_sha);
         if (len > chunk.size ||strncmp(commit_sha,chunk.memory,len)!=0)
         {
            chunk.memory[len]=0;
            debug("fail url check >%s< != >%s<",commit_sha,chunk.memory);
            Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: check FAILED for %s\n",request);

            free(chunk.memory);
            exitcode=1;
         }
         else
         {
            Write_dyn_trace(trace, green,"[OK]\n");
            free(chunk.memory);
         }
      }
      return exitcode;
   }
   else Write_dyn_trace(trace, red,"[FAILED]\n  ERROR: could not initialise curl\n");

   return exitcode=1;
}

/*----------------------------------------------------------------------------------
 * verify_http_config:
 * exec /bin/sh nginx -t
 * returns 0 on success
 *-----------------------------------------------------------------------------------*/
int verify_http_config(void * opaque)
{
	char * newarg[4];

   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

	// no need to set environment for individual commands
	((data_exchange_t *)opaque)->needenvp=0;

	void * saved_args=((data_exchange_t *)opaque)->paramlist;
	((data_exchange_t *)opaque)->paramlist=(char **)newarg;

	newarg[0]="/bin/sh";
	newarg[1]="-c";
	newarg[2]="sudo /usr/sbin/nginx -t -q";
	newarg[3]=NULL;

	Write_dyn_trace_pad(trace, none,75,"+ Test nginx config ...");
	debug("cmd: %s %s %s \n",newarg[0],newarg[1],newarg[2]);

	exitcode=cmd_exec(opaque);
	if (exitcode) {debug("nginx config failure\n");Write_dyn_trace(trace, red,"[FAILED]\n");}
	if (!exitcode) Write_dyn_trace(trace, green,"[OK]\n");
	((data_exchange_t *)opaque)->paramlist=saved_args;
	return exitcode;

}

/*----------------------------------------------------------------------------------
 * reload_http_config:
 * exec systemctl reload nginx
 * returns 0 on success
 *-----------------------------------------------------------------------------------*/
int reload_http_config(void * opaque)
{
	char * newarg[4];

   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

	// no need to set environment for individual commands
	((data_exchange_t *)opaque)->needenvp=0;

	void * saved_args=((data_exchange_t *)opaque)->paramlist;
	((data_exchange_t *)opaque)->paramlist=(char **)newarg;

	newarg[0]="/bin/sh";
	newarg[1]="-c";
	newarg[2]="sudo /usr/sbin/nginx -s reload";
	newarg[3]=NULL;

	Write_dyn_trace_pad(trace, none,75,"+ Reload nginx ...");
	debug("cmd: %s %s %s \n",newarg[0],newarg[1],newarg[2]);

	exitcode=cmd_exec(opaque);
	if (exitcode) {debug("nginx reload failure\n");Write_dyn_trace(trace, red,"[FAILED]\n");}
	if (!exitcode) Write_dyn_trace(trace, green,"[OK]\n");
	((data_exchange_t *)opaque)->paramlist=saved_args;
   usleep(500000); // allow reload before checking with url_verify!!!
	return exitcode;
}
