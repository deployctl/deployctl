/*
 gitlab_api.c
 Created by Danny Goossen, Gioxa Ltd on 4/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



#include "deployd.h"


/*------------------------------------------------------------------------
 * get_release(void * opaque, cJSON ** release)
 * returns the gitlab cJSON response of:
 * /api/v4/projects/<project id>/repository/tags/<tag>
 * with the CI-token
 * returns 1 on success
 * caller need to free release
 *------------------------------------------------------------------------*/
int get_release(void * opaque, cJSON ** release)
{
   cJSON * env_json= ((data_exchange_t *)opaque)->env_json;
   CURLcode res;
   *release=NULL;
   CURL *curl = curl_easy_init();
        if(curl)
    {
        //build request
        char * request=calloc(1, 0x1000);
        //"CI_PROJECT_URL":   "https://gitlab.gioxa.com/deployctl/test_deploy_release",

        char * buildrepo=cJSON_GetObjectItem(env_json, "CI_PROJECT_URL")->valuestring;

        //CI_PROJECT_PATH="gitlab-org/gitlab-ce"
        char * project_path=cJSON_GetObjectItem(env_json, "CI_PROJECT_PATH")->valuestring;
       debug("ci-project_path: %s\n",project_path);
       //find position of CI_PROJECT_PATH in url
        size_t pos = strstr(buildrepo, project_path) - buildrepo;

       // copy till CI_PROJECT_PATH
        memcpy(request,buildrepo ,pos );
        // and add =>api/v4/projects/{CI_PROJECT_ID}/repository/tags/{CI_COMMIT_SHA}
        sprintf(request+pos, "api/v4/projects/%s/repository/tags/%s",cJSON_get_key(env_json, "CI_PROJECT_ID"),cJSON_get_key(env_json, "CI_COMMIT_TAG"));
        //Done

        debug("\nRequest API: %s\n",request);
        struct MemoryStruct chunk;
        init_dynamicbuf(&chunk);
        struct curl_slist *list = NULL;

        curl_easy_setopt(curl, CURLOPT_URL, (char*)request);
        debug("CI_JOB_TOKEN : %s\n",cJSON_get_key(env_json, "CI_JOB_TOKEN"));
        debug("CI_COMMIT_TAG : %s\n",cJSON_get_key(env_json, "CI_COMMIT_TAG"));
       //CI_JOB_TOKEN
        char headertoken[255];
        sprintf(headertoken, "JOB_TOKEN: %s",cJSON_get_key(env_json, "CI_JOB_TOKEN"));
        debug("-H \"%s\"\n",headertoken);

        list = curl_slist_append(list, headertoken);

        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, list);
        /* send all data to this function  */
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
        /* we pass our 'chunk' struct to the callback function */
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
        res = curl_easy_perform(curl);
        curl_slist_free_all(list);
        if (res!=CURLE_OK )
        {
            debug("\n Failed to get %s\n",request);

            free_dynamicbuf(&chunk);

            if (request) free(request);
            curl_easy_cleanup(curl);
            return 1;
        }
        long http_code = 0;
        curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
        if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
        {
           //result in chunk
           debug("gitlab_api reply:\n>>>\n%s\n<<<\n",chunk.memory);
           cJSON * tag = cJSON_Parse(chunk.memory);
           if (!tag)
           {
               debug("failed json parse request: -->\n%s\n<--\n",chunk.memory);
               if (request) free(request);
               curl_easy_cleanup(curl);
               free_dynamicbuf(&chunk);
               return 1;
           }
           *release=tag;

           free_dynamicbuf(&chunk);
           if (request) free(request);
           curl_easy_cleanup(curl);
           return 0;
      }
        else
        {
           debug("Failed API request with %d\n%s\n",http_code,chunk.memory);
        }
      free_dynamicbuf(&chunk);
      if (request) free(request);
      curl_easy_cleanup(curl);
      return 0;
    }
   return 1;
}

// used to interupt the long polling
int progress_callback(void *clientp,   curl_off_t dltotal,   curl_off_t dlnow,   curl_off_t ultotal,   curl_off_t ulnow)
{
  stop_struct_t * stop_struct_data=(stop_struct_t *)clientp;
  if (*(stop_struct_data->exit) || *(stop_struct_data->reload)) return 1;

  return 0;
}

cJSON * get_job(cJSON * runner,cJSON * info, void * stop_struct, CURL * curl)
{

   char * token=cJSON_get_key(runner, "token");
   char * domain=cJSON_get_key(runner, "ci_url");
   if (!domain || !token) return NULL;
   int * error_count=&(cJSON_GetObjectItem(runner, "errorcount")->valueint);
   int runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
   cJSON * result=NULL;
   CURLcode res;
   if(curl)
   {
      //build request
      char request[1024];
      sprintf(request, "%s/api/v4/jobs/request",domain);

      struct MemoryStruct chunk;
      struct headerStruct headers_r;
      init_dynamicbuf( &chunk);
      init_header_struct(&headers_r);
      // add JSON header
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, "accept: application/json");
      //headers = curl_slist_append(headers, "User-Agent: gitlab-runner 10.0.0-RC1");
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

      // Enable intruption for gracefull shutdown
      curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, &progress_callback);
      curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, stop_struct);
      /* enable progress meter */
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
      curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);

      curl_easy_setopt(curl, CURLOPT_URL, (char*)request);

      cJSON * params=cJSON_CreateObject();
      cJSON_AddStringToObject(params, "token", token);


         char * tmp=cJSON_get_key(runner, "last_update");
         if (tmp) cJSON_AddStringToObject(params, "last_update", tmp);

      cJSON_AddItemToObject(params, "info", info);
      char * params_str=cJSON_PrintUnformatted(params);
      cJSON_DetachItemFromObject(params, "info");
      cJSON_Delete(params);

      //debug("runner %2d: get_job_sent_params: %s\n",runner_id,params_str);
      /* complete within 70 seconds (need 50 for long polling)*/
      curl_easy_setopt(curl, CURLOPT_TIMEOUT, 70L);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, params_str);
      //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");
      /* send all data to this function  */
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);

      /* we pass our 'chunk' struct to the callback function */
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);

      curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION,WriteHeaderCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEHEADER, &headers_r);
      res = curl_easy_perform(curl);
      curl_slist_free_all(headers);

      long http_code = 0;
      curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (getdebug())
      {
         char * print_it=cJSON_PrintUnformatted(headers_r.header);
         if (print_it)
         {
            debug("runner %2d: get_job_receive_headers: %s\n",runner_id,print_it);
            free(print_it);
         }
      }
      char * x_last_update=cJSON_get_key(headers_r.header, "X-GitLab-Last-Update");
      if (x_last_update)
      {
         if (cJSON_get_key(runner, "last_update"))
            cJSON_ReplaceItemInObject(runner, "last_update", cJSON_CreateString(x_last_update));
         else
            cJSON_AddItemToObject(runner, "last_update", cJSON_CreateString(x_last_update));
      }

      if (validate_key(headers_r.header, "Gitlab-Ci-Builds-Polling","yes")==0  )
      {
      alert("runner %2d: Long Polling (%s) ...\n",runner_id, domain);
         if (cJSON_get_key(runner, "Long Polling"))
         cJSON_ReplaceItemInObject(runner, "Long Polling", cJSON_CreateString("yes"));
         else
         cJSON_AddItemToObject(runner,"Long Polling", cJSON_CreateString("yes"));

      }
      else
      {
         if (cJSON_get_key(runner, "Long Polling"))
         cJSON_ReplaceItemInObject(runner, "Long Polling", cJSON_CreateString("no"));
         else
         cJSON_AddItemToObject(runner,"Long Polling", cJSON_CreateString("no"));
      }
      if (http_code == 201 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         debug("runner %2d: got a job\n",runner_id);
        // printf("%s\n",chunk.memory);
         result= cJSON_Parse(chunk.memory);
         if (*error_count) (*error_count)=0;
      }
      else if ((http_code == 204 )&& res != CURLE_ABORTED_BY_CALLBACK)
      {
         debug("runner %2d: no job scheduled : %s\n",runner_id,chunk.memory);
         if (*error_count) (*error_count)=0;
      }
      else
      {
          if ((*error_count)<10) (*error_count)++; else (*error_count)=10;
          if (res)
             error( "runner %2d: (%2d) getjob failed %s\n",runner_id,(*error_count),curl_easy_strerror(res));
          else
             error("runner %2d: (%2d) X-Request-Id:%s =>getjob failed HTTP_code: %ld >> %s\n",runner_id,(*error_count),cJSON_get_key(headers_r.header, "X-Request-Id") ,http_code,chunk.memory);
      }
      free_dynamicbuf(&chunk);
      free_header_struct(&headers_r);
      free(params_str);
      curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, NULL);
      curl_easy_setopt(curl, CURLOPT_WRITEHEADER, NULL);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, NULL);
   }

   return result;
}

// bug gitlab
void fix_reset_escape_sequence(char * output_buf,size_t o_read)
{
  char * walk=output_buf;
  walk[o_read]=0; // we read max one char less then buffer size!
  while (1) {
      char *p = strstr(walk,"\033[00m");
      // walked past last occurrence of needle; copy remaining part
      if (p == NULL) break;
      *(p+3)=';'; // change second 0 with ;
      walk=p+1;
  }
}

int  update_details(void * userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
     char * tmp_trace=cJSON_get_key(trace->params,"trace");
     fix_reset_escape_sequence(tmp_trace,strlen(tmp_trace));
   int response=1;
   struct MemoryStruct chunk;
   init_dynamicbuf( &chunk);

  ;
   CURLcode res;
   if(trace->curl)
   {
      curl_easy_setopt(trace->curl, CURLOPT_URL, trace->url);
      char *data=cJSON_Print(trace->params);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, data);

      // add JSON header
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, "accept: application/json");
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, headers);
      // change request to PUT
      curl_easy_setopt(trace->curl, CURLOPT_CUSTOMREQUEST, "PUT");

      /* send all data to this function  */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
      /* we pass our 'chunk' struct to the callback function */
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, (void *)&chunk);
      /* complete within 20 seconds */
      curl_easy_setopt(trace->curl, CURLOPT_TIMEOUT, 20L);
      // Do request
      res = curl_easy_perform(trace->curl);
      curl_slist_free_all(headers);
      long http_code = 0;
      curl_easy_getinfo (trace->curl, CURLINFO_RESPONSE_CODE, &http_code);
      if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         //Succeeded
         //printf("DONE PUT\n");
         if(strcmp(chunk.memory,"true")==0 || strcmp(chunk.memory,"null")==0)
            response=0;
         if(strcmp(chunk.memory,"false")==0)
            response=-1;
         //setdebug();
         if (response) debug( "update job #%10d: http 200, reply \"true\": >%s< \n",trace->job_id,chunk.memory);
         else debug( "update job #%10d: http 200, no reply \"true\": >%s< \n",trace->job_id,chunk.memory);
         //clrdebug();
      }
      else
      {
         if (res != CURLE_OK) error( "update job #%10d: failed: %s\n",trace->job_id,
                        curl_easy_strerror(res));
         else
         {
            error( "update job #%10d: HTTP %ld feedback: %s\n",trace->job_id,http_code,chunk.memory);
         }
         response=1;
      }

      if (data) free(data);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(trace->curl, CURLOPT_POSTFIELDS, NULL);
   }
   if (chunk.memory) free_dynamicbuf(&chunk.memory);
   return response;
}


// Progress of artifacts download
// set's data->progress to 1 if an update
// set's data->artifact json key progress int % progress value.
// scan_artifacts_update will use artifact
// scan progress will use data-> porgress
int progress_update_arti(void *userp,  curl_off_t dltotal, curl_off_t dlnow,
                         curl_off_t ultotal, curl_off_t ulnow)
{
   struct data_thread *data = (struct data_thread *)userp;
   CURL *curl = data->curl;
   double curtime = 0;
   curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &curtime);
   double lastruntime= data->lastruntime;
   if((curtime - lastruntime) >= 10)
   {
      data->lastruntime = curtime;
      debug( "DL TIME: %f => %d / %d \r\n", curtime,(int)dlnow,(int) dltotal);
      int progress=-1;
      if ((int) dltotal !=0)
      {
            progress=(100LL*(int)dlnow)/dltotal;
      }
      cJSON_GetObjectItem(data->artifact, "progress")->valueint=progress;
      data->progress=1;
   }
   return 0;
}

// for curl version < 0x072000 (7.32)
int progress_update_arti_oldcurl(void *userp,   double dltotal,   double dlnow,   double ultotal,   double ulnow)
{
   return progress_update_arti(userp, (curl_off_t)dltotal,
                               (curl_off_t)dlnow,
                               (curl_off_t)ultotal,
                               (curl_off_t)ulnow);
}

void *get_artifact(void *userp)
{
   FILE *fp;
   CURL *curl=NULL;
   struct data_thread *data = (struct data_thread *)userp;
   if (!data) {printf("no data structure to pull one url\n"); return 0;}
   else
      debug("start download %s\n",cJSON_get_key(data->artifact, "name"));
   if (data->error_count==0)
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("download"));
   else
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("retrying"));
   curl = curl_easy_init();

   if (curl)
   {
      data->lastruntime = 0;
      data->curl = curl;
      curl_easy_setopt(curl, CURLOPT_URL, cJSON_get_key(data->artifact, "api"));
      debug("url_artifact: %s ,headerAuth: %s\n",cJSON_get_key(data->artifact, "api"),cJSON_get_key( data->artifact, "header_auth"));
      struct curl_slist *headers = NULL;
      headers = curl_slist_append(headers, "Content-Type: application/json");
      headers = curl_slist_append(headers, cJSON_get_key( data->artifact, "header_auth"));
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

      curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);
      curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "GET");
      // Enable intruption for gracefull shutdown
      curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, &progress_update_arti_oldcurl);
      curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, userp);
   #if LIBCURL_VERSION_NUM >= 0x072000
       curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_update_arti);
       curl_easy_setopt(curl, CURLOPT_XFERINFODATA, userp);
   #endif
      /* enable progress meter */
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);

      int res=1;
      char filename[1024];
      char *path=data->path;
      char * name=cJSON_get_key(data->artifact, "name");
      if (!name || !path) error("no name or path %p %p",name,path);

      sprintf(filename, "%s/%s.zip",path, name);
      debug("curl download to %s\n",filename);

      long http_code = 0;
      fp = fopen(filename,"wb");
      if (fp==NULL) error("ERR: curl pull %s prob %s\n",filename,strerror(errno));
      else
      {
         //curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, 0);
         curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);

         res= curl_easy_perform(curl); /* ignores error */
         fclose(fp);
         curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);
         data->http_code=http_code;
      }
      curl_slist_free_all(headers);
      if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
      {
         cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("done"));
         debug("Finish download %s\n",cJSON_get_key(data->artifact, "name"));
      }
      else
      {
         data->error=1;
         if (res)
            debug( "curl failed for %s : %s\n",cJSON_get_key(data->artifact, "name"),curl_easy_strerror(res));
         else {
            debug("HTTP_code: %ld for %s\n",data->http_code,cJSON_get_key(data->artifact, "name"));
         }
         cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("error"));
      }
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
      curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
      curl_easy_setopt(curl, CURLOPT_POSTFIELDS, NULL);
      curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);
   #if LIBCURL_VERSION_NUM >= 0x072000
         curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, NULL);
         curl_easy_setopt(curl, CURLOPT_XFERINFODATA, NULL);
   #endif
      curl_easy_cleanup(curl);
      data->finish=1;
      return NULL;
   }
   else
   {
      data->error=1;
      cJSON_ReplaceItemInObject(data->artifact, "status", cJSON_CreateString("error"));
      error("No Curl init\n");
      return NULL;
   }
}

// Doesn work:

/*
Started POST "/api/v4/jobs/19715/artifacts/authorize" for 192.168.2.1 at 2017-08-17 13:51:11 +0700
JWT::DecodeError (Nil JSON web token):
/opt/gitlab/embedded/lib/ruby/gems/2.3.0/gems/jwt-1.5.6/lib/jwt.rb:120:in `decode'
/opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/workhorse.rb:166:in `decode_jwt'
/opt/gitlab/embedded/service/gitlab-rails/lib/gitlab/workhorse.rb:162:in `verify_api_request!'
/opt/gitlab/embedded/service/gitlab-rails/lib/api/runner.rb:172:in `block (2 levels) in <class:Runner>'

GET authorisation for artifacts

* About to connect() to gitlab.gioxa.com port 443 (#2)
*   Trying 171.101.61.55...
* Connected to gitlab.gioxa.com (171.101.61.55) port 443 (#2)
*   CAfile: /etc/pki/tls/certs/ca-bundle.crt
  CApath: none
* SSL connection using TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384
* Server certificate:
* 	subject: CN=gitlab.gioxa.com
* 	start date: Jul 25 00:56:00 2017 GMT
* 	expire date: Oct 23 00:56:00 2017 GMT
* 	common name: gitlab.gioxa.com
* 	issuer: CN=Let's Encrypt Authority X3,O=Let's Encrypt,C=US
> POST /api/v4/jobs/19715/artifacts/authorize HTTP/1.1
Host: gitlab.gioxa.com
Accept: * / *
CI_JOB_TOKEN: _Ftoqjy1znjJnVhLbLSe
Content-Length: 15
Content-Type: application/x-www-form-urlencoded

* upload completely sent off: 15 out of 15 bytes
< HTTP/1.1 500 Internal Server Error
< Server: nginx
< Date: Thu, 17 Aug 2017 06:51:11 GMT
< Content-Type: application/json
< Content-Length: 39
< Connection: keep-alive
< Cache-Control: no-cache
< Vary: Origin
< X-Request-Id: 7c60cc26-0291-4e23-a82e-d54044f1b345
< X-Runtime: 0.028801
<
* Connection #2 to host gitlab.gioxa.com left intact
HTTP_code: 500 for filesize 100
return data: {"message":"500 Internal Server Error"}

*/
int get_artifact_auth(cJSON * job)
{
   CURL *curl=NULL;
   struct MemoryStruct chunk;
   init_dynamicbuf( &chunk);
   debug("\n****************************************************************\nGET authorisation for artifacts\n\n");
   curl = curl_easy_init();
   char url[1024];
   sprintf(url,"%s/api/v4/jobs/%d/artifacts/authorize",cJSON_get_key(job, "ci_url"),cJSON_GetObjectItem(job, "id")->valueint);

   curl_easy_setopt(curl, CURLOPT_URL, url);

   char JOB_TOKEN[256];
   sprintf(JOB_TOKEN,"CI_JOB_TOKEN: %s",cJSON_get_key( job, "token"));
   struct curl_slist *headers = NULL;
//   headers = curl_slist_append(headers, "Content-Type: application/json");
   headers = curl_slist_append(headers, JOB_TOKEN);
//   headers = curl_slist_append(headers, "accept: application/json");
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

   curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");

   // create params cJSON struct

   //cJSON * params=cJSON_CreateObject();
   //cJSON_AddNumberToObject(params, "filesize", 100);
   //char * params_str=cJSON_PrintUnformatted(params);
   //cJSON_Delete(params);

   long http_code = 0;
   //debug("params: %s\n",params_str);
   // post data
   curl_easy_setopt(curl, CURLOPT_POSTFIELDS, "filesize=202022");
   /* send all data to this function  */
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
   /* we pass our 'chunk' struct to the callback function */
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
   /* complete within 20 seconds */
   curl_easy_setopt(curl, CURLOPT_TIMEOUT, 20L);

   int res= curl_easy_perform(curl); /* ignores error */
   int result_f=0;

   curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

   if (http_code == 200 && res != CURLE_ABORTED_BY_CALLBACK)
   {
      //debug("return data: %s\n",chunk.memory);
      result_f=0;
   }
   else
   {
     result_f=res;
      if (res)
         debug( "curl failed to get artifacys authorisation with filesize %d",100);
      else {
         debug("HTTP_code: %ld for filesize %d\n",http_code,100);
         error("Err return data: %s\n",chunk.memory);
         result_f=(int)http_code;
      }
   }

  //if (params_str) free(params_str);
  curl_slist_free_all(headers);
  if (chunk.memory) free_dynamicbuf(&chunk.memory);
  curl_easy_cleanup(curl);
   return result_f;
}


int upload_artifact(cJSON * job, char * upload_name)
{
   CURL *curl=NULL;
   struct MemoryStruct chunk;
   init_dynamicbuf( &chunk);
   debug("\n****************************************************************\nUpLoad artifacts\n\n");
   curl = curl_easy_init();
   char url[1024];
   sprintf(url,"%s/api/v4/jobs/%d/artifacts",cJSON_get_key(job, "ci_url"),cJSON_GetObjectItem(job, "id")->valueint);

   curl_easy_setopt(curl, CURLOPT_URL, url);

   char JOB_TOKEN[256];
   sprintf(JOB_TOKEN,"JOB-TOKEN: %s",cJSON_get_key( job, "token"));
   struct curl_slist *headers = NULL;
  // headers = curl_slist_append(headers, "Content-Type: application/json");
   headers = curl_slist_append(headers, JOB_TOKEN);
   headers = curl_slist_append(headers, "expect:");
   headers = curl_slist_append(headers, "accept: application/json");
   //headers = curl_slist_append(headers,"expect:");
   curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

   curl_easy_setopt(curl, CURLOPT_VERBOSE, 0L);

   curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "POST");

 struct curl_httppost *post=NULL;
 struct curl_httppost *last=NULL;
cJSON * artifacts= cJSON_GetArrayItem( cJSON_GetObjectItem(job,"artifacts"),0);

 if (cJSON_get_key(artifacts,"expire_in"))
 {
   debug("expire_in %s\n",cJSON_get_key(artifacts,"expire_in"));
 curl_formadd(&post, &last,
              CURLFORM_COPYNAME, "expire_in",
              CURLFORM_COPYCONTENTS, cJSON_get_key(artifacts,"expire_in"), CURLFORM_END);
}
else
{
  debug("expire_in [default] 1w\n");
curl_formadd(&post, &last,
             CURLFORM_COPYNAME, "expire_in",
             CURLFORM_COPYCONTENTS, "1w", CURLFORM_END);
}
 //curl_formadd(&post, &last,
//            CURLFORM_COPYNAME, "file",
//            CURLFORM_COPYCONTENTS, "artifacts.zip", CURLFORM_END);

char filename[1024];
if (cJSON_get_key(artifacts,"name"))
{
  sprintf(filename,"%s.zip",cJSON_get_key(artifacts,"name"));
}
else
{
  sprintf(filename,"artifacts.zip");
}
debug("filename=%s\n",filename);
    curl_formadd(&post, &last,
                CURLFORM_COPYNAME, "file",
                CURLFORM_CONTENTTYPE, "application/zip",
                CURLFORM_FILE,  upload_name,
                CURLFORM_FILENAME,filename, CURLFORM_END);

              /* Set the form info */
 curl_easy_setopt(curl, CURLOPT_HTTPPOST, post);

   long http_code = 0;

   /* send all data to this function  */
   curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteMemoryCallback);
   /* we pass our 'chunk' struct to the callback function */
   curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)&chunk);
   /* complete within 20 seconds */
   curl_easy_setopt(curl, CURLOPT_TIMEOUT, 1200L);

   int res= curl_easy_perform(curl);
   int result_f=0;
   /* free the post data again */
   curl_formfree(post);
   curl_easy_getinfo (curl, CURLINFO_RESPONSE_CODE, &http_code);

   if (http_code == 201 && res != CURLE_ABORTED_BY_CALLBACK)
   {
      //debug("return data: %s\n",chunk.memory);
      result_f=0;
   }
   else
   {
     result_f=res;
      if (res)
         debug( "curl failed to get artifacys authorisation with filesize %d",100);
      else {
         error("HTTP_code: %ld for upload artifact \n",http_code);
         error("return data: %s\n",chunk.memory);
         result_f=(int)http_code;
      }
   }
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, NULL);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION,NULL);
  curl_easy_setopt(curl, CURLOPT_HTTPPOST, NULL);
  curl_easy_setopt(curl, CURLOPT_HTTPHEADER, NULL);
  //if (params_str) free(params_str);
  curl_slist_free_all(headers);
  if (chunk.memory) free_dynamicbuf(&chunk.memory);
  curl_easy_cleanup(curl);
   return result_f;
}
