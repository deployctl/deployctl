/*
 svg_badge.c
 Created by Danny Goossen, Gioxa Ltd on 6/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include "deployd.h"



static const char * svg= "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"@@c@@\" height=\"20\">  <linearGradient id=\"b\" x2=\"0\" y2=\"100%\">" \
"<stop offset=\"0\" stop-color=\"#bbb\" stop-opacity=\".1\"/>  <stop offset=\"1\" stop-opacity=\".1\"/>" \
" </linearGradient><mask id=\"a\">  <rect width=\"@@c@@\" height=\"20\" rx=\"3\" fill=\"#fff\"/>  </mask> <g mask=\"url(#a)\"> " \
" <path fill=\"#555\"    d=\"M0 0 h@@a@@ v20 H0 z\"/>  <path fill=\"#4c1\"    d=\"M@@a@@ 0 h@@b@@ v20 H@@a@@ z\"/>  <path fill=\"url(#b)\"" \
" d=\"M0 0 h@@c@@ v20 H0 z\"/>  </g>  <g fill=\"#fff\" text-anchor=\"middle\">  <g font-family=\"DejaVu Sans,Verdana,Geneva,sans-serif\" font-size=\"11\"> " \
" <text x=\"@@d@@\" y=\"15\" fill=\"#010101\" fill-opacity=\".3\">@@f@@</text><text x=\"@@d@@\" y=\"14\">@@f@@</text> " \
" <text x=\"@@e@@\" y=\"15\" fill=\"#010101\" fill-opacity=\".3\">@@g@@</text>  <text x=\"@@e@@\" y=\"14\">@@g@@</text>  </g>  </g></svg>";


/*
 svg placements_stubs:

 @@a@@ = strlen(@@f@@) * 7 + 3
 @@b@@ = strlen(@@g@@) * 7 + 5
 @@c@@ = @@a@@ + @@b@@

 @@d@@ = @@a@@ / 2
 @@e@@ = @@a@@ + @@b@@ / 2

 @@f@@ = "TAG"
 @@g@@ = "0.0.1"

 */

/*------------------------------------------------------------------------
 * Creates a release badge
  +-------+-------+
  | str_a | str_b |
  +-------+-------+
 * auto calculates the witdh
 * returns 1 on success
 * caller need to free *badge_str
 *------------------------------------------------------------------------*/

int make_svg_badge(char * str_a, char * str_b, char ** badge_str) {

    struct MemoryStruct output;
    init_dynamicbuf( &output);
    *badge_str=NULL;

    //Calculate size of badge according the str_a and str_b size
    size_t len_a= strlen(str_a)*9+3; // @@a@@
    size_t len_b= strlen(str_b)*9+3; // @@b@@
    size_t len_c= len_a+len_b; // @@c@@
    size_t pos_a= len_a/2;  //@@d@@
    size_t pos_b= len_a+len_b/2;  //@@e@@

    regex_t regex;
    int reti;
    size_t prev_end=0;
    char regex_str[]= "@@([a-z])@@";

    int regex_match_cnt=2+1; // ( 2 matches in above expression: from @@..@@ and inbtween the @@, and 1 position to store the -1 for end.)
    char * subject=(char*)svg;

    regmatch_t   ovector[3];
    //regmatch_t  * ovector= calloc(sizeof(regoff_t)*2,regex_match_cnt);

    /* Compile regular expression */
    debug("make tag badge %s:%s\n",str_a,str_b);
    //reti =
	regcomp(&regex, regex_str, REG_EXTENDED);
    //if (reti) { // no need to test here, it'll fail later as well
    //    error( "Could not compile regex\n");
    //    return -1;
    //}

    /* Execute regular expression in loop */
    for (;;)
    {
        reti = regexec(&regex, subject+prev_end, regex_match_cnt, ovector, 0);
        if (!reti)
        {
            //puts("Match");
            int i=0;
            while (ovector[i].rm_so!=-1 && i<5)
            {
               // debug("found at %lld to %lld\n", ovector[i].rm_so+prev_end,ovector[i].rm_eo+prev_end);
                i++;
            }
            if (ovector[1].rm_eo !=-1 && ovector[0].rm_so !=-1 && (ovector[1].rm_eo-ovector[1].rm_so) == 1)
            {
                Writedynamicbuf_n((void *)(subject+ prev_end), ovector[0].rm_so, &output);

                // since we use here only 1 char as named substring, we'll have that in ovector[2]
                char stub_code=subject[prev_end+ovector[1].rm_so];
                prev_end=prev_end+ovector[0].rm_eo;
                switch (stub_code) {
                    case 'a':
                    {
                        char tmp[20];
                        sprintf(tmp,"%zu", len_a);
                        Writedynamicbuf(tmp,&output);
                        break;
                    }
                    case 'b':
                    {
                        char tmp[20];
                        sprintf(tmp,"%zu", len_b);
                        Writedynamicbuf(tmp,&output);
                        break;
                    }
                    case 'c':
                    {
                        char tmp[20];
                        sprintf(tmp,"%zu", len_c);
                        Writedynamicbuf(tmp,&output);
                        break;
                    }
                    case 'd':
                    {
                        char tmp[20];
                        sprintf(tmp,"%zu", pos_a);
                        Writedynamicbuf(tmp,&output);
                        break;
                    }
                    case 'e':
                    {
                        char tmp[20];
                        sprintf(tmp,"%zu", pos_b);
                        Writedynamicbuf(tmp,&output);
                        break;
                    }
                    case 'f':
                    {
                        Writedynamicbuf(str_a,&output);
                        break;
                    }
                    case 'g':
                    {
                        Writedynamicbuf(str_b,&output);
                        break;
                    }
                    default:
                        error("svg, regex unknown stub code %c @ pos : %d",subject[prev_end+ovector[1].rm_so],prev_end+ovector[1].rm_so);break;
                }
            }

        }
        else if (reti == REG_NOMATCH) {
            // copy rest from lastmatch end to end of subject
            Writedynamicbuf_n((void *)(subject+ prev_end), strlen(subject)-prev_end, &output);
            break;
        }
        else { prev_end=0;break; }
            // no need to change, regex is not variable
            //regerror(reti, &regex,errorbuf, 1024);
            //error( "Regex match failed: %s\n", errorbuf);
    }

    /* Free memory allocated to the pattern buffer by regcomp() */
    regfree(&regex);
    if (prev_end)
    {
        *badge_str=output.memory;
        //debug ("svg tag size %d >>>>>\n%s\n<<<<\n",output.size,output.memory);
        debug("success parsing tag.svg, len: %d \n",strlen(output.memory));
        return 0;
    }
    else
    {
        if (output.memory) {free(output.memory); *badge_str=NULL;}return -1;
    }
}
