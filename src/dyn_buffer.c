/*
 dyn_buffer.c
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include "deployd.h"

const char * color_data[]={ANSI_BOLD_BLACK,ANSI_BOLD_RED,ANSI_BOLD_GREEN,ANSI_BOLD_YELLOW,ANSI_BOLD_BLUE,ANSI_BOLD_MAGENTA,ANSI_BOLD_CYAN,ANSI_BOLD_WHITE, \
                           ANSI_BLACK,ANSI_RED,ANSI_GREEN,ANSI_YELLOW,ANSI_BLUE,ANSI_MAGENTA,ANSI_CYAN,ANSI_WHITE, \
                           ANSI_CROSS_BLACK,ANSI_CROSS_RED,ANSI_CROSS_GREEN,ANSI_CROSS_YELLOW,ANSI_CROSS_BLUE,ANSI_CROSS_MAGENTA,ANSI_CROSS_CYAN,ANSI_CROSS_WHITE, \
                           ANSI_RESET};



/*------------------------------------------------------------------------
 * Helper function for Curl request
 *------------------------------------------------------------------------*/
size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
       // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
       // LCOV_EXCL_STOP

    }
    else
        mem->memory=tmp;

    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;
    return realsize;
}

void init_header_struct( void *userp)
{
   struct headerStruct * s_header=(struct headerStruct*)userp;
   s_header->header=cJSON_CreateObject();
}
void free_header_struct( void *userp)
{
   struct headerStruct * s_header=(struct headerStruct*)userp;
   if (s_header->header) cJSON_Delete(s_header->header);
   s_header->header=NULL;
}




/*------------------------------------------------------------------------
 * Helper function for Curl request
 *------------------------------------------------------------------------*/
size_t WriteHeaderCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
   size_t realsize = size * nmemb;
   struct headerStruct * s_header=(struct headerStruct*)userp;

   char * e=contents;
   int len=(int)realsize;
   int pos = 0;
   while (*e++ != ':' && pos < len) pos++;
   if (pos != len && pos >1)
   {

         cJSON_AddItemToObject_n(s_header->header, contents, pos, cJSON_CreateString_n(contents+pos+2, len-pos-4));

   }
   return realsize;
}



/*------------------------------------------------------------------------
 * Helper function append string to dynamic trace
 *------------------------------------------------------------------------*/
int init_dynamic_trace(void**userp, const char * token, const char * url, int job_id)
{
   struct trace_Struct **trace_p = (struct trace_Struct **)userp;
   if (!trace_p || ! token || !url) return -1;
   if (*trace_p) free_dynamic_trace(userp);
   *trace_p=NULL;

   (*trace_p)=calloc(1, sizeof(struct trace_Struct));
   if (*trace_p)
   {
      (*trace_p)->url=strdup(url);
      (*trace_p)->job_id=job_id;
      (*trace_p)->params = cJSON_CreateObject();
      cJSON_AddStringToObject((*trace_p)->params, "state", "running");
      cJSON_AddStringToObject((*trace_p)->params, "token", token);
      cJSON * tmp=cJSON_CreateObject();
      tmp->valuestring=calloc(1,0x10000);
      if (tmp)
      {
         tmp->type=cJSON_String;
         cJSON_AddItemToObject((*trace_p)->params, "trace", tmp);
         (*trace_p)->trace_len=0;
         (*trace_p)->trace_buf_size=0x10000; // 64K initial buffer size
         (*trace_p)->curl=curl_easy_init();;
         (*trace_p)->tests=NULL;
         (*trace_p)->mark=0;
         (*trace_p)->min_trace_buf_increment=0x4000; //increment with minimun 16K
      }
      else
      {
         cJSON_Delete( (*trace_p)->params);
         if ((*trace_p)->url) free((*trace_p)->url);
         (*trace_p)->url=NULL;
         free(*trace_p);
         (*trace_p)=NULL;
      }
   }
   if (*trace_p) debug("trace->params ptr %p\n",(*trace_p)->params);
   return 0;
}

size_t Write_dynamic_trace_n(const void *contents,size_t n, void *userp)
{
  struct trace_Struct *trace = (struct trace_Struct *)userp;
  if (contents && n>0)
  {
    cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
    if (item && item->valuestring)
    {
       if (trace->trace_len+1+n> trace->trace_buf_size)
       {
          size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n);
          char * temp=realloc(item->valuestring,newsize);
          if (temp )
          {
            trace->trace_buf_size=newsize;
            if (temp!=item->valuestring)
            {
              item->valuestring=temp;
            }
         }
         else
         {
            error("Write_dyn_trace_n, no more mem");
            return -1;
         }
       }
       memcpy(item->valuestring+trace->trace_len, contents, n);
       trace->trace_len+=n;
       item->valuestring[trace->trace_len]=0;
  }
  else error("Write_dyn_trace_n, cjson trace problem\n");
  }
  return n;
}

size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...)
{
   
   
   if (!userp || !message|| strlen(message)==0) return -1;
   va_list varg;
   va_start (varg, message);
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   size_t consten_buf_len=0;
   char * contents=NULL;
   // print into temp buffer and adjust buffer till content fits
   
   size_t cnt=0;
   int res=0;
   do {
      cnt++;
      if (contents) free(contents);
      contents=NULL;
      contents=(char *)calloc(cnt,trace->min_trace_buf_increment);
      res=-1;
      if (contents)
      {
         consten_buf_len=(size_t)(trace->min_trace_buf_increment * cnt);
      	res=vsnprintf(contents,consten_buf_len ,message,varg);
      }
      else
         break;
   } while ((size_t)res >= consten_buf_len);
   
   va_end(varg);
   
   if (res<0)
   {
      error("Buffer error contents\n");
      if (contents) free (contents);
      contents=NULL;
   }
   size_t n;
   // with content and length, check if trace buffer needs to be increased
   if (contents)
   {
      // content ready, let's calculate the length
      if (color!= none)
         n= strlen(contents) + strlen(color_data[color]) + strlen(ANSI_RESET);
      else
         n= strlen(contents);
      
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         // increase buffer if needed
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               if (contents) free(contents);
               return -1;
            }
         }
         // append contents
         if (color!= none)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s",color_data[color],contents,ANSI_RESET);
         else
            memcpy(item->valuestring+trace->trace_len, contents, n);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   if (contents) free(contents);
   return n;
}


size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...)
{
   if (!userp || !message|| strlen(message)==0) return -1;
   
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   size_t consten_buf_len=0;
   char * contents=NULL;
   // print into temp buffer and adjust buffer till content fits
   va_list varg;
   va_start (varg, message);
   int cnt=0;
   int res=0;
   do {
      cnt++;
      consten_buf_len=trace->min_trace_buf_increment * cnt;
      if (contents) free(contents);
      contents=NULL;
      contents=calloc(1,consten_buf_len);
      res=-1;
      if (contents)
         res=vsnprintf(contents,consten_buf_len ,message,varg);
      else
         break;
   } while((size_t)res >= consten_buf_len);
   va_end(varg);
   
   if (res<0)
   {
      error("Buffer error contents\n");
      if (contents) free (contents);
      contents=NULL;
   }
   size_t realsize=-1;
   // with content and length, check if trace buffer needs to be increased
   if (contents)
   {
      
      realsize = strlen(contents);
      char pad[]="                                                                                                                                                                       ";
      if (realsize<=len2pad) pad[len2pad-realsize]=0;
      else
      { // trim contents
         contents[len2pad]=0;
         if (len2pad>2)
         {
            contents[len2pad-1]='.';
            contents[len2pad-2]='.';
         }
         pad[0]=0;
      }
      size_t n;
      // content ready, let's calculate the length
      if (color!= none)
         n= strlen(contents) + strlen(color_data[color]) + strlen(ANSI_RESET) + strlen(pad);
      else
         n= strlen(contents)+strlen(pad);
      
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         // increase buffer if needed
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               if (contents) free(contents);
               return -1;
            }
         }
         // append contents with padding to the buffer
         if (color!= none)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s%s",color_data[color],contents,ANSI_RESET,pad);
         else
            sprintf(item->valuestring+trace->trace_len,"%s%s",contents,pad);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   if (contents) free(contents);
   return realsize;
}

size_t Write_dynamic_trace_color(const void *contents,char * state, enum term_color color, void *userp)
{
   if (!userp || !contents) return -1;
   
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   
   size_t n;
   size_t realsize=-1;
   realsize=strlen(contents);
   // with content and length, check if trace buffer needs to be increased
   if (contents)
   {
      // content ready, let's calculate the length
      if (color!= none)
         n= strlen(contents) + strlen(color_data[color]) + strlen(ANSI_RESET);
      else
         n= strlen(contents);
      
      cJSON * item=cJSON_GetObjectItem(trace->params, "trace" );
      if (item && item->valuestring)
      {
         // increase buffer if needed
         if (trace->trace_len+1+n> trace->trace_buf_size)
         {
            size_t newsize=trace->trace_buf_size+ MAX(trace->min_trace_buf_increment, n);
            char * temp=realloc(item->valuestring,newsize);
            if (temp )
            {
               trace->trace_buf_size=newsize;
               if (temp!=item->valuestring)
               {
                  item->valuestring=temp;
               }
            }
            else
            {
               error("Write_dyn_trace_n, no more mem");
               return -1;
            }
         }
         // append contents
         if (color!= none)
            sprintf(item->valuestring+trace->trace_len,"%s%s%s",color_data[color],contents,ANSI_RESET);
         else
            memcpy(item->valuestring+trace->trace_len, contents, n);
         trace->trace_len+=n;
         item->valuestring[trace->trace_len]=0;
      }
      else error("Write_dyn_trace_n, cjson trace problem\n");
   }
   if (state)
   {
      if (cJSON_get_key(trace->params, "state"))
         cJSON_ReplaceItemInObject(trace->params, "state", cJSON_CreateString(state));
      else
         cJSON_AddItemToObject(trace->params, "state", cJSON_CreateString(state));
   }
   return realsize;
}




size_t Write_dynamic_trace(const void *contents,char * state, void *userp)
{
   size_t realsize = strlen(contents);
   return Write_dynamic_trace_n(contents, realsize, userp);
}


/*------------------------------------------------------------------------
 * Helper function get trace string
 *------------------------------------------------------------------------*/
const char * get_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   return cJSON_get_key(trace->params, "trace");
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t clear_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
   if (tmp)
   {
      tmp->valuestring[0]=0;
      trace->trace_len=0;
      trace->mark=-1;
   }
   else
      error("could not get trace\n");
   return 0;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_till_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   if (trace->mark!=-1 && trace->trace_len>0 && trace->trace_len>trace->mark)
   {
      cJSON * tmp=cJSON_GetObjectItem( trace->params, "trace");
      if (tmp)
      {
         tmp->valuestring[trace->mark]=0;
         trace->trace_len=trace->mark;
      }
      else
         error("could not get trace\n");
   }
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void set_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=trace->trace_len;
   return;
}
/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void clear_mark_dynamic_trace( void *userp)
{
   struct trace_Struct *trace = (struct trace_Struct *)userp;
   trace->mark=-1;
   return;
}

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
void free_dynamic_trace(void **userp)
{
   if (*userp)
   {
      struct trace_Struct **trace_p = (struct trace_Struct **)userp;
      if ((*trace_p)->params)cJSON_Delete( (*trace_p)->params);
      (*trace_p)->params=NULL;
      if ((*trace_p)->url) free((*trace_p)->url);
      (*trace_p)->url=NULL;
      if ((*trace_p)->curl) curl_easy_cleanup((*trace_p)->curl);
      (*trace_p)->curl=NULL;
      (*trace_p)->mark=-1;
      free (*trace_p);
      (*trace_p)=NULL;
   }
   return;
}


// START Dynbuffer

/*------------------------------------------------------------------------
 * Helper function write string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t Writedynamicbuf(void *contents, void *userp)
{
    size_t realsize = strlen(contents);
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
      // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
      // LCOV_EXCL_STOP
    }
    else
        mem->memory=tmp;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

/*------------------------------------------------------------------------
 * Helper function write non NULL string to dynamic buffer
 *------------------------------------------------------------------------*/
size_t Writedynamicbuf_n(void *contents, size_t content_len, void *userp)
{
    size_t realsize = content_len;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    void * tmp= realloc(mem->memory, mem->size + realsize + 1);
    if(tmp == NULL) {
        // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return 0;
        // LCOV_EXCL_STOP
    }
    else
        mem->memory=tmp;


    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

/*------------------------------------------------------------------------
 * intializes the dynamic buff
 *------------------------------------------------------------------------*/
size_t init_dynamicbuf( void *userp)
{
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    mem->memory = malloc( 1);
    mem->size=0;
    if(mem->memory == NULL) {
        // LCOV_EXCL_START
        /* out of memory! */
       // error("not enough memory (realloc returned NULL)\n");
       return -1;
        // LCOV_EXCL_STOP
    }
    mem->memory[mem->size] = 0;
    return 0;
}
/*------------------------------------------------------------------------
 * intializes the dynamic buff
 *------------------------------------------------------------------------*/
void free_dynamicbuf( void *userp)
{
   struct MemoryStruct *mem = (struct MemoryStruct *)userp;

   free(mem->memory);
   mem->memory=NULL;
   mem->size=-1;
     return ;
}
