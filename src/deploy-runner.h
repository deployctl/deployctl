//
//  deploy-runner.h
//  deployctl
//
//  Created by Danny Goossen on 27/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef deployctl_deploy_runner_h
#define deployctl_deploy_runner_h
#include "common.h"


int runners(parameter_t * parameters);
//int do_job(cJSON * runner,cJSON* job,parameter_t * parameters );
int do_job(cJSON * runner,cJSON* job,parameter_t * parameters);
int  update_details(void * userp);

#endif
