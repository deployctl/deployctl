/*
 exec.c
 Created by Danny Goossen, Gioxa Ltd on 4/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



#include "deployd.h"

#define PIPE_READ 0
#define PIPE_WRITE 1

struct script_control {
   const char *shell;		/* shell to be executed */
   const char *shellcommand;		/* command to be executed */
   int master;		/* pseudoterminal master file descriptor */
   int slave;		/* pseudoterminal slave file descriptor */
   pid_t child;
   #if !HAVE_LIBUTIL
      char *line;		/* terminal line */
   #else
      int close_pipe;
   #endif
   int childstatus;	/* child process exit value */
   //char ** envp;
   cJSON * env_vars;
   gid_t gid;
   uid_t uid;
   int timeout;

   unsigned int
change_ugid:1,
need_envp:1,
die:1;			/* terminate program */
};


int socket_nonblocking (int sock)
{
   int flags;

   if(sock != INVALID_SOCKET)
   {
      flags = fcntl (sock, F_GETFL, 0);
      return fcntl (sock, F_SETFL, flags | O_NONBLOCK | O_CLOEXEC | SO_KEEPALIVE);//| O_NDELAY);
   }
   return -1;

}
/*
 * Set the socket to blocking -rjkaes
 */
int socket_blocking (int sock)
{
   int flags;

   if(sock != INVALID_SOCKET)
   {
      flags = fcntl (sock, F_GETFL, 0);
      return fcntl (sock, F_SETFL, (flags & ~O_NONBLOCK) | O_CLOEXEC);// | O_NDELAY);
   }
   return -1;
}

int socket_O_CLOEXEC (int sock)
{
   int flags;

   if(sock != INVALID_SOCKET)
   {
      flags = fcntl (sock, F_GETFL, 0);
      return fcntl (sock, F_SETFL, flags | O_CLOEXEC);
   }
   return -1;

}
/*------------------------------------------------------------------------
 * Helper functions for uid and gid
 *------------------------------------------------------------------------*/

int get_uid_gid_user(uidguid_t * uidguid,const char * user)
{
    struct passwd pwd;
    struct passwd *result;
    char *buf;
    size_t bufsize;
    int s;

    bufsize = sysconf(_SC_GETPW_R_SIZE_MAX);
    if (bufsize == -1)          /* Value was indeterminate */
        bufsize = 16384;        /* Should be more than enough */

    buf = malloc(bufsize);
    if (buf == NULL) { error("malloc pwd\n");return -1;}

    s = getpwnam_r(user, &pwd, buf, bufsize, &result);
    free(buf);
    if (result == NULL) {
        if (s == 0) error("user %s not found\n",user);
        else {errno = s; sock_error("getpwnam_r");}
        return 1;
    }
    uidguid->gid=pwd.pw_gid;
    uidguid->uid=pwd.pw_uid;
    return 0;
}

int get_gid(gid_t * gid,const char* user)
{
    uidguid_t uidguid;
    int ret=get_uid_gid_user(&uidguid,user);
    if (!ret)*gid=uidguid.gid;
    return ret;
}
int get_uid(uid_t * uid,const char*user)
{
    uidguid_t uidguid;
    int ret=get_uid_gid_user(&uidguid,user);
    if (!ret) *uid=uidguid.uid;
    return ret;
}

int exec_cmd_col(void * opaque,int cmd);

/*----------------------------------------------------------------------------------
 * int cmd_exec()
 * fork and executes the a command as per ((data_exchange_t *)opaque)->paramlist
 * and optional environment a per ((data_exchange_t *)opaque)->newenvp
 * * with gid and uid as per data_exchange.gid/uid
 * with umask according to umask
 *
 * blocking function with timeout while waiting for feedback and exit-code
 *
 * output's the cmd stdout and stderr in output_buff and returns exit code for client
 *-----------------------------------------------------------------------------------*/
int cmd_exec(void * opaque)
{
  return(exec_cmd_col(opaque,2));
}

/*----------------------------------------------------------------------------------
 * int cmd_write()
 * fork and executes a write file command as per
 * and optional environment a per ((data_exchange_t *)opaque)->newenvp
 * with gid and uid as per data_exchange.gid/uid
 * with umask according to umask
 *
 * blocking function with timeout while waiting for feedback and exit-code
 *
 * output's the cmd stdout and stderr in output_buff and returns exit code for client
 *-----------------------------------------------------------------------------------*/
int cmd_write(void * opaque)
{
  return(exec_cmd_col(opaque,1));
}

/*----------------------------------------------------------------------------------
 * int exec_col(void * opaque,int cmd)
 * fork and executes a wcmd_write or cmd_exec according to cmd
 * and optional environment a per ((data_exchange_t *)opaque)->newenvp
 * with gid and uid as per data_exchange.gid/uid
 * with umask according to umask
 * blocking function with timeout while waiting for feedback and exit-code
 *
 * output's the cmd stdout and stderr in output_buff and returns exit code for client
 *-----------------------------------------------------------------------------------*/
int exec_cmd_col(void * opaque,int cmd_choice)
{
   debug("start command exec\n");

    data_exchange_t * data_exchange=((data_exchange_t *)opaque);
    // feedback buffer
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;

    // new envp
    cJSON * env_vars=data_exchange->env_json;

    // get the details for the command
    char ** paramlist =data_exchange->paramlist;

    int needenvp=data_exchange->needenvp;
    gid_t gid=data_exchange->gid;
    uid_t uid=data_exchange->uid;

    pid_t child_pid;

    int aStdoutPipe[2];
    if (pipe(aStdoutPipe) < 0) {sock_error("allocating pipe for child stdout redirect");Write_dyn_trace(trace, red,"System error\n");return(15);}
    // Fork_it
      fflush(stdout);
    child_pid = fork();

   if(child_pid == 0)
   {
      // The Child
      // redirect stdout
      if (dup2(aStdoutPipe[PIPE_WRITE], STDOUT_FILENO) == -1) {sock_error("redirecting stdout");exit(2);}
      // redirect stderr
      if (dup2(aStdoutPipe[PIPE_WRITE], STDERR_FILENO) == -1) {sock_error("redirecting stderr");exit(3);}

      int fd = open("/dev/null", O_WRONLY);
      if (dup2( fd , STDIN_FILENO) == -1) {sock_error("redirecting stdin");exit(3);}
      // all these are for use by parent only, so close

      close(aStdoutPipe[PIPE_READ]);
      close(aStdoutPipe[PIPE_WRITE]);
      // set user to root and group to the requested user

      if (!data_exchange->current_user && setgid(gid)) { error("could not set gid\n") ; exit(4);}
      if (!data_exchange->current_user && setuid(uid)) { error("could not set uid\n") ; exit(5);}

      // user=7 rwx , group=7 r--, other: ---
      umask( S_IRWXO ); /* set newly created file permissions */

      //printf("%s : %s %s\n", paramlist[0],paramlist[1],paramlist[2]);

      if (cmd_choice==1)
      {
         // write to file
         FILE *f = fopen(paramlist[1], "w");
         if (f == NULL){ fprintf(stderr,"Error opening file : %s\n",strerror(errno));exit(8);}
         /* print some text */
         int retvalue=0;
         int errvalue=0;
         do{ retvalue=fprintf(f, "%s",paramlist[2]);} while ( retvalue==-1 && (errvalue=errno)== EINTR );
         if (retvalue<0) { fprintf(stderr,"Error io-command %s: %s\n", paramlist[0],strerror(errvalue)); exit (9); }
         if (fclose(f) <0 ) { fprintf(stderr,"Error io-command %s: %s\n", paramlist[0],strerror(errno));exit (10);}
         exit(0);
      }
      // LCOV_EXCL_START
      else if (cmd_choice==2)

      // does get tested, but no registration since we never exit here
      {
         if (needenvp && env_vars && cJSON_GetArraySize(env_vars)>0)
         {
            cJSON * pos=NULL;
            cJSON_ArrayForEach(pos, env_vars)
            {
               if (cJSON_IsString(pos))
               {
                  setenv(pos->string,pos->valuestring,1);
                  debug("env: %s=%s\n",pos->string,pos->valuestring);
               }
               else debug("skipped non string %s\n",pos->string);
            }
            //cJSON_Delete(env_vars);
            //env_vars=NULL;
         }

         {
            execvp(paramlist[0], paramlist);
            //printf("what's wrong here???\n");
         }
      // just for safety, should never come here
      _exit(6);
      } else exit(7);
   // LCOV_EXCL_STOP
   }
   else if (child_pid>0) // The Parent
   {
     // parent wait's and return result
     // close unused file descriptors, these are for child only

      close(aStdoutPipe[PIPE_WRITE]);
      char output_buf[0x10000];
      fd_set rds;
      int sfd=aStdoutPipe[PIPE_READ];
      int s_ret=0;
      int s_err=0;
      ssize_t o_read=0;
      int o_r_error=0;
      struct timeval tv;
      tv.tv_sec = ((data_exchange_t *)opaque)->timeout;
      //tv.tv_sec = TIME_OUT_CHILD;
      tv.tv_usec = 0;
      int timeout=0;
      socket_nonblocking(aStdoutPipe[PIPE_READ]);
      do
      {
      FD_ZERO( &rds );
      FD_SET( aStdoutPipe[PIPE_READ], &rds );
      s_ret=select(sfd+1, &rds, NULL, NULL, &tv);
      if (s_ret==SOCKET_ERROR) s_err=errno;
      else if (s_ret>0)
      {
         o_read=read(aStdoutPipe[PIPE_READ], output_buf, 0x10000);
         if (o_read==0) break;
         else if (o_read >0)
            //Write_dyn_trace(trace, none,"%.*s",o_read,output_buf);
            Write_dynamic_trace_n (output_buf,o_read,trace);
         else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("std_out pipe read", o_r_error);break; }
      }
      else
      {
         Write_dyn_trace(trace, none,"System Error: timeout executing command!!!");
         exitcode=1;
         timeout=1;
         break;
      }
      } while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
      close(aStdoutPipe[PIPE_READ]);
      usleep(10000);
      int status;
      if (timeout)
      {
         debug("Timeout, kill child\n");
         kill(child_pid,SIGKILL); // TODO need to check with WHOANG
      }
      pid_t w=0;
      int this_error=0;
      while ((w= waitpid(child_pid,&status, 0)) ==-1 && (this_error=errno)==EINTR )
      {
         debug("exec: interupted Syscall\n");

      }
      if (w==-1) {
         sock_error_no("waitpid",this_error);
         if (!exitcode) Write_dyn_trace(trace, none,"System error: error on 'waitpid()': %s!!!",strerror(this_error));
         exitcode=1;
      }
      else if (WIFEXITED(status))
      {
         exitcode=WEXITSTATUS(status);
         if (exitcode)
         {
             debug("exit cmd: %s w error: %d\n",paramlist[0], exitcode);
         }
         else
         {
             debug("[OK]\n");
             //Write_dyn_trace(trace, green,"\t[OK]\n");
         }
      }
      else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
         alert("command stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal(WSTOPSIG(status)));
         exitcode=1;
         if (!timeout)
            Write_dyn_trace(trace, red,"\ncommand stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal(WSTOPSIG(status)));
      }
      else
      {    /* Non-standard case -- may never happen */
      error("Unexpected status child (0x%x)\n", status);exitcode=status;}
   }
    else
    {
        error("Fork failed \n"); Write_dyn_trace(trace, red,"\nsystem Error FORK failed\n");exitcode=1;}
    return (exitcode);
}

/*----------------------------------------------------------------------------------
 * ibash_it(char ** output_buf,int buff_len, const char* valuestring ,cJSON * env_vars,int unpriviledged)
 * fork and executes a printf of the valuestring
 * and optional environment a per env_vars
 * with gid and uid as per data_exchange.gid/uid
 * with umask according to umask
 * blocking function with timeout while waiting for feedback and exit-code
 *
 * output's the cmd stdout and stderr in output_buff and returns exit code for client
 *-----------------------------------------------------------------------------------*/
int bash_it(char * output_buf,int buff_len, const char* valuestring ,cJSON * env_vars,int unpriviledged)
{

   debug("bash_it: start substitute %s\n",valuestring);
   // feedback buffer
   int v_exit_code=0;
   int * exitcode=&v_exit_code;

   char shellcommand[1024];
   snprintf(shellcommand,1024,"/usr/bin/printf \"%s\"",valuestring );
   printf("start bash_it\n");
   pid_t child_pid;

   int aStdoutPipe[2];
   if (pipe(aStdoutPipe) < 0) {sock_error("allocating pipe for child stdout redirect");snprintf(output_buf,buff_len,"System error\n");return(15);}
   // Fork_it
   child_pid = fork();

   if(child_pid == 0)
   {
      fflush(stdout);
      fflush(stderr);

      // No output to STDOUT/STDERR or our result get's compromissed !!!
	  // NOT even for debug purposes !!!!
	   
      if (dup2(aStdoutPipe[PIPE_WRITE], STDOUT_FILENO) == -1) {sock_error("redirecting stdout");exit(2);}
      // redirect stderr
      //if (dup2(aStdoutPipe[PIPE_WRITE], STDERR_FILENO) == -1) {sock_error("redirecting stderr");exit(3);}
      int fd2 = open("/dev/null", O_WRONLY);
      if (dup2( fd2 , STDERR_FILENO) == -1) {sock_error("redirecting stderr");exit(3);}
      int fd = open("/dev/null", O_WRONLY);
      if (dup2( fd , STDIN_FILENO) == -1) {sock_error("redirecting stdin");exit(3);}
      // all these are for use by parent only, so close
      close(aStdoutPipe[PIPE_READ]);
      close(aStdoutPipe[PIPE_WRITE]);

      //TODO change to user nobody, if no current user

      if (!unpriviledged)
      {
        uidguid_t uidguid;
         if (get_uid_gid_user(&uidguid,"nobody")==0)
         {
         if( setgid(uidguid.gid)) { error("could not set gid\n") ; exit(4);}
         if (setuid(uidguid.uid)) { error("could not set uid\n") ; exit(5);}
         }
      }
      if (env_vars && cJSON_GetArraySize(env_vars)>0)
      {
         cJSON * pos=NULL;
         cJSON_ArrayForEach(pos, env_vars)
         {
            if (cJSON_IsString(pos))
            {
               setenv(pos->string,pos->valuestring,1);
            }
         }
      }
	   const char * the_shell = getenv("SHELL");
	   const char * shname = strrchr(the_shell, '/');
	   if (shname)
		   shname++;
	   else
		   shname = the_shell;
	   
	   if (the_shell == NULL || strcmp(shname,"nologin")==0) the_shell = _PATH_BSHELL;
	   
	   if (access(the_shell, X_OK) == 0 )
	   {
		   execl(the_shell, shname, "-c", shellcommand, NULL);
	   }
	   else
	   {
		   execlp(shname, "-c", shellcommand, NULL);
	   }
         // just for safety, should never come here
      _exit(6);
   }
   else if (child_pid>0) // The Parent
   {
      // parent wait's and return result
      // close unused file descriptors, these are for child only
      close(aStdoutPipe[PIPE_WRITE]);
	   usleep(100); // wait a bit to get things settled
      fd_set rds;
      int sfd=aStdoutPipe[PIPE_READ];
      int s_ret=0;
      int s_err=0;
      size_t o_read=0;
      int o_r_error=0;
      struct timeval tv;
      tv.tv_sec = 1;
      //tv.tv_sec = TIME_OUT_CHILD;
      tv.tv_usec = 0;
      int timeout=0;
      memset(output_buf,0,buff_len); // clear buffer!!!
      socket_nonblocking(aStdoutPipe[PIPE_READ]);
      do
      {
         FD_ZERO( &rds );
         FD_SET( aStdoutPipe[PIPE_READ], &rds );
         s_ret=select(sfd+1, &rds, NULL, NULL, &tv);
         if (s_ret==SOCKET_ERROR) s_err=errno;
         else if (s_ret>0)
         {
            size_t len=strlen(output_buf);
            size_t read_now= buff_len-len-1;
            if (read_now<1)
            {
               error("bash_it truncating output\n");
               char trunck[1024];
               o_read=read(aStdoutPipe[PIPE_READ], trunck, 1024);
            }
            else
            {
              char * to_buf=output_buf+len;
              o_read=read(aStdoutPipe[PIPE_READ], to_buf, read_now);
            }
            if (o_read==0) break;
            else if (o_read >0);
            else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("std_out pipe read", o_r_error);break; }
         }
         else
         {
            sprintf((output_buf),"System Error: timeout substituding commands!!!");
            *exitcode=1;
            timeout=1;
            break;
         }
      } while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
      close(aStdoutPipe[PIPE_READ]);
      usleep(100);
      int status;
      if (timeout)
      {
         debug("Timeout, kill child\n");
         kill(child_pid,SIGKILL); // TODO need to check with WHOANG
      }
      pid_t w=0;
      int this_error=0;
      while ((w= waitpid(child_pid,&status, 0)) ==-1 && (this_error=errno)==EINTR )
      {
         debug("bash_it: interupted Syscall\n");
      }
      if (w==-1) {
         sock_error_no("waitpid",this_error);
         if (!*exitcode) sprintf((output_buf),"System error: error on 'waitpid()': %s!!!",strerror(this_error));
         *exitcode=1;
      }
      else if (WIFEXITED(status))
      {
         *exitcode=WEXITSTATUS(status);
         if (*exitcode)
            info("exit cmd: bash_it w error: %d\n", *exitcode);
         else
         {
            //info("[OK]\n");
            //sprintf(output_buf+strlen(output_buf),"\t[OK]\n");
            ;
         }
      }
      else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
         alert("command stopped/killed by signal %d : %s", WTERMSIG(status),strsignal(WSTOPSIG(status)));
         *exitcode=1;
         if (!timeout)
            sprintf((output_buf),"System Error: Bash stopped/killed by signal %d : %s!!!", WTERMSIG(status),strsignal(WSTOPSIG(status)));
      }
      else
      {    /* Non-standard case -- may never happen */
         error("Unexpected status child (0x%x)\n", status);*exitcode=status;}
   }
   else
   {
      error("Fork failed \n"); sprintf((output_buf),"System Error: FORK failed!!!");*exitcode=1;}
   return (*exitcode);
}

// New PTY concept

static int done(struct script_control *ctl)
{

   kill(ctl->child, SIGTERM);	/* make sure we don't create orphans */

   if (WIFSIGNALED(ctl->childstatus))
      return(WTERMSIG(ctl->childstatus));
   else
      return(WEXITSTATUS(ctl->childstatus));
}


static void getmaster(struct script_control *ctl)
{
  ctl->master=-1;
#if defined(HAVE_LIBUTIL) && defined(HAVE_PTY_H)
   int rc=-1;
   rc = openpty(&ctl->master, &ctl->slave, NULL, NULL, NULL);

   if (rc < 0) {
      error("openpty failed");
      kill(0, SIGTERM);
      done(ctl);
   }
#else
   char *pty, *bank, *cp;

   pty = &ctl->line[strlen("/dev/ptyp")];
   for (bank = "pqrs"; *bank; bank++) {
      int ptypos=(int)strlen("/dev/pty");
      ctl->line[ptypos] = *bank;
      *pty = '0';
      if (access(ctl->line, F_OK) != 0)
         break;
      for (cp = "0123456789abcdef"; *cp; cp++) {
         *pty = *cp;
         ctl->master = open(ctl->line, O_RDWR | O_CLOEXEC);
         if (ctl->master >= 0) {
            char *tp = &ctl->line[strlen("/dev/")];
            int ok;

            /* verify slave side is usable */
            *tp = 't';
            ok = access(ctl->line, R_OK | W_OK) == 0;
            *tp = 'p';
            if (ok) return;
            close(ctl->master);
            ctl->master = -1;
         }
      }
   }
   ctl->master = -1;
   error("out of pty's\n");

   kill(0, SIGTERM);
   done(ctl);
#endif // have libutil
}

static void getslave(struct script_control *ctl)
{
#ifndef HAVE_LIBUTIL
   ctl->line[strlen("/dev/")] = 't';
   ctl->slave = open(ctl->line, O_RDWR | O_CLOEXEC);
   if (ctl->slave < 0) {
      printf(("cannot open %s"), ctl->line);
      kill(0, SIGTERM);
      done(ctl);
   }
#endif
   setsid();
  // ioctl(ctl->slave, TIOCSCTTY, 0);
}

/* don't use DBG() stuff here otherwise it will be in  the typescript file */
static void do_shell(struct script_control *ctl)
{
   const char *shname;

   getslave(ctl);

   /* close things irrelevant for this process */
   close(ctl->master);
   //close(ctl->sigfd);
   int fd = open("/dev/null", O_WRONLY | O_CLOEXEC);
   if (dup2( fd , STDIN_FILENO) == -1) {sock_error("redirecting stdin");exit(3);}
   //dup2(ctl->slave, STDIN_FILENO);
   dup2(ctl->slave, STDOUT_FILENO);
   dup2(ctl->slave, STDERR_FILENO);
   close(ctl->slave);

   if (ctl->change_ugid && setgid(ctl->gid)) { error("could not set gid\n") ; exit(4);}
   if (ctl->change_ugid && setuid(ctl->uid)) { error("could not set uid\n") ; exit(5);}

   // user=7 rwx , group=7 r--, other: ---
   umask( S_IRWXO ); /* set newly created file permissions */


   ctl->master = -1;
   shname = strrchr(ctl->shell, '/');
   if (shname)
      shname++;
   else
      shname = ctl->shell;

   // TODO check this!! for now disable
   //sigprocmask(SIG_SETMASK, &ctl->sigorg, NULL);

   /*
    * When invoked from within /etc/csh.login, script spawns a csh shell
    * that spawns programs that cannot be killed with a SIGTERM. This is
    * because csh has a documented behavior wherein it disables all
    * signals when processing the /etc/csh.* files.
    *
    * Let's restore the default behavior.
    */
   //signal(SIGTERM, SIG_DFL);
    setenv("TERM","xterm-256color",1);
    setenv("CLICOLOR","1",1);
   /* test extreem env
    int i;
    for (i=0;i<150;i++)
    {
      char name[20];
      char value[120];
      sprintf(name,"CUSTOM_%d",i);
      sprintf(value,"****************************************************************************%d",i);
      setenv(name,value,1);
    }
    */
   if (ctl->need_envp && ctl->env_vars && cJSON_GetArraySize(ctl->env_vars)>0 )
   {
      cJSON * pos=NULL;
      cJSON_ArrayForEach(pos, ctl->env_vars)
      {
         if (cJSON_IsString(pos))
         {
            setenv(pos->string,pos->valuestring,1);
            debug("env: %s=%s\n",pos->string,pos->valuestring);
         }
         else debug("skipped non string %s\n",pos->string);
      }
      //cJSON_Delete(ctl->env_vars);
      //ctl->env_vars=NULL;
   }
   if (access(ctl->shell, X_OK) == 0 )
   {
        // printf("child: execl\n");
         execl(ctl->shell, shname, "-c", ctl->shellcommand, NULL);
   } else
   {
         //printf("child: execlp\n");
         execlp(shname, "-c", ctl->shellcommand, NULL);
   }
   printf(("failed to execute %s\n"), ctl->shell);
   kill(0, SIGTERM);
   exit(done(ctl));
}

static int do_io(struct script_control *ctl, struct trace_Struct * trace)
{
   // parent wait's and return result
   // close unused file descriptors, these are for child only
   size_t buf_size=0x80000;
   char * output_buf=calloc(1,buf_size);
   fd_set rds;
   int s_ret=0;
   int s_err=0;
   ssize_t o_read=0;
   int o_r_error=0;
   struct timeval tv;
   tv.tv_sec = ctl->timeout;
   tv.tv_usec = 0;
   int timeout=0;
   int exitcode=0;
   socket_nonblocking(ctl->master);
   int maxfds=ctl->master;
   socket_nonblocking(ctl->master);
#if HAVE_LIBUTIL
   if (ctl->close_pipe > ctl->master) maxfds=ctl->close_pipe;
   socket_nonblocking(ctl->close_pipe);
#endif
   do
   {
      FD_ZERO( &rds );
      FD_SET( ctl->master, &rds );
#if HAVE_LIBUTIL
            FD_SET( ctl->close_pipe, &rds );
#endif
      tv.tv_sec = ctl->timeout;
      s_ret=select(maxfds+1, &rds, NULL, NULL, &tv);
      if (s_ret==SOCKET_ERROR) s_err=errno;
      else if (s_ret>0)
      {
        if (FD_ISSET(ctl->master, &rds ))
        {
           memset(output_buf,0,buf_size);
           o_read=read(ctl->master, output_buf,buf_size-1);
           if (o_read==0) {debug("trace: EOF\n");;break;}
           else if (o_read >0){
              debug ("trace: Received %d bytes",(int)o_read);
              Write_dynamic_trace_n (output_buf,o_read,trace);
            }
           else if ((o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("pty read", o_r_error);break; }
         }
#if HAVE_LIBUTIL
         if (FD_ISSET(ctl->close_pipe, &rds ))
         {
            memset(output_buf,0,buf_size);
            ssize_t csr=read(ctl->close_pipe,output_buf,buf_size-1);
            if (csr==0) {debug ("pipeclosed\n");break;}
            else if (csr<0 && (o_r_error=errno)!=EINTR && o_r_error!=EAGAIN) { sock_error_no("close pipe read", o_r_error);break; }
            else break;
         }
#endif
      }
      else
      {
        Write_dyn_trace(trace, red,"System Error: timeout executing command!!!\n");
         timeout=1;
         break;
      }
   } while (s_ret!=SOCKET_ERROR || ( s_ret==SOCKET_ERROR && (s_err==EINTR || s_err==EAGAIN || s_err==EWOULDBLOCK)));
   //printf("outputbuf p %p\n",output_buf);
   usleep(10000);
   int status;
   if (output_buf) free(output_buf);
   output_buf=NULL;
   if (timeout)
   {
      debug("Timeout, kill child\n");
      kill(ctl->child,SIGKILL); // TODO need to check with WHOANG
   }
   pid_t w=0;
   int this_error=0;
   while ((w= waitpid(ctl->child,&status, 0)) ==-1 && (this_error=errno)==EINTR )
   {
      debug("exec: interupted Syscall\n");
   }
   if (w==-1) {
      sock_error_no("waitpid",this_error);
      //if (!exitcode)
      Write_dyn_trace(trace, red,"System error: error on 'waitpid()': %s!!!\n",strerror(this_error));
      exitcode=1;
   }
   else if (WIFEXITED(status))
   {
      exitcode=WEXITSTATUS(status);
      if (exitcode)
      {
         debug("exit cmd: %s w error: %d\n",ctl->shellcommand, exitcode);
      }
      else
      {
         //debug("[OK]\n");
         //Write_dyn_trace(trace, green,"\t[OK]\n");
      }
   }
   else if (WIFSIGNALED(status) || WIFSTOPPED(status)) {
      alert("command stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal(WSTOPSIG(status)));
      exitcode=1;
      if (!timeout)
         Write_dyn_trace(trace, red,"\ncommand stopped/killed by signal %d : %s\n", WTERMSIG(status),strsignal(WSTOPSIG(status)));
   }
   else
   {    /* Non-standard case -- may never happen */
      error("Unexpected status child (0x%x)\n", status);
      exitcode=status;
   }

   //restore_tty(ctl, TCSADRAIN);
   return exitcode;
}

int exec_color(void * opaque)
{
   debug("start command color exec\n");

   data_exchange_t * data_exchange=((data_exchange_t *)opaque);
   // feedback buffer
   int exit_code=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;


   char line[20];
   sprintf(line ,"/dev/ptyXX");
   struct script_control ctl =
   {
      .need_envp=data_exchange->needenvp,
      .env_vars =data_exchange->env_json,
#if !HAVE_LIBUTIL
            .line = line,
#else
            .close_pipe= -1,
#endif
      .master = -1,
      .gid=data_exchange->gid,
      .uid=data_exchange->uid,
      .timeout=((data_exchange_t *)opaque)->timeout,
      0
   };


   const char* shname=NULL;
   ctl.shell = getenv("SHELL");
   shname = strrchr(ctl.shell, '/');
   if (shname)
      shname++;
   else
      shname = ctl.shell;
   if (ctl.shell == NULL || strcmp(shname,"nologin")==0)
      ctl.shell = _PATH_BSHELL;

   getmaster(&ctl);

   if (ctl.master==INVALID_SOCKET)
   {
    Write_dyn_trace(trace, red,"\nsystem Error no PTY\n");
    error("system Error no PTY\n");
     return (-1);
   }
   ctl.shellcommand=data_exchange->shellcommand;
    //strdup("echo -e \"\033[31;1m print it RED \033[0;m \" && sleep 1 && echo -e \"\033[32;1m print it GREEN \033[0;m \" && env && ls -la&& exit 3");
#if HAVE_LIBUTIL
        int aStdoutPipe[2];
        if (pipe(aStdoutPipe) < 0) {sock_error("allocating pipe for child stdout redirect");Write_dyn_trace(trace, red,"System error allocating pipe\n");return(15);}
#endif
   fflush(stdout);
   debug("forking\n");
   ctl.child = fork();
   switch (ctl.child) {
      case -1: /* error */
#if HAVE_LIBUTIL
        close (aStdoutPipe[0]);
        close (aStdoutPipe[1]);
#endif
        error("Fork failed \n");
         Write_dyn_trace(trace, red,"\nsystem Error FORK failed\n");
         kill(0, SIGTERM);
         done(&ctl);
         exit_code=EXIT_FAILURE;
         break;
      case 0: /* child */
#if HAVE_LIBUTIL
         close(aStdoutPipe[PIPE_READ]);
         socket_O_CLOEXEC(aStdoutPipe[PIPE_WRITE]);
#endif
         printf("is it flushed?\n");
         fflush(stdout);
         fflush(stderr);
         do_shell(&ctl);
         break;
      default: /* parent */
#if HAVE_LIBUTIL
         close(aStdoutPipe[PIPE_WRITE]);
         socket_nonblocking(aStdoutPipe[PIPE_READ]);
         ctl.close_pipe=aStdoutPipe[PIPE_READ];
#endif
         exit_code=do_io(&ctl,trace);
         break;
   }
   close(ctl.master);
   return exit_code;
}
