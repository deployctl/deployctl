/*
 gitlab_api.h
 Created by Danny Goossen, Gioxa Ltd on 4/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
#ifndef __deployctl__gitlab_api__
#define __deployctl__gitlab_api__

#include "common.h"

struct data_thread {
   cJSON * artifact;
   int finish;
   int free;
   long http_code;
   char * path;
   int error;
   int error_count;
   double lastruntime;
   CURL *curl;
   int progress;
};

int get_release(void * opaque, cJSON ** release);
void *get_artifact(void *userp);
int  update_details(void * userp);
//cJSON * get_job(cJSON * runner,cJSON * info, int n_runners, CURL * curl);
cJSON * get_job(cJSON * runner,cJSON * info, void * stop_struct, CURL * curl);
int get_artifact_auth(cJSON * job);
int upload_artifact(cJSON * job, char * upload_name);

#endif /* defined(__deployctl__gitlab_api__) */
