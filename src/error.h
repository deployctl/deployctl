/*
 cJSON_deploy.h
 Created by Danny Goossen, Gioxa Ltd on 7/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
#ifndef error_h
#define error_h

#include <stdio.h>

void setverbose();
int getverbose();
void clrverbose();

void setdebug();
int getdebug();
void clrdebug();

void setsyslog();
int getsyslog();
void clrsyslog();

int alert( char *message, ...);
int error( char *message, ...);
int info( char *message, ...);
int debug( char *message, ...);
int sock_error(char * msg);
void sock_error_no(char * msg,int e);
int hex_dump_msg(const char*buf,ssize_t size_peek,const char *message, ...);

#endif /* error_h */
