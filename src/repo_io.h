//
//  repo_io.h
//  deployctl
//
//  Created by Danny Goossen on 29/6/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#ifndef __deployctl__repo_io__
#define __deployctl__repo_io__

#include "common.h"

cJSON * JSON_dir_list_x( const char *path,const char *subpath,int recursive, const char * extension);
cJSON * read_json(void * opaque, const char * base_path,const char * sub_path,const char * filename);
#endif /* defined(__deployctl__repo_io__) */
