/*
 release_html.c
 Created by Danny Goossen, Gioxa Ltd on 3/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



#include "deployd.h"

/*------------------------------------------------------------------------
 * check if substring of list in tag,
 * return 1 on no match, indicating a clean tag ( not beta or test ...)
 *------------------------------------------------------------------------*/
int is_clean_tag(void * opaque,const char * tag, const char ** list)
{
   if (!tag && list) return 0;
   if (!list || !*list ) return 1;
   char * work_tag=strdup(tag);
   upper_string(work_tag);
   debug("is clean tag: %s, upp: %s \n",tag, work_tag);

   const char** item;
   int result=1;
   for (item = list; *item != NULL; item++)
   {
      if (strstr(work_tag,*item)!=NULL)
      {
         debug ("Not a clean tag\n");
         result=0;;
         break;
      }

   }
   if (work_tag) free(work_tag);
   if (result) debug("Result is clean\n");
   return result;
}

/*------------------------------------------------------------------------
 * slug a null or non Null terminated string:
 * replace all non (a-zA-Z0-9) with -
 * no - at beginning or end, no subsequent dashes
 *
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * slug_it(const char * input_str, size_t len)
{
   regex_t regex;
   int reti;
   size_t prev_end=0;
   char * output=NULL;
	char * instr=NULL;
   char regex_str[]= "[^a-zA-Z0-9]";
   int regex_match_cnt=1+1;

   regmatch_t   ovector[2];
   //regmatch_t  * ovector= calloc(sizeof(regmatch_t),regex_match_cnt);
   if (!input_str) return NULL;

	
	// prepare input and output buffer
	if (len)
	{
		output=calloc(1, len+1);
		instr=calloc(1, len+1);
		memcpy(instr, input_str, len);
	}
	else
	{
		len=strlen(input_str);
		output=calloc(1, strlen(input_str)+1);
		instr=strdup(input_str);
	}
   if (!output || !instr){
      if (output) free(output);
      if (instr) free(instr);
      return NULL;
   }
   /* Compile regular expression */

   reti = regcomp(&regex, regex_str, REG_EXTENDED);
   if (reti) { // should not happen, regex is fixed, not variable
      error("Could not compile regex\n");
	   regfree(&regex);
	   if (instr) free(instr);
	   if (output) free(output);
	   return NULL;
   }

   /* Execute regular expression in loop */
	
   for (;;)
   {
      size_t len_subject;
      reti = regexec(&regex, instr+prev_end, regex_match_cnt, ovector, 0);
      if (!reti)
      {
         if (ovector[0].rm_so !=-1 && (ovector[0].rm_eo-ovector[0].rm_so) == 1)
         {
              if (!prev_end )
               memcpy(output,instr,ovector[0].rm_so);
            else
               memcpy(output+strlen(output),instr+prev_end,ovector[0].rm_so);

            len_subject=strlen(output);
            if ( len_subject>0 && output[len_subject-1]!='-' ) output[len_subject]='-';
            prev_end=prev_end+ovector[0].rm_eo;
         }
         else break;
      }
      else if (reti == REG_NOMATCH)
      {
         // copy rest from lastmatch end to end of subject
           memcpy(output+strlen(output),(instr+ prev_end), len-prev_end);
         break;
      }
      else
      { char errmsg[1024];regerror(reti, &regex,(char *) errmsg, 1024);error( "Regex match failed: %s\n", errmsg);break;}
   }
	
	//remove trailing -
   while(strlen(output) && output[strlen(output)-1]=='-') output[strlen(output)-1]='\0';
	
   regfree(&regex);
   if (strlen(output)==0)
   {
      if (output) free(output);
      if (instr) free(instr);
      return NULL;
   }
   if (instr) free(instr);
   // we only want lowercase
   lower_string(output);
   // chop anything longer then 63
   if (strlen(output)>63) output[63]=0;
   // we cant end with -
   if (len>61 && output[62]=='-') output[62]=0;
   return output;
}

/*------------------------------------------------------------------------
 * returns the html body or a markdown string w  cmark, markdown processor
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * html_body_release(char * buf_in)
{
    //debug("cmark version: %d\n",cmark_version());
     cmark_parser *parser;
    int options = CMARK_OPT_DEFAULT;
    cmark_node *document;
    parser = cmark_parser_new(options);
    cmark_parser_feed(parser, buf_in, strlen(buf_in));

    document = cmark_parser_finish(parser);

    cmark_parser_free(parser);

    char * body = cmark_render_html(document, options);
    //debug("html_body=>>>\n%s\n<<<=\n",body);
    cmark_node_free(document);
    return body;
}


/*------------------------------------------------------------------------
 * Read a file with dirpath/filename
 * returns NULL on errors
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char* read_file( const char * dirpath, const char * filename)
{

   if (!dirpath || !filename) return NULL;
   //printf("%s/%s\n",dirpath,filename);
   char filepath[1024];
   sprintf(filepath, "%s/%s",dirpath,filename);
   FILE *f = fopen(filepath, "r");
   if (f == NULL)
   {
      debug("process_link_file: %s\n",strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   //fprintf(stderr,"readfile: >\n%s\n",mem.memory);
   return mem.memory;
}
/*------------------------------------------------------------------------
 * Read a JSON file with dirpath/filename
 * returns NULL on errors
 * returns a cJSON object, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
cJSON * read_release_json(void * opaque, const char * dirpath)
{
   // debug
   //printf("%s\n",dirpath);
   if (!dirpath) return NULL;
   FILE *f = fopen(dirpath, "r");
   if (f == NULL)
   {
      debug("read_previous release.json: %s\n",strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   cJSON * result=cJSON_Parse(mem.memory);
   free(mem.memory);
   return result;
}


/*------------------------------------------------------------------------
 * get filesize of filename, returns -1 on error
 *------------------------------------------------------------------------*/
off_t fsize(const char *filename) {
   struct stat st;

   if (stat(filename, &st) == 0)
   {
      //debug("file size stat: %d\n",st.st_size);
      return st.st_size;
   }
   return -1;
}


/*------------------------------------------------------------------------
 * cJSON * JSON_dir_list=> creates a dir list cJSON object
 *
 * creates cJSON array of href links list, with filename and filesize
 * excludes directories
 * excludes files starting with "."
 * excludes files with extension ".url", as these are links
 * links prefixed with sub_path
 * from directory basepath/public/sub_path
 *------------------------------------------------------------------------*/
/* e.g.:
"files": [
 {
  "name": "file2.sh",
  "link": "0-1-5/files/file2.sh",
  "size": 54321
 },
 {
  "name": "my_file1.txt",
  "link": "0-1-5/files/my_file1.txt",
  "size": 12345
 }
]
 */
cJSON * JSON_dir_list( const char *basepath, const char *sub_path)
{
    debug("getting directory listing: %s/public%s\n",basepath, sub_path);
    DIR *dp;
    struct dirent *ep;
    char dirpath[1024];
    sprintf(dirpath, "%s/public%s",basepath,sub_path);

    dp = opendir (dirpath);
    cJSON * filearray=NULL;;
    cJSON * tmp=NULL;
      char link[1024];
    if (dp != NULL)
    {
            debug("dir open\n");
        while ((ep = readdir (dp)))
        {
            if (ep->d_name[0]!='.' && ep->d_type!=DT_DIR && !(strstr(ep->d_name, ".url\0")))
            {
                tmp= cJSON_CreateObject();
                cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(ep->d_name));
                sprintf(link, "%s/%s",sub_path,ep->d_name);
                cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));

               char filename[1024];
               sprintf(filename, "%s/%s",dirpath,ep->d_name);
               off_t filesize=fsize(filename);
               cJSON_AddItemToObject(tmp, "size", cJSON_CreateNumber(filesize));

                if (!filearray) filearray=cJSON_CreateArray();
                cJSON_AddItemToArray(filearray, tmp);
            }
        }
        (void) closedir (dp);
            debug("directory closed\n");
            debug("is cJSON_IsArray=%d\n",cJSON_IsArray(filearray));
    }
    else
    {
        sock_error("Directory JSON parser");
        return NULL;
    }
    debug("DONE directory listing: %s\n",dirpath);
    return filearray;
}





/*------------------------------------------------------------------------
 * cJSON * JJSON_link_list=> creates a links list cJSON object
 *
 * creates the links structure of the release from *.url
 * with "name" : filename without ".url"
 * with "link" : subpath/<content before ,>
 * if a ',' present, a "metric" : <content after metric>
 * links prefixed with sub_path
 * from directory basepath/public/sub_path
 *------------------------------------------------------------------------*/
/*
"links":
[
 {
   "name": "coverage",
   "link": "0-1-5/coveragexxx"
   "metric": "99.1%"
 } , {.....}
]
*/
cJSON * JSON_link_list( const char *basepath, const char *sub_path)
{
   debug("getting directory link listing: %s/public%s\n",basepath, sub_path);
   DIR *dp;
   struct dirent *ep;
   char dirpath[1024];
   sprintf(dirpath, "%s/public%s",basepath,sub_path);

   dp = opendir (dirpath);
   cJSON * linkarray=NULL;;
   cJSON * tmp=NULL;
   char link[1024];
   if (dp != NULL)
   {
      while ((ep = readdir (dp)))
      {
         //(strstr(ep->d_name, ".url\0")
         char * url_ext=strstr(ep->d_name, ".url\0");
         if (url_ext)
         {
            tmp= cJSON_CreateObject();
            // use part of .url file, without the url as name

            // read the file, and parse into link and metric
            char * url_content=read_file(dirpath, ep->d_name);
            if (url_content && strlen(url_content)>1)
            {
               if (url_content[strlen(url_content)-1]=='\n') url_content[strlen(url_content)-1]=0;
               cJSON_AddItemToObject(tmp, "name", cJSON_CreateString_n(ep->d_name,(int)(url_ext-ep->d_name)));
               char * tab_pos=strchr(url_content, (int)',');
               if (tab_pos) tab_pos[0]=0; // this cuts the url_content string to the link
               //fprintf(stderr,"url_content=>%s<\n",url_content);
               if (strncmp(url_content, "http://", 7)==0)
                  sprintf(link,"%s",url_content);
               else if (strncmp(url_content, "https://", 8)==0)
                  sprintf(link,"%s",url_content);
               else if (url_content[0]=='/')
                  sprintf(link, "%s%s",sub_path,url_content);
               else
                  sprintf(link, "%s/%s",sub_path,url_content);
               cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));
               if (tab_pos)
               {
               	 cJSON_AddItemToObject(tmp, "metric", cJSON_CreateString(tab_pos+1));
               }
               free(url_content);
               if (!linkarray) linkarray=cJSON_CreateArray();
               cJSON_AddItemToArray(linkarray, tmp);
            }
         }
      }
      (void) closedir (dp);
   }
   else
   {
      sock_error("Directory JSON parser");
      return NULL;
   }
   debug("DONE directory link listing: %s\n",dirpath);
   return linkarray;
}

/*------------------------------------------------------------------------
 * converts a release(s).json obj to a javascript var with project info
 * returns NULL on errors
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * create_release_json_js (void * opaque, char * download_url, char * releases_json, int tag, char * last_tag)
{
   debug("start  create_release_json_js\n");
   cJSON * env_cjson=((data_exchange_t *)opaque)->env_json;

  struct MemoryStruct js_out;
  init_dynamicbuf(&js_out);

	/*
    var target = "Releases"
	 var download_url= "https://downloads.deployctl.com";
	 var deployctl_version="0.0.20";
	 var project_info={
	 "name":	"deployctl",
	 "link":	"https://gitlab.gioxa.com/deployctl/deployctl",
	 "path": "/deployctl/deployctl"
	 };
	 */

   Writedynamicbuf("var target = \"",  &js_out);
   if (strcmp(cJSON_get_key(env_cjson, "CI_ENVIRONMENT_NAME"),"production")==0)
   {
      if (tag)
   	Writedynamicbuf( "Release",  &js_out);
      else
      Writedynamicbuf( "Releases",  &js_out);
   }
   else
     Writedynamicbuf(cJSON_get_key(env_cjson, "CI_ENVIRONMENT_NAME"),  &js_out);

   Writedynamicbuf("\";\n",  &js_out);


   if (download_url)
   {
	Writedynamicbuf("var download_url = \"",  &js_out);
	Writedynamicbuf( download_url,  &js_out);
	Writedynamicbuf("\";\n",  &js_out);
   }
   if (last_tag && !tag) // can only write last_tag when releases
   {
   	Writedynamicbuf("var last_tag = \"",  &js_out);
   	Writedynamicbuf( last_tag,  &js_out);
   	Writedynamicbuf("\";\n",  &js_out);
   }
	Writedynamicbuf("var deployctl_version = \"",  &js_out);
	Writedynamicbuf( PACKAGE_VERSION,  &js_out);
	Writedynamicbuf("\";\n",  &js_out);
	// project_info
	Writedynamicbuf("var project_info = {",  &js_out);
	// project name
	Writedynamicbuf( "\"name\" : \"",  &js_out);
	Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_NAME"),  &js_out);
	Writedynamicbuf("\",\n",  &js_out);
	// project path
	Writedynamicbuf( "\"path\" : \"",  &js_out);
	Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_PATH"),  &js_out);
	Writedynamicbuf("\",\n",  &js_out);
	// project path
	Writedynamicbuf( "\"link\" : \"",  &js_out);
	Writedynamicbuf( cJSON_get_key(env_cjson, "CI_PROJECT_URL"),  &js_out);
	Writedynamicbuf("\"",  &js_out);
	// close
	Writedynamicbuf("};\n",  &js_out);
	 if (releases_json)
    {
    Writedynamicbuf("var ReleaseData = ",  &js_out);
    Writedynamicbuf(releases_json,  &js_out);
    Writedynamicbuf(";\n",  &js_out);
    }
    return (js_out.memory);

}

/*------------------------------------------------------------------------
 * get's the git-release from .GIT_VERSION in project_path, if present
 * returns NULL on errors or not present
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * check_if_git_version_file(const char * project_path)
{

   if (!project_path ) return NULL;
   char filepath[1024];

   sprintf(filepath, "%s/.GIT_VERSION",project_path);

   FILE *f = fopen(filepath, "r");
   if (f == NULL)
   {
      debug("read gitversion : %s\n",strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
      debug("Reading: %s\n",filepath);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   int i=0;
   while (mem.memory[i]!=0 )
   {
      if(mem.memory[i] ==13 || mem.memory[i] ==10) mem.memory[i]=0;
      i++;
   }
   if (strlen(mem.memory)>2 && strlen(mem.memory)<30)
   {
       debug("Read: %s\n",mem.memory);
      return mem.memory;
   }
   else
   {
      debug("Read bad : %s\n",mem.memory);
      free(mem.memory);
      return NULL;
   }
}

/*------------------------------------------------------------------------
 * get's the last tag from latest.tag from basePATH/public if present
 * returns NULL on errors or not present
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * check_last_tag(void * opaque,const char * basePATH)
{

   if (!basePATH ) return NULL;
   char filepath[1024];
   sprintf(filepath, "%s/public/latest.tag",basePATH);
   debug ("try to find last tag in %s\n",filepath);
   FILE *f = fopen(filepath, "r");
   if (f == NULL)
   {
      debug("read last_tag : %s\n",strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   debug("Found tag: >%s<\n",mem.memory);
   int i=0;
   while (mem.memory[i]!=0 )
   {
      if(mem.memory[i] ==13 || mem.memory[i] ==10) mem.memory[i]=0;
      i++;
   }
   if (strlen(mem.memory)>2 && strlen(mem.memory)<30)
   {
      return mem.memory;
   }
   else
   {
      free(mem.memory);
      return NULL;
   }
}



int  add_deploy_info_json(void * opaque,cJSON * project_json)
{
   cJSON * env=((data_exchange_t *)opaque)->env_json;
   char* ci_project_url=cJSON_get_key(env, "CI_PROJECT_URL");

   char link[1024];
   debug("inside add deploy info\n");
   // "project": {"name": "test_deploy_release","link": "https://gitlab.gioxa.com/deployctl/test_deploy_release"},
   cJSON * tmp=cJSON_CreateObject();
   cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(cJSON_get_key(env, "CI_PROJECT_NAME")));
   cJSON_AddItemToObject(tmp, "path", cJSON_CreateString(cJSON_get_key(env, "CI_PROJECT_PATH")));
   sprintf(link, "%s",ci_project_url);
   cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));
   cJSON_AddItemToObject(project_json, "project", tmp);
   debug("project name\n");
   // "commit": {"name": "d89a8974","link": https://gitlab.gioxa.com/deployctl/test_deploy_release/commit/d89a8974b4363b3f65256330ccdd6dec6614df4e},
   tmp=cJSON_CreateObject();
   char * CI_COMMIT_SHA=cJSON_GetObjectItem(env, "CI_COMMIT_SHA")->valuestring;
   char commit_name[9];
   sprintf(commit_name, "%.*s",8,CI_COMMIT_SHA);
   cJSON_AddItemToObject(tmp, "name",cJSON_CreateString(commit_name) );
   sprintf(link, "%s/commit/%s",ci_project_url,CI_COMMIT_SHA);
   cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));
   cJSON_AddItemToObject(project_json, "commit", tmp);
   debug("commit\n");
   //"deployed": {"name": "danny.goossen@gioxa.com" },  // no link yet, no info yet, "link": "https://gitlab.gioxa.com/dgoo2308"},

   tmp=cJSON_CreateObject();
   if (cJSON_GetObjectItem(env, "GITLAB_USER_NAME"))
   {
      cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(cJSON_GetObjectItem(env, "GITLAB_USER_NAME")->valuestring));
      cJSON_AddItemToObject(tmp, "login", cJSON_CreateString(cJSON_GetObjectItem(env, "GITLAB_USER_LOGIN")->valuestring));
   }
   else
      cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(cJSON_GetObjectItem(env, "GITLAB_USER_EMAIL")->valuestring));
   
   cJSON_AddItemToObject(tmp, "user_id", cJSON_CreateString(cJSON_GetObjectItem(env, "GITLAB_USER_ID")->valuestring));

   time_t timer;
   char date_buffer[26];
   struct tm* tm_info;

   time(&timer);
   tm_info = localtime(&timer);
   strftime(date_buffer, 26, "%Y-%m-%dT%H:%M:%S%z", tm_info);
   cJSON_AddItemToObject(tmp, "date", cJSON_CreateString(date_buffer));

   cJSON_AddItemToObject(tmp, "pipeline_id", cJSON_CreateString(cJSON_GetObjectItem(env, "CI_PIPELINE_ID")->valuestring));
   cJSON_AddItemToObject(tmp, "job_id", cJSON_CreateString(cJSON_GetObjectItem(env, "CI_JOB_ID")->valuestring));

   cJSON_AddItemToObject(project_json, "deployed", tmp);
   debug("LEAVING add deploy info\n");
   return 0;
 ///
}


/*------------------------------------------------------------------------
 * create_thisrelease_json:
 * creates a cJSON object of this release
 * from release_info (gitlab api) and CI-variables with base_path and sub_path
 *------------------------------------------------------------------------*/
cJSON * create_thisrelease_json( void * opaque , cJSON * release_info, int production, const char * base_path,const char * sub_path)
{
   cJSON * env=((data_exchange_t *)opaque)->env_json;

    cJSON * this_release=NULL;
    this_release=cJSON_CreateObject();
    // start the release info.
    debug("start making this release JSON\n");
    char link[1024];
    char* ci_build_tag=NULL;
    cJSON * tmp;
    char* ci_project_url=cJSON_get_key(env, "CI_PROJECT_URL");
    if (cJSON_GetObjectItem(env, "CI_COMMIT_TAG"))
    {
        ci_build_tag=cJSON_get_key(env, "CI_COMMIT_REF_NAME");

        // "tag": { "name": "{CI_COMMIT_SHA}","link": "https://gitlab.gioxa.com/deployctl/test_deploy_release/tags/{CI_COMMIT_SHA}" ==> only when CI_COMMIT_SHA},
        tmp=cJSON_CreateObject();
        cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(ci_build_tag));
        sprintf(link, "%s/tags/%s",ci_project_url,ci_build_tag);
        cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));
        cJSON_AddItemToObject(this_release, "tag", tmp);
        tmp=cJSON_CreateObject();
        if ( cJSON_GetObjectItem(release_info,"message" ))
        {
       // "tag_info": { "message": "test site and api","notes_html": "<p>test site and api ... etc</p>"},
           cJSON_AddItemToObject(tmp, "message", cJSON_CreateString(cJSON_get_key(release_info,"message" )));
        }
        else
            {
               cJSON_AddItemToObject(tmp, "message", cJSON_CreateString("---"));
           }
        cJSON * tmp_json=cJSON_GetObjectItem(release_info, "release");
        if (tmp_json)
        {
            if (cJSON_GetObjectItem(tmp_json, "description"))
            {
                //debug("parsing release->description==>\n%s\n<==\n",cJSON_GetObjectItem(tmp_json, "description")->valuestring);
                char * tmp_str= html_body_release(cJSON_GetObjectItem(tmp_json, "description")->valuestring);
                if (tmp_str)
                {
                    cJSON_AddItemToObject(tmp, "notes_html", cJSON_CreateString(tmp_str));
                    free(tmp_str);
                }
                else cJSON_AddItemToObject(tmp, "notes_html", cJSON_CreateString("<p>could not Parse Release</p>"));
            } else debug("could not find description in release_info\n");
        }else debug("could not find release in release_info\n");
        cJSON_AddItemToObject(this_release, "tag_info", tmp);
            debug("end tag  check\n");
    }
    else
    {
       	tmp=cJSON_CreateObject();
       char * project_path=cJSON_get_key(env, "CI_PROJECT_DIR");
       char * git_version;
        // "environment": { "name": "{CI_ENVIRONMENT_NAME}",
         // so
       // priority 1 : CI_GIT_VERSION
       // priority 2 : file .GIT_VERSION
       // if none above CI_COMMIT_TAG
       // link always to /tree/$CI_COMMIT_REF_NAME
       // if


       char commit_sha[10];
       sprintf(commit_sha,"%.*s",8,cJSON_get_key (env, "CI_COMMIT_SHA"));
       debug("start tag name: with commit sha >%s<\n",commit_sha );
       if (cJSON_GetObjectItem(env, "CI_GIT_VERSION"))
       {
          if (strcmp(commit_sha, cJSON_get_key (env, "CI_GIT_VERSION"))==0)
          {
            // in case we had no tag commit's yet, we get hash, so add the # in front
            char commit_sha_hash[10];
            sprintf(commit_sha_hash,"#%s",commit_sha);
            cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(commit_sha_hash));
          }
          else
             cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(cJSON_get_key (env, "CI_GIT_VERSION")));
       }
       else if ((git_version=check_if_git_version_file(project_path)))
       {
           debug("Got file_git_version >%s<\n",git_version);
          if (strcmp(commit_sha, git_version)==0)
          { // in case we had no tag commit's yet, we get hash, so add the # in front
				 debug("Same as commit, add # in front \n");
             char commit_sha_hash[10];
             sprintf(commit_sha_hash,"#%s",commit_sha);
             cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(commit_sha_hash));
          }
          else
          {
          	debug(" not same so write git_version from file\n");
             cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(git_version));

          }
         if (git_version) free(git_version);
       }
       else
       {
          char commit_sha_hash[10];
          sprintf(commit_sha_hash,"#%s",commit_sha);
          cJSON_AddItemToObject(tmp, "name", cJSON_CreateString(commit_sha_hash));
       }
       sprintf(link, "%s/tree/%s",ci_project_url,cJSON_get_key (env, "CI_COMMIT_REF_NAME"));
       cJSON_AddItemToObject(tmp, "link", cJSON_CreateString(link));
       cJSON_AddItemToObject(this_release, "tag", tmp);
    }

   add_deploy_info_json(opaque,this_release);

   // get htref dirlist
   cJSON * tmp_links =JSON_link_list( base_path ,sub_path);

   cJSON * env_hrefs=cJSON_GetObjectItem(env,"DEPLOY_hrefs");
   cJSON * hr_item;
   cJSON * tmp_link=NULL;
   // get links for gitlab-ci.yml
   if (env_hrefs)
       cJSON_ArrayForEach(hr_item,env_hrefs)
       {
         // links need to have content.
          if (strlen(hr_item->child->string)!=0 && strlen(hr_item->child->valuestring)!=0)
          {
            tmp_link=cJSON_CreateObject();
            debug("adding yaml link: %s,%s\n",hr_item->child->string,hr_item->child->valuestring);
            cJSON_AddItemToObject(tmp_link,"name",cJSON_CreateString(hr_item->child->string));
            cJSON_AddItemToObject(tmp_link,"link",cJSON_CreateString(hr_item->child->valuestring));
            if (!tmp_links) tmp_links=cJSON_CreateArray();
            cJSON_AddItemToArray(tmp_links, tmp_link);
         }
       }

   if (tmp_links ) cJSON_AddItemToObject(this_release, "links", tmp_links);

   cJSON * tmp_files =JSON_dir_list( base_path ,sub_path);
   if (tmp_files) cJSON_AddItemToObject(this_release, "files", tmp_files);
   debug("Done building this_release\n");
   return this_release;
}

/*------------------------------------------------------------------------
 * create_releases_json:
 * Merges previous cJSON ** releases with cJSON * this_release
 * if release-tag already present, it's replaced by this_release
 *
 * returns in **releases the cJSON releases, or NULL
 * returns NULL on errors or not present
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
char * create_releases_json( void * opaque , const cJSON * this_release, cJSON ** releases, int production)
{
   debug("Start create_releases_json\n");
   cJSON * env=((data_exchange_t *)opaque)->env_json;
   char* ci_build_tag=cJSON_get_key(env, "CI_COMMIT_REF_NAME");

    if (!ci_build_tag && *releases) // what is this?
    {
        debug("delete releases\n");
        cJSON_Delete(*releases);
        *releases=NULL;
    }

    // finish it
    cJSON * releases_array=NULL;
    if (!*releases)
    {
        *releases= cJSON_CreateObject();
    }
    releases_array=cJSON_GetObjectItem(*releases, "releases");
    if (!releases_array)
    {
           debug("no releases\n");
        releases_array=cJSON_CreateArray();
        cJSON_AddItemToObject(*releases, "releases",releases_array );
    }
    if (*releases)
    {
        debug("checking if this release was already deployd?\n");
        cJSON * pos=NULL;
        int item=0;
        int exists=0;
        int found_dup=-1;
        cJSON_ArrayForEach(pos, releases_array)
        {
            cJSON * tag=NULL;
            tag=cJSON_GetObjectItem(pos, "tag");
                  if (tag) exists=(validate_key(tag, "name", ci_build_tag)==0);
                if (exists) found_dup=item;
                  exists=0;
            item ++;
        }
        if (found_dup>-1)
        {
            debug("found_dup release tag, deleting: %d\n",found_dup);
            cJSON_DeleteItemFromArray(releases_array, found_dup);
        }
    }
    debug("Inserting this release in the releases array\n");
    cJSON_AddItemToBeginArray(releases_array, cJSON_Duplicate(this_release,1));
    char * result=cJSON_PrintBuffered(*releases, 1, 1);
    return result;
}

/*------------------------------------------------------------------------
 * validate_project_path_slug(void * opaque, char * output_buf)
 * compaires the CI-environment with CI_PROJECT_PATH_SLUG
 * returns 0 on valid
 *------------------------------------------------------------------------*/
int validate_project_path_slug(cJSON * env_json, struct trace_Struct *trace, int suggest)
{
    int result=0;
   char * project_path_slug=NULL;

   project_path_slug=slug_it(cJSON_get_key(env_json, "CI_PROJECT_PATH"), 0);
	
   result=validate_key(env_json, "CI_PROJECT_PATH_SLUG", project_path_slug);
   if (result!=0)
   { // recommend to set PROJECT_PATH_SLUG
      if (project_path_slug)
      {
          if (suggest) Write_dyn_trace(trace, red,"\nERROR: set \"CI_PROJECT_PATH_SLUG\" to \"%s\"\n",project_path_slug);
         debug("ERROR: set \"CI_PROJECT_PATH_SLUG\" to \"%s\"\n",project_path_slug);
      }
      else
      {
         Write_dyn_trace(trace, red,"\nERROR: \"CI_PROJECT_PATH\" invalid or missing \" %s \n", cJSON_get_key(env_json, "CI_PROJECT_PATH"));
         debug("ERROR: \"CI_PROJECT_PATH\" invalid or missing \" %s \n", cJSON_get_key(env_json, "CI_PROJECT_PATH"));
      }
   }
   else
      debug("OK: \"CI_PROJECT_PATH_SLUG\" == \"%s\"\n",project_path_slug);

   if (project_path_slug) free(project_path_slug);
   return result;
}

// TODO move to utils.c

/*------------------------------------------------------------------------
 * extract the version Number
 * 
 * returns 0 on valid
 *------------------------------------------------------------------------*/
int ci_extract_server_version(const char* CI_SERVER_VERSION,int *major_v,int*minor_v,int *revision_v)
{
	//setdebug();
   const char s[2] = ".";
   char *token;
   int count=0;
   char * str=strdup(CI_SERVER_VERSION);
   char * org_str_save=str; //save original str for free
   debug( "\nversion str %s\n", str );
   char * saveptr=NULL;
	
   /* get the first token */
   token = strtok_r(str, s,&saveptr);
   
   /* walk through other tokens */
   while( token != NULL && count<3 )
   {
      
      debug( "version token %d: %s\n",count ,token );
      char * errCheck=NULL;
      int i = (int)strtol(token, &errCheck,(10));
      
      if(errCheck != token)
         debug( "value round %d: %d\n",count,i);
      else
         debug("value round %d: --\n",count);
      
      if (count==0 && major_v)
      {
         if(errCheck != token)
            *major_v=i;
         else
            *major_v=-1;
      }
      else if (count==1&& minor_v)
      {
         if(errCheck != token)
            *minor_v=i;
         else
            *minor_v=-1;
      }
      else if (count==2&& revision_v)
      {
         if(errCheck != token)
            *revision_v=i;
         else
            *revision_v=-1;
      }
      count++;
      token = strtok_r(NULL, s,&saveptr);
   }
   if (org_str_save)free(org_str_save);
	debug("finish get version\n");
   return 0;
}
