/*
 cJSON_deploy.c
 Created by Danny Goossen, Gioxa Ltd on 11/2/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include "deployd.h"

// dupplicate a string from a non null terminated string
char* cJSON_strdup_n(const unsigned char * str, size_t n)
{
   size_t len = 0;
 char *copy = NULL;

   if (str == NULL)
   {
      return NULL;
   }

   len = n + 1;
   if (!(copy = (char *)calloc(len,1)))
   {
      return NULL;
   }
   memcpy(copy, str, len);
   copy[n]=0;
   return copy;
}

// add item to object with a key derived from a non 0 terminated string
void   cJSON_AddItemToObject_n(cJSON *object, const char *string,size_t n , cJSON *item)
{
   if (!item)
   {
      return;
   }

   /* free old key and set new one */
   if (item->string)
   {
      free(item->string);
   }
   item->string = (char*)cJSON_strdup_n((const unsigned char*)string,n);
   cJSON_AddItemToArray(object,item);
}

// create a string object from a non null terminated string
cJSON *cJSON_CreateString_n(const char *string, size_t n)
{
   cJSON *item = cJSON_CreateObject();
   if(item)
   {
      item->type = cJSON_String;
      item->valuestring = (char*)cJSON_strdup_n((const unsigned char*)string, n);
      if(!item->valuestring)
      {
         cJSON_Delete(item);
         return NULL;
      }
   }

   return item;
}

/* insert item at the beginning of the array/object. */
void   cJSON_AddItemToBeginArray(cJSON *array, cJSON *item)
{
     if (!array) return;
    cJSON *c = array->child;
    if (!item)
    {
        return;
    }
    if (!c)
    {
        /* list is empty, start new one */
        array->child = item;
    }
    else
    {
        array->child=item;
        item->next=c;
        c->prev=item;
    }
}

// create an JSON tree from **envp
// but only for environment vars starting with : CI_, GITLAB_ or DEPLOY_
cJSON * cJSON_Create_env_obj(char** envp)
{
   cJSON *environment = cJSON_CreateObject();
   char** env;
   for (env = envp; *env != 0; env++)
   {
      char* thisEnv = *env;
      //printf("thisenv: %s\n", thisEnv);

      char * e=thisEnv;
      int len=(int)strlen(thisEnv);
      int pos = 0;
      while (*e++ != '=' && pos < len) pos++;
      if (pos != len && pos >4)
      {
         if (strncmp(thisEnv,"CI_",3)==0 || \
            ( pos >8 && strncmp(thisEnv,"DEPLOY_",7)==0 )|| \
             ( pos>7 && strncmp(thisEnv,"GITLAB",6)==0) ) \
         {
           cJSON_AddItemToObject_n(environment, thisEnv, pos, cJSON_CreateString(thisEnv+pos+1));
         }
      }
   }
   return environment;
}

// compaire a string value of a key, check if present and compaire
// returns 0 if match
int validate_key(cJSON * item, const char * key,const char * value )
{
   if (!key || !item) return -2;
   if (item->type==cJSON_String && !value) return -1;
   cJSON * tmp_json=cJSON_GetObjectItem(item, key );
   if (tmp_json)
   {
       if (tmp_json->valuestring && value)
          return(strcmp(value,tmp_json->valuestring)); // return 0 on equal string
       else if (!value && !tmp_json->valuestring)
          return 0; // NULL==NULL
       else
          return -1;
   }
   else
   {

     return(-1);
     //else return 0; // no item == no value
   }
}

// get a key valuestring but check if exist
// return NULL on non exist
int get_key(cJSON * item, const char * key,char ** value )
{

   if (!key) return -2;
   cJSON * tmp_json=cJSON_GetObjectItem(item, key );
   if (tmp_json)
   {
      if (value) *value=tmp_json->valuestring;
      return (0);
   }
   else
   {
      return(-1);
   }
}

// parse the JSON env-tree to an **envp
int parse_env(cJSON * env_json, char *** newevpp)
{
   int i;
   size_t len;
   int array_size = cJSON_GetArraySize(env_json);
   *newevpp=calloc(sizeof(char *), array_size+1);
   char**walk=*newevpp;
   for (i = 0; i < array_size; i++)
   {
      cJSON *subitem = cJSON_GetArrayItem(env_json, i);
      if (cJSON_IsString(subitem))
      {
      len=strlen(subitem->string)+1+strlen(subitem->valuestring) +1;
      *walk=calloc(1, len);
      sprintf(*walk, "%s=%s",subitem->string,subitem->valuestring);
        // printf("%s\n",*walk);
      }
      walk++;
   }
   return (0);
}

// cleanup of env created with : int parse_env(cJSON * env_json, char *** newevpp)
void free_envp(char *** envp)
{
    char ** walk = *envp;
    while (*walk)
    {
        if (*walk) free(*walk);
        *walk=NULL;
        walk ++;
    }
    free(*envp);
    *envp=NULL;
}


// check if item is present in json string array
int in_string_array(cJSON * json_list, char * search)
{
   if (!json_list || json_list->type==cJSON_NULL ||  json_list->type==cJSON_Invalid) return -3;
   if (!search) return 0;

   cJSON * pos=NULL;
   int found=0;
   cJSON_ArrayForEach(pos,json_list)
   {
      if (pos->valuestring && strcmp(search,pos->valuestring)==0)
      {
         found =1;
         break;
      }
   }
   return !found;
}

// check if list of keys is present in JSON structure
int check_presence(cJSON * env_json, char ** list, struct trace_Struct *trace)
{
   if (!env_json || !trace) return -1;
   if (!list) return 0;
   char** item;
   int result=0;
   for (item = list; *item != NULL; item++)
   {
      if(!cJSON_GetObjectItem(env_json, *item))
      {
         result++;

            Write_dyn_trace(trace, red,"\nERROR: Missing \"%s\"\n",*item);

      }
   }
   return result;
}

// get's the object->valuestring of key
// return NULL on non exiting
// return a null terminated string,
// caller should not freeit, points to string in object
char * cJSON_get_key(cJSON * item, const char * key)
{
   if (!item || !key ) return NULL;
   char * result=NULL;
   cJSON * thisitem=cJSON_GetObjectItem(item, key);
   if (thisitem) result=thisitem->valuestring;
   return result;
}
