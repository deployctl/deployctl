//
//  deploy-runner.c
//  deployctl
//
//  Created by Danny Goossen on 21/7/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"


// TODO need external var from main, indication quit or stop

/*------------------------------------------------------------------------
 * Read a file with dirpath/filename
 * returns NULL on errors
 * return a null terminated string, caller is responsible for freeing it!
 *------------------------------------------------------------------------*/
cJSON* read_runners_file( const char * dirpath, const char * filename)
{
   cJSON * runners_json=NULL;

   if (!dirpath || !filename) return NULL;
   //printf("%s/%s\n",dirpath,filename);
   char filepath[1024];
   sprintf(filepath, "%s/%s",dirpath,filename);
   FILE *f = fopen(filepath, "r");
   if (f == NULL)
   {
      error("process_runners_file: %s\n",strerror(errno));
      return NULL;
   }
   struct MemoryStruct mem;
   init_dynamicbuf(&mem);
   char buf[1024];
   bzero(buf, 1024);
   while( fgets( buf, 1024, f ) != NULL )
   {
      Writedynamicbuf(buf, &mem );
      bzero(buf, 1024);
   }
   fclose(f);
   //fprintf(stderr,"readfile: >\n%s\n",mem.memory);
   ;
   if (mem.memory && strlen(mem.memory) >0)
   runners_json= yaml_sting_2_cJSON (NULL , mem.memory);
   free_dynamicbuf(&mem);
   // validate json
   if (!runners_json)
   {
      error("Failed to get runner.yaml %s \n",filepath);
      return NULL;
   }
   else
   {
      alert(" got runners\n");
      // TODO for each, check if token and url
      // if present, cleanup url, not allowed to have / at the end!!!
      // otherwise no long polling
      /*
      if (check_presence(runners_json,
                     (char *[])
                     {"token","ci_url",NULL},
                         NULL) == 0 )
      {
      
       
      }
      else
      {
         cJSON_Delete(runners_json);
         return NULL;
      }
      */
      return runners_json;
   }
}


//curl --request POST "https://gitlab.example.com/api/v4/jobs/request" --form "token=t0k3n"
//Responses:

//Status	Data	Description
//201	yes	When a build is scheduled for a runner
//204	no	When no builds are scheduled for a runner (for GitLab Runner >= v1.3.0)
//403	no	When invalid token is used or no token is sent



int do_job(cJSON * runner,cJSON* job,parameter_t * parameters )
{
   void *trace=NULL;//=&v_trace;
   
	
   int unpriviledged=parameters->current_user;
   int runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
   int res=0; // failed or success
   cJSON_AddStringToObject(job, "ci_url", cJSON_get_key(runner, "ci_url"));
	
   cJSON * id_j=cJSON_GetObjectItem(job, "id");
   int id=0;
   if (cJSON_IsNumber(id_j)) id=id_j->valueint;
   alert("runner %2d: Start Job %d (%s)\n",runner_id,id,cJSON_get_key(runner, "ci_url"));
   char temp[1024];
   snprintf(temp,1024,"%s/api/v4/jobs/%d",cJSON_get_key(job, "ci_url"),id);

   init_dynamic_trace(&trace,cJSON_get_key(job, "token") , temp,id);
   
   Write_dynamic_trace_color("", "running",none ,trace);
   Write_dyn_trace(trace,bold_cyan,"deploy-runner %s\n",PACKAGE_VERSION);
   char dashes[]="--------------------------------";
   dashes[strlen(PACKAGE_VERSION)]=0;
   Write_dyn_trace(trace,bold_magenta,"--------------%s\n\n",dashes);

   
   char hostname[1024];
   gethostname(hostname, 1024);
   time_t timer;
   char time_str[26];
   struct tm* tm_info;
   time(&timer);
   tm_info = localtime(&timer);
   strftime(time_str, 26, "%Y-%m-%d %H:%M:%S", tm_info);

   Write_dynamic_trace("jobid",NULL ,trace);
   sprintf(temp," #%d on %s @ %s\n",id,hostname,time_str);
   Write_dynamic_trace(temp,NULL, trace);
   update_details(trace);

   char work_dir[1024];
   sprintf((char*)work_dir,"/opt/deploy/var/%d",id);
   cJSON_AddStringToObject(job, "ci_work_dir", work_dir);

	/*
	if (getdebug() )
	{
		debug("debug job\n");
		char * print_it=cJSON_PrintBuffered(job,1,1);
		if (print_it)
		{
			
			Write_dyn_trace(trace, bold_cyan, "job:\n %s\n",print_it);
			debug("job:\n %s\n",print_it);
			free(print_it);
		}
		
	}
	 */
   cJSON * u_artifacts= cJSON_GetArrayItem( cJSON_GetObjectItem(job,"artifacts"),0);
   // usleep(1000000);
   // prepare env_vars for deployctl
   // first flatten the env vars
	debug ("job ptr %p",job);

   Write_dyn_trace(trace,bold_yellow,"\n* Prepare Environment:\n");
   update_details(trace);
   cJSON *env_vars=NULL;
   char CI_PROJECT_DIR[1024];
   if (res==0)
   {

      cJSON * var=NULL;
	   print_json(cJSON_GetObjectItem(job, "variables"));
	   cJSON_ArrayForEach(var,cJSON_GetObjectItem(job, "variables"))
	   {
		   cJSON * value_object=cJSON_GetObjectItem(var, "value");
		   if (strncmp(cJSON_get_key(var, "key"), "CI_BUILD_", 9)!=0 && value_object )
		   { // remove legacy CI_BUILD_xxxx vars
			   if (!env_vars) env_vars=cJSON_CreateObject();
			   if (!cJSON_get_key(env_vars,cJSON_get_key(var, "key")))
				   cJSON_AddItemToObject(env_vars, cJSON_get_key(var, "key"),  cJSON_Duplicate(value_object, 1));
			   else
				   cJSON_ReplaceItemInObject(env_vars,cJSON_get_key(var, "key"),cJSON_Duplicate(value_object, 1));
		   }
	   }

      sprintf(CI_PROJECT_DIR,"%s/project_dir",work_dir); //,cJSON_get_key(env_vars,"CI_PROJECT_NAME"));
      cJSON_AddStringToObject(env_vars, "CI_PROJECT_DIR", CI_PROJECT_DIR );
      cJSON_AddStringToObject(job, "CI_PROJECT_DIR", CI_PROJECT_DIR );

	  // check artiname, so far no processing!!! To dO
      if (cJSON_GetObjectItem(u_artifacts, "name")&& cJSON_GetObjectItem(u_artifacts, "name")->type==cJSON_String )

          cJSON_AddStringToObject(env_vars, "_arti_name",   cJSON_get_key(u_artifacts, "name"));

      if (cJSON_GetObjectItem(u_artifacts, "paths")&& cJSON_GetObjectItem(u_artifacts, "paths")->type!=cJSON_NULL )

          cJSON_AddItemToObject(env_vars, "_arti_paths", cJSON_Duplicate(cJSON_GetObjectItem(u_artifacts, "paths"),1));

   }
   	debug ("env ptr %p",env_vars);
	

	// anti Fraud
	int cnt=0;
	Write_dyn_trace_pad(trace,yellow,75,"  + anti ACL fraud correction");
	update_details(trace);
	
	if ((cnt=check_presence(env_vars,
							(char *[]){"CI_PROJECT_URL","CI_REGISTRY_USER","CI_REGISTRY_PASSWORD","CI_JOB_TOKEN","CI_PROJECT_NAMESPACE","CI_PROJECT_PATH","CI_PROJECT_NAME",NULL}
							, trace)))
	{
		Write_dyn_trace(trace, red,"ERROR: %d Environment variable(s) Missing\n",cnt);
		debug("\nERROR: %d Environment variable(s) Missing!!!\n",cnt);
		res=1;
	}

	if (res==0) res=verify_project_url(job,env_vars,trace);
	
	if (res==0) res=verify_project_name_path_namespace(env_vars,trace);

	if (res==0) res= pre_project_path_slug_check(env_vars,trace);

	if (res==0) Write_dyn_trace(trace, green,"[OK]\n");
	update_details(trace);
	/*
	if (getdebug() )
	{
		debug("debug job\n");
		char * print_it=cJSON_PrintBuffered(job,1,1);
		if (print_it)
		{
		
			Write_dyn_trace(trace, bold_cyan, "job:\n %s\n",print_it);
			debug("job:\n %s\n",print_it);
			free(print_it);
		}
		
	}
	if (getdebug() )
	{
		debug("debug env\n");
		char * print_it=cJSON_PrintBuffered(env_vars,1,1);
		if (print_it)
		{
			
			Write_dyn_trace(trace, bold_cyan, "env_vars UN Processed:\n %s\n",print_it);
			debug("env_vars_unprocessed:\n %s\n",print_it);
			free(print_it);
		}
		
	}
  */
	debug("start environment vars \n");
	
   if (res==0)
   {
   // only show var's when verbose is on
   Write_dyn_trace_pad(trace, yellow,75,"  + Process DEPLOY variables");
   update_details(trace);
   res+= process_deploy_href( env_vars,trace );
   
   // No need to preprocess, it's already an array of strings
    if (cJSON_GetObjectItem(env_vars, "_arti_paths"))
      res+= process_deploy_path( env_vars,trace, "_arti_paths",NULL,NULL ,unpriviledged);
	 
   res+= process_deploy_path( env_vars,trace, "DEPLOY_REPO_PATH", "/",NULL,unpriviledged );
	 
   res+= process_deploy_path( env_vars,trace, "DEPLOY_RELEASE_PATH", "/release/","/", unpriviledged );
	   
   res+= process_deploy_path( env_vars,trace, "DEPLOY_PUBLISH_PATH", "/public/","/",unpriviledged );
	   
   if (!res)Write_dyn_trace(trace, green, "[OK]\n");
   update_details(trace);

   }
   // substitute var's within var's:
   if (res==0)
   {
      Write_dyn_trace_pad(trace,yellow,75,"  + Substitute variables ...");
      res+=bash_subs_one_level(trace,env_vars, 0 ,unpriviledged);
      if (res==0)res=bash_subs_specials(trace,env_vars,unpriviledged);
      if (res==0)
         Write_dynamic_trace_color("[OK]\n", NULL,green, trace);
      update_details(trace);
   }
   if (res==0)
   {
   Write_dyn_trace_pad(trace, yellow,75,"  + post-process DEPLOY variables");
   res+= post_process_deploy_href( env_vars,trace );
   if (cJSON_GetObjectItem(env_vars, "_arti_paths"))
   {
      res+= post_process_deploy_path( env_vars,trace, "_arti_paths",NULL,NULL ,unpriviledged);
   }
   res+= post_process_deploy_path( env_vars,trace, "DEPLOY_REPO_PATH","/",NULL ,unpriviledged);
   res+= post_process_deploy_path( env_vars,trace, "DEPLOY_RELEASE_PATH", "/release/","/" ,unpriviledged);
   res+= post_process_deploy_path( env_vars,trace, "DEPLOY_PUBLISH_PATH", "/public/" ,"/",unpriviledged );
   if (!res)Write_dyn_trace(trace, green, "[OK]\n");
   update_details(trace);
   }
	
   // mov artipath  back to job
   if (cJSON_GetObjectItem(env_vars, "_arti_paths")) cJSON_ReplaceItemInObject(u_artifacts,"paths",cJSON_DetachItemFromObject(env_vars,"_arti_paths"));
   if (cJSON_GetObjectItem(env_vars, "_arti_name")) cJSON_ReplaceItemInObject(u_artifacts,"name",cJSON_DetachItemFromObject(env_vars,"_arti_name"));
   // Filter script
	debug("start steps\n");
	cJSON*deploy_cmds=NULL;
	if (res==0)
	{
		int count=0;
		
		cJSON*steps=cJSON_GetObjectItem(job, "steps");
		cJSON*script=cJSON_GetObjectItem((cJSON_GetArrayItem(steps, 0)),"script");
		cJSON*step=NULL;
		if (cJSON_GetArraySize(script)>0)
			Write_dyn_trace_pad(trace,yellow,75,"  + Remove non deployctl commands from script:");
		cJSON_ArrayForEach(step,script)
		{
			if (strstr(step->valuestring, "deployctl")==step->valuestring)
			{
				if(!deploy_cmds) deploy_cmds=cJSON_CreateArray();
				cJSON_AddItemToArray(deploy_cmds, cJSON_CreateString(step->valuestring ));
			}
			else
			{
				count ++;
				if (count==1)
					Write_dyn_trace(trace, bold_magenta, "\n       * ");
				else
					Write_dyn_trace(trace, bold_magenta, "       * ");
				Write_dyn_trace(trace, cross_magenta, "   %s   \n",step->valuestring);
			}
		}
		if (count==0) Write_dyn_trace(trace, green,"[OK]\n");
		
		update_details(trace);
	}
	

	
   
   _mkdir(CI_PROJECT_DIR,"");
	
   // fetch git ci_project_url if needed
   if (res==0 )
   {
     if (cJSON_GetObjectItem(env_vars,"GIT_STRATEGY") && validate_key(env_vars,"GIT_STRATEGY","none")==0)
           Write_dyn_trace(trace,bold_yellow,"\n* Skipped git on request\n");
     else
     {
	   
        res=lgit2_checkout(job, trace);
        update_details(trace);
     }

   }
   // cancel??
   if (parameters->hubsignal || parameters->exitsignal)
   {
      if (parameters->hubsignal)
        Write_dyn_trace(trace,bold_red,"\n-- Runner recived Reload signal\n");
      else
        Write_dyn_trace(trace,bold_red,"\n-- Runner recived EXIT signal\n");
      res=1;
     Write_dyn_trace(trace,yellow,"\n Please retry JOB later on, stopping deployment\n");
       update_details(trace);
   }
	
	// fetch artifacts if needed
   if (res==0)
   {
       cJSON * artifacts=prepare_artifacts(job);
       if (artifacts && cJSON_GetArraySize(artifacts)>0)
       {
          char zip_dir[1024];
          sprintf(zip_dir,"%s/.zips",work_dir);

          sprintf(temp,"\n* %2d JOB artifacts to     Download   -   Unzip  \n",cJSON_GetArraySize(artifacts));
          Write_dynamic_trace_color(temp,NULL, bold_yellow,trace);
          sprintf(temp,"  ---------------------------------------------\n");
          Write_dynamic_trace_color(temp,NULL, bold_yellow,trace);
          set_mark_dynamic_trace(trace);
          scan_artifacts_status(artifacts,trace);
          update_details(trace);

          int count=cJSON_GetArraySize(artifacts);
          res=count;
          int update_error=0;
          int res_prev=0;
          struct dl_artifacts_s dl_artifacts;
          sprintf(temp,"%d",id);
          init_download_artifacts(&dl_artifacts, count, work_dir, zip_dir);
          int count_u=0;
          while ( res>0 && !update_error && !parameters->hubsignal && !parameters->exitsignal) // loop till finish artifacts
          {
             int progress_update=0;
             res=download_artifacts(artifacts,&dl_artifacts);
             progress_update=progress_change(&dl_artifacts);
             if ((res!=res_prev) || res <1 || progress_update) // resduce network trafic, only report changes
             {
                if ((count_u%5 ==0) || res <1 || progress_update)
                {
                   scan_artifacts_status(artifacts,trace);
                   update_error= update_details(trace);
                   if (update_error){
                     printf("\n** main update details error, canceling downloads\n");
                      cancel_download_artifacts(&dl_artifacts);
                   }
                }
                count_u ++;
             }
             res_prev=res;
          }
          // cleanup the xip dir
          // TODO add again, temp disable remove for debug
          //printf("Finish, not removing zips, res=%d\n",res);


          close_download_artifacts(&dl_artifacts);
          _rmdir("",  zip_dir);
          if (update_error) res=-1;
       }
       update_details(trace);
       cJSON_Delete(artifacts);
   }


   if (parameters->hubsignal || parameters->exitsignal)
   {
      if (parameters->hubsignal)
      Write_dyn_trace(trace,bold_red,"\n-- Runner received reload signal\n");
      else
      Write_dyn_trace(trace,bold_red,"\n-- Runner received EXIT signal\n");
      res=1;
      Write_dyn_trace(trace,yellow,"\n Please retry JOB later on, stopping deployment\n");
      update_details(trace);
   }
	
   // if we have something to execute, try to get lock
	
   if (res==0 && deploy_cmds && cJSON_GetArraySize( deploy_cmds)>0)
   {

      int got_lock=0;
      int lock_res=0;
      alert("runner %2d: REQUEST deployment Lock for job #%d \n",runner_id,id);
      set_mark_dynamic_trace(trace);
      int count_wait_lock=0;
      while (got_lock==0)
      {
         if (parameters->hubsignal || parameters->exitsignal)
         {
            if (parameters->hubsignal)
            Write_dyn_trace(trace,bold_red,"\n-- Runner received Reload signal\n");
            else
            Write_dyn_trace(trace,bold_red,"\n-- Runner received EXIT signal\n");
            res=1;
            Write_dyn_trace(trace,yellow,"\n Please retry JOB later on, stopping deployment\n");
            update_details(trace);
            break;
         }
         lock_res=pthread_mutex_trylock(parameters->nginx_lock);
         if (lock_res==0)
         {
            got_lock=1;
            if (count_wait_lock>0){
               clear_till_mark_dynamic_trace(trace);
               if (count_wait_lock>=240)
               Write_dyn_trace(trace, magenta,"\n ( Waited for a deployment slot %d minutes )\n",count_wait_lock/120);
               else
               Write_dyn_trace(trace, magenta,"\n ( Waited for a deployment slot %d seconds )\n",count_wait_lock/2);
               update_details(trace);
            }
         }
         else
         {
            if (lock_res== EBUSY)
            {
               usleep(500000);
            }
            else if (lock_res!= EINTR)
            {
               error("runner %2d: Error on getting lock for job #%d : %s\n",runner_id,id,strerror(lock_res));
               break;
            }
            if ((count_wait_lock%10) ==0 )
            {
               clear_till_mark_dynamic_trace(trace);
               if (count_wait_lock>=240)
                 Write_dyn_trace(trace, yellow,"\n Waiting for a deployment slot... %2d minutes\n",count_wait_lock/120);
               else if (count_wait_lock==0)
                 Write_dyn_trace(trace, yellow,"\n Waiting for a deployment slot...\n");
               else
                 Write_dyn_trace(trace, yellow,"\n Waiting for a deployment slot... %2d seconds\n",count_wait_lock/2);
              update_details(trace);
            }
         }
         count_wait_lock++;
      }
	   // execute script!!!!
      if (got_lock)
      {
           alert("runner %2d: Got deployment Lock for job #%d \n",runner_id,id);
           Write_dyn_trace(trace, bold_yellow,"\n* Start Deployment: ");
           Write_dyn_trace(trace, cyan,"%s\n",cJSON_get_key(env_vars, "CI_ENVIRONMENT_NAME"));
           update_details(trace);
            cJSON*step=NULL;
            cJSON_ArrayForEach(step,deploy_cmds)
            {
               Write_dyn_trace(trace, bold_cyan, "\n$> %s\n",step->valuestring);
               char * command=strstr(step->valuestring,"deployctl ");
               // and execute
               if (!res) res= exec_command(command+10,env_vars,trace,parameters);
               update_details(trace);
               if (res) break;
            }
      }
      else
      {
         res=1;
      }
      if (got_lock && lock_res==0)
      {
        pthread_mutex_unlock (parameters->nginx_lock);
        alert("runner %2d: Released deployment Lock for job #%d\n",runner_id,id);
      }
   }
   	else if (res==0)
   	{
		 Write_dyn_trace(trace, bold_yellow,"\n* Script: ");
		 Write_dyn_trace(trace, bold_magenta,"   *** No deployctl commands ***\n");
		 update_details(trace);
   	}
   // and clean it up
   	if (deploy_cmds) cJSON_Delete( deploy_cmds);
	
    debug("starting artifacts\n");
	
	int disable_upload=0;
   	if ( !disable_upload &&
       ( // job failed and always or on failure, make artifacts
          (res && (validate_key(u_artifacts,"when","always")==0  || validate_key(u_artifacts,"when","on_failure")==0) )
      || // or job succeeds an artifacts NOT on failure
          (!res && validate_key(u_artifacts,"when","on_failure")!=0)
       )
    && // and
      ( // path OR/AND untracked => artifacts UpLoad
          (cJSON_GetObjectItem(u_artifacts, "paths")     && cJSON_GetObjectItem(u_artifacts, "paths")->type    !=cJSON_NULL ) || \
          (cJSON_GetObjectItem(u_artifacts, "untracked") && cJSON_IsTrue(cJSON_GetObjectItem(u_artifacts, "untracked" )) )
       )
      )
 {
     int result=0;
     char upload_zip_name[1024];
     sprintf(upload_zip_name,"%s/artifacts.zip",work_dir);

     debug("Upload artifacts: untacked: %s\n",cJSON_get_key(u_artifacts,"untracked") );
      set_mark_dynamic_trace(trace);
      Write_dyn_trace(trace,bold_yellow,"\n* Artifacts:\n");
    
        cJSON * filelist=NULL;
      int untracked_n=0;
      int path_files_n=0;
      if (cJSON_GetObjectItem(u_artifacts, "untracked") &&cJSON_IsTrue(cJSON_GetObjectItem(u_artifacts, "untracked")))
      {
        // OK
         Write_dyn_trace(trace,none,"  + Finding untracked files ....");
         update_details(trace);
        result=untracked(CI_PROJECT_DIR,&filelist);
        if (!result && filelist) untracked_n=cJSON_GetArraySize(filelist);
        Write_dyn_trace(trace,none," Found %d file(s)\n",untracked_n);
        update_details(trace);
      }
      else
      	debug("untracked: false !!\n");
      debug("start path filelist\n");
      if (cJSON_GetObjectItem(u_artifacts, "paths")     && cJSON_GetObjectItem(u_artifacts, "paths")->type    !=cJSON_NULL)
      {
        Write_dyn_trace(trace,none,"  + Finding files from paths ...");
        update_details(trace);
          // got a list of paths to explore and add to the filelist
          result= result+getfilelist(CI_PROJECT_DIR,cJSON_GetObjectItem(u_artifacts, "paths"),&filelist);
          if (!result && filelist)  path_files_n=cJSON_GetArraySize(filelist)-untracked_n;


            Write_dyn_trace(trace,none," Found %d file(s)\n",path_files_n);
            update_details(trace);

      }
      debug("print file-list\n");
      //get_artifact_auth(job);

      if (!result )
      {
		  /*
         if (getdebug() && filelist)
         {
           debug ("artifacts files:\n");
            Write_dyn_trace(trace, cyan,"  + artifact files:\n");
            cJSON * file;
            cJSON_ArrayForEach(file,filelist)
            {
               Write_dyn_trace(trace, cyan,"    - %s\n",file->valuestring);
               debug(" - %s\n",file->valuestring);
            }
         }
		 */
         // zip_it
         debug("zip %d files to %s\n",cJSON_GetArraySize(filelist),upload_zip_name);
         result=list_zip_it(upload_zip_name,CI_PROJECT_DIR,filelist);

         if (!res) res=result;
      }
      if (!result)
      {
         Write_dyn_trace_pad(trace,none,75,"  + zipped and Uploading ");
          result=upload_artifact(job,upload_zip_name);
          if (!res) res=result;
      }
      if (!result)
      {
         Write_dynamic_trace_color(" [OK]\n", NULL, green,trace);
      }
      else
      {
         Write_dynamic_trace_color(" [FAIL]\n", NULL, red,trace);
      }
      update_details(trace);
      if (filelist) cJSON_Delete(filelist);
      filelist=NULL;
   }
   else
   {
     debug("artifacts SKIPPED\n");
     //Write_dynamic_trace_color("\n* Upload artifacts SKIPPED\n", NULL, cyan,trace);
     //update_details(trace);
   }

   // Clean it all up
	
   Write_dynamic_trace_color("\n* Clean_up\n", NULL, bold_yellow,trace);
   update_details(trace);
	
	// if run in debug holdon to the project dir
   if (!getdebug()) _rmdir("", work_dir);
	
	debug("workdir=%s",work_dir);

   // return result to gitlab
   if (res==0)
      Write_dynamic_trace_color("\nSUCCESS.\n", "success", bold_green,trace);
   else
      Write_dynamic_trace_color("\nJob FAILED.\n", "failed", bold_red,trace);

   int fail_count=0;
   int cancel=0;
   while ((cancel=update_details(trace))>0 && fail_count<3) {fail_count++; usleep(100000* fail_count);}
   if (cancel==-1) alert("runner %2d: job %d :canceled by user\n",runner_id,id);
   else
      if (res==0)
        alert("runner %2d: job %d : SUCCESS\n",runner_id,id);
      else
        alert("runner %2d:job %d :FAILED\n",runner_id,id);
	
   if (env_vars) cJSON_Delete(env_vars);
   if (trace) free_dynamic_trace(&trace);
	
	// DONE
   return 0;
}

struct check_runner_data_s{
   cJSON * runner;
   cJSON * job;
   cJSON * info;
   parameter_t * parameters;
   int stop;
   int stop_ack;
};


void * check_runner_thread(void * data)
{

   CURL * curl;
   curl = curl_easy_init();

   int new_interval;
   int interval;
   struct timeval now_time, last_time;

   int * nextcheck=NULL;
   int runner_id;
   int error_count=0;
   int old_error=0;
      cJSON * job=NULL;
   cJSON * runner=((struct check_runner_data_s *)data)->runner;
   cJSON * info=((struct check_runner_data_s *)data)->info;
   parameter_t * parameters=((struct check_runner_data_s *)data)->parameters;
   stop_struct_t stop_struct_data;
   stop_struct_data.exit=&parameters->exitsignal;
   stop_struct_data.reload=&parameters->hubsignal;

   // init next check
   gettimeofday(&last_time, NULL);
   cJSON_AddItemToObject(runner, "nextcheck", cJSON_CreateNumber((int)last_time.tv_sec ));
   runner_id=cJSON_GetObjectItem(runner, "id")->valueint;
   interval=cJSON_GetObjectItem(runner, "interval")->valueint;
   nextcheck=&cJSON_GetObjectItem(runner, "nextcheck")->valueint;

   while (!((struct check_runner_data_s *)data)->stop && !parameters->exitsignal && !parameters->hubsignal) {
      gettimeofday(&now_time, NULL);
      // get next interval

      if ((int)now_time.tv_sec >= *nextcheck  )
      {
         old_error=error_count;
         //setdebug();
         job=get_job(runner,info, &stop_struct_data, curl);
         // get error_count
         error_count=cJSON_GetObjectItem(runner, "errorcount")->valueint;
         //clrdebug();
         if (job)
		 {
            do_job(runner, job,parameters);
            cJSON_Delete(job);
            job=NULL;

         }
         // Calculate new interval and update next_check
         // see issue #61:
         //if long polling, set interval to 0 else if interval =0, set it to a minimun of 3
         if (cJSON_get_key(runner, "Long Polling"))
         {
            if (validate_key(runner, "Long Polling", "yes")==0)
              new_interval=error_count *6;
            else
            {
               if (interval ==0)
               { // if not long polling and interval is 0, set it to 3 seconds + xxx according to errorcount
                 new_interval=3 + error_count *6;
               }
               else
                 new_interval=interval + error_count *6;
            }
         }
         else
         {  // not long polling and interval is 0, set it to 3 seconds
            if (interval ==0)
              new_interval=3 + error_count *6;
            else
              new_interval=interval + error_count *6;
         }

         if (new_interval>60) new_interval=60; // max 60 seconds

          // update next check
         *nextcheck=(int)now_time.tv_sec+new_interval;
          // log info on error count get new job
         if (old_error!=error_count) alert("runner %2d: new interval %d (error_count=%d)\n",runner_id,new_interval,error_count);


      }
      else
        usleep(1000000); // it was not time yet, so sleep a second.
   }
   alert("runner %2d: exit, stop received\n",runner_id);
   ((struct check_runner_data_s *)data)->stop_ack=1;
   if (curl) curl_easy_cleanup(curl);
   return NULL;
}

int runners(parameter_t * parameters)
{
   char * print_it=NULL;
   pthread_mutex_t nginx_lock;
   pthread_mutex_t repo_lock;
   struct check_runner_data_s check_runner_data[8];
   pthread_t thread_id[8];
   //printf("deploy-runner!\n");
   while (!parameters->exitsignal)
   {
      cJSON * runners=read_runners_file(parameters->testprefix,parameters->config_file);
      if (runners==NULL)
      {
        error("runners: No runners, exit\n");
        return -1;
      }
      alert("runners: Got %d runners to handle\n",cJSON_GetArraySize(runners));

      if (cJSON_GetArraySize(runners)>8)
      {
         error("runners: Too Many Runners, Max 8 runners, exit\n");
         return -1;
      }
      cJSON * pos=NULL;
      int count=0;
      cJSON_ArrayForEach(pos,runners)
      {
         count++;
         cJSON * tmp=cJSON_GetObjectItem(pos, "interval");
         if (tmp)
         {
            if (!cJSON_IsNumber(tmp))
            {
               int interval=(int)strtol(tmp->valuestring, NULL, 10);
               cJSON_ReplaceItemInObject(pos, "interval", cJSON_CreateNumber(interval));
            }
         }
         else cJSON_AddItemToObject(pos, "interval", cJSON_CreateNumber(1));
         tmp=cJSON_GetObjectItem(pos, "errorcount");
         if (!tmp) cJSON_AddItemToObject(pos, "errorcount", cJSON_CreateNumber(0));
         tmp=cJSON_GetObjectItem(pos, "id");
         if (!tmp) cJSON_AddItemToObject(pos, "id", cJSON_CreateNumber(count));

      }
      cJSON * info=cJSON_CreateObject();
      
      cJSON_AddStringToObject(info, "name", "deployctl-runner");
      cJSON_AddStringToObject(info, "version", PACKAGE_VERSION);
      //cJSON_AddStringToObject(info, "name", "gitlab-runner");
      
      //cJSON_AddStringToObject(info, "version", "10.0.0-rc.1");
      
      cJSON_AddStringToObject(info, "revision", "1");
      cJSON_AddStringToObject(info, "platform", "Linux");
      cJSON_AddStringToObject(info, "architecture", "x86_64");
      
      //cJSON_AddStringToObject(info, "User-Agent", "gitlab-runner 10.0.0-rc.1");
      // fmt.Sprintf("%s %s (%s; %s; %s/%s)", v.Name, v.Version, v.Branch, v.GOVersion, v.OS, v.Architecture)


      alert("runners: Ready to start polling %d\n",cJSON_GetArraySize(runners));
      if (getdebug()) // TODO make this nicer
      {
         print_it =cJSON_PrintBuffered(runners, 1, 1);
         debug("%s\n",print_it);
         if (print_it) free(print_it);
      }

      cJSON * runner=NULL;
      int i=0;
      pthread_mutex_init(&nginx_lock, NULL);
      pthread_mutex_init(&repo_lock, NULL);
      parameters->repo_lock=&repo_lock;
      parameters->nginx_lock=&nginx_lock;
      cJSON_ArrayForEach(runner,runners)
      {
         check_runner_data[i].stop=0;
         check_runner_data[i].stop_ack=0;
         check_runner_data[i].info=info;
         check_runner_data[i].job=NULL;
         check_runner_data[i].parameters=parameters;

         check_runner_data[i].runner=runner;
         pthread_create(&thread_id[i], NULL,check_runner_thread, &check_runner_data[i]);
         usleep(250000); // sleep 250ms to avoid startup race.
         i++;
      }
      while (!parameters->hubsignal && !parameters->exitsignal) usleep(1000000);
      debug("runners: breakout while\n");
      int j;
      int n_runners=cJSON_GetArraySize(runners);
      for (j=0;j<n_runners;j++) check_runner_data[i].stop=1;
      alert("runners: Waiting for runners to stop...\n");
      // wait for runners to stop
      int all_stop=0;
      int stop_timout=60*10; // 10 minute wait for deployments to stop
      while (!all_stop && stop_timout)
      {
         int stop_nack=0;
         for (j=0;j<n_runners;j++) stop_nack= stop_nack + !check_runner_data[j].stop_ack;
         all_stop=(stop_nack==0);
         usleep(1000000);
         stop_timout--;
      }
      if (!stop_timout)
      {  // not all stopped, cancel the ones that did not stop
         alert("runners: Not All runners down, Cancelling runner threads\n");
         for (j=0;j<n_runners;j++)
         {
            if (!check_runner_data[j].stop_ack) pthread_cancel(thread_id[j]);
         }
      }
      // and join all together.
      alert("runners: joining runner threads\n");
      for (j=0;j<n_runners;j++) pthread_join(thread_id[j], NULL);
      alert("runners: All runners down...\n");
      parameters->hubsignal=0;
	   
      cJSON_Delete(runners);
      runners=NULL;
   }
   pthread_mutex_destroy(&nginx_lock);
   pthread_mutex_destroy(&repo_lock);

   return 0;
}
