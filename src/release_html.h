/*
 release_html.h
 Created by Danny Goossen, Gioxa Ltd on 3/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */

#ifndef __deployctl__release_html_h__
#define __deployctl__release_html_h__

#include "common.h"

//char * html_release_start(char * fout);
char * html_body_release(char * buf_in);

char * slug_it(const char * input_str, size_t len);
int validate_project_path_slug(cJSON * env_json, struct trace_Struct *trace, int suggest);
int ci_extract_server_version(const char* CI_SERVER_VERSION,int *major_v,int*minor_v,int *revision_v);
cJSON * JSON_dir_list( const char *basepath, const char *sub_path);
cJSON * JSON_link_list( const char *basepath, const char *sub_path);
cJSON * read_release_json(void * opaque, const char * dirpath);

cJSON * create_thisrelease_json( void * opaque , cJSON * release_info, int production, const char * base_path,const char * sub_path);

char * create_releases_json( void * opaque , const cJSON * this_release, cJSON ** releases, int production);

char * create_release_json_js (void * opaque, char * download_url, char * releases_json, int tag, char * last_tag);

int is_clean_tag(void * opaque, const char * tag, const char ** list);
char * check_if_git_version_file(const char * project_path);
char * check_last_tag(void * opaque,const char * basePATH);
char* read_file( const char * dirpath, const char * filename);
int  add_deploy_info_json(void * opaque,cJSON * project_json);


#endif /* defined(__deployctl__release_html_h__) */
