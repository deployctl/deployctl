/*
 dyn_buffer.h
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */
#ifndef __deployctl__dyn_buffer__
#define __deployctl__dyn_buffer__

#include "common.h"

// for curl
struct MemoryStruct {
    char *memory;
    size_t size;
};

// for curl
struct headerStruct {
   cJSON * header;
};



size_t WriteMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp);
size_t WriteHeaderCallback(void *contents, size_t size, size_t nmemb, void *userp);
size_t Writedynamicbuf(void *contents, void *userp);
size_t Writedynamicbuf_n(void *contents, size_t content_len, void *userp);
size_t init_dynamicbuf( void *userp);
void init_header_struct( void *userp);
void free_header_struct( void *userp);
void free_dynamicbuf( void *userp);

size_t Write_dynamic_trace_color(const void *contents,char * state, enum term_color,void *userp);
size_t Write_dynamic_trace(const void *contents,char * state, void *userp);
size_t Write_dynamic_trace_n(const void *contents,size_t n, void *userp);

size_t clear_dynamic_trace( void *userp);
void free_dynamic_trace( void **userp);
int init_dynamic_trace(void**userp, const char * token, const char * url, int job_id);
void clear_till_mark_dynamic_trace( void *userp);
void set_mark_dynamic_trace( void *userp);
void clear_mark_dynamic_trace( void *userp);
size_t Write_dyn_trace(void *userp,enum term_color color, const char *message, ...);
size_t Write_dyn_trace_pad(void *userp,enum term_color color, int len2pad,const char *message, ...);

const char * get_dynamic_trace( void *userp);




#endif /* defined(__deployctl__dyn_buffer__) */
