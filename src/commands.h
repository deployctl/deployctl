/*
 commands.c
 Created by Danny Goossen, Gioxa Ltd on 8/3/17.
 
 MIT License
 
 Copyright (c) 2017 deployctl, Gioxa Ltd.
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 
 */


#ifndef __deployctl__commands__
#define __deployctl__commands__

#include <stdio.h>

/*------------------------------------------------------------------------
 * Type def functions for commands
 *------------------------------------------------------------------------*/

typedef int(* do_command_t)(void *opaque);

int version (void *opaque);
int list    (void *opaque);
// the command paramlist
typedef char * t_paramlist[4] ;

// the command structure
typedef struct command_t
{
    char * request;
    t_paramlist  parmList;
    do_command_t internal_command;
    int   do_envp;
    char * user;
} command_t;

int exec_command(const char * this_command,cJSON *env_json, struct trace_Struct *trace,parameter_t * parameters);


#endif /* defined(__deployctl__commands__) */
