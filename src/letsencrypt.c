/*
  letsencrypt.c
  Created by Danny Goossen, Gioxa Ltd on 22/3/17.

MIT License

Copyright (c) 2017 deployctl, Gioxa Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

#include "deployd.h"

/*----------------------------------------------------------------------------------
 * letsencrypt the DEPLOY_DOMAIN with GITLAB_USER_EMAIL
 * exec /bin/sh certbot .....
 * returns 0 on success
 *-----------------------------------------------------------------------------------*/
int letsencrypt(void * opaque, char * domain,char * email)
{
   int exitcode=0;
   struct trace_Struct *trace=((data_exchange_t *)opaque)->trace;
   char command[4096]; // temporary string
   char dir[1024];

   ((data_exchange_t *)opaque)->needenvp=0;
   sprintf((char *)dir,"/opt/deploy/.acme.sh/%s/fullchain.cer",domain);
	debug("check %s\n",dir );
   if( access( dir, F_OK ) == -1 )
   {
      const char * testpref=((data_exchange_t *)opaque)->parameters->testprefix;
      int snres=snprintf((char *)command,4096,"%s/opt/deploy/.acme.sh/acme.sh --issue --keylength 4096 --home %s/opt/deploy/.acme.sh -w %s/opt/deploy/var -d %s",testpref,testpref,testpref,domain); //--email %s
      if (snres>=4096)
      {
         Write_dyn_trace(trace, red,"ERROR: command exeeds max length\n");
         exitcode=1;
         return(exitcode);
      }
      Write_dyn_trace(trace, none,"+ %s \n",command);
      update_details(trace);
      debug("cmd: %s\n",command);
      ((data_exchange_t *)opaque)->shellcommand=command;
      exitcode=exec_color(opaque);
      if (exitcode) {debug("ERROR: Failed to create certificates\n");}
      // check if certs
      sprintf((char *)dir,"%s/opt/deploy/.acme.sh/%s/fullchain.cer",((data_exchange_t *)opaque)->parameters->testprefix ,domain);
      if (!exitcode && access( dir, F_OK ) == -1 )
      {
         debug("ERROR: certificate missing\n");
         Write_dyn_trace(trace, red,"ERROR: certificate missing\n");
         exitcode=1;
      }
      sprintf((char *)dir,"%s/opt/deploy/.acme.sh/%s/%s.key",((data_exchange_t *)opaque)->parameters->testprefix,domain,domain);
      if (!exitcode && access( dir, F_OK ) == -1 )
      {
         debug("ERROR: priv key missing\n");
         Write_dyn_trace(trace, red,"ERROR: priv key missing\n");
         exitcode=1;
      }

   }else
   {
      Write_dyn_trace_pad(trace, none,75,"+ SSL config already exists...");
   Write_dyn_trace(trace, green,"[OK]\n");
   }
   return(exitcode);
}
