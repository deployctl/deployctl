//
//  bash_it.c
//  deployctl
//
//  Created by Danny Goossen on 6/8/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"

int check_path(char * path , int allow_wildcard,int unpriviledged)
{
   char bashed[1024];

   int ret=bash_it(bashed, 1024, path, NULL,unpriviledged);
   debug("bashed path=%s, ret=%d\n",bashed,ret);
   if (ret==0 && (strstr(bashed,"../")
                  || strchr(bashed, '?') || strchr(bashed, '%') || (strchr(bashed, '*') && !allow_wildcard )
                  || strchr(bashed, ':') || strchr(bashed, '|') || strchr(bashed, '"')
                  || strchr(bashed, ' ') || strchr(bashed, '<') || strchr(bashed, '>')  )) ret=1;
   return ret;
}

cJSON * find_sub_env(cJSON * env_vars, char * haystack)
{
   char * pos_ptr=NULL;
   cJSON *sub_env= NULL;
   cJSON * walk_vars=env_vars->child;
   int found=0;
   do{ // make a list of all posible matches
      char * needle=walk_vars->string;

      if ((pos_ptr=strstr(haystack,needle)))
      {
         char * pos=NULL;
         size_t offset=0;
         while((pos=strchr(walk_vars->valuestring+offset,'$')) && pos[1]==' ' ) {offset=pos-walk_vars->valuestring+1;}

         if(!pos) // if result contain also a "$" and not "$ " skip, for next iteration
         {
            if (!sub_env) sub_env= cJSON_CreateObject();
            cJSON_AddStringToObject(sub_env,walk_vars->string, walk_vars->valuestring);
            found++;
         }
      }
      walk_vars=walk_vars->next;
   } while (walk_vars);
   return sub_env;
}

int bash_subs_one_level(struct trace_Struct * trace,cJSON *env_vars, int level, int unpriviledged )
{
   // make a list of items with a $
   int result=0;
   cJSON *env_vars_to_bash= NULL;
   cJSON * var=NULL;

   cJSON_ArrayForEach(var,env_vars)
   {
      if (cJSON_IsString(var))
      {
         // sanitize first!!! "$(" and '`'
         // TODO need also escaped sequences that would represent $( or `
         if (strstr(var->valuestring,"$(") || strchr( var->valuestring,'`') )
         {
            if (env_vars_to_bash ) cJSON_Delete(env_vars_to_bash);
			 env_vars_to_bash=NULL;
            Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
            Write_dynamic_trace_color("\n\t ERROR: ", NULL,red,trace);
            Write_dynamic_trace("illegal characters in variable: \"", NULL,trace);
            Write_dynamic_trace_color(var->string, NULL,yellow,trace);
            Write_dynamic_trace("\"\n", NULL,trace);
            result=1;
            break;
         }
         else if(strchr( var->valuestring,'$') && !strchr( var->valuestring,' '))
         {
            char * pos=NULL;
            size_t offset=0;
            while((pos=strchr(var->valuestring+offset,'$')) && pos[1]==' ' ) {offset=pos-var->valuestring+1;}
            if (pos)
            {
               if (! env_vars_to_bash) env_vars_to_bash=cJSON_CreateObject();
               cJSON_AddStringToObject(env_vars_to_bash, var->string,  var->valuestring);
            }
         }
      }
   }
   // this is the real exit
   if (!env_vars_to_bash ) return result;


   cJSON * walk_to_bash=env_vars_to_bash->child;
   do
   {
      // we make list of vars we need to substitude this level.
      cJSON *sub_env= find_sub_env(env_vars, walk_to_bash->valuestring);
      if (!sub_env)
      {
         debug("level: %d, not found any matches: %s\n",level,walk_to_bash->string);
         result=1;
      }
      else{

         char bash_std_buf[1024];

         
         result=bash_it(bash_std_buf,1024,walk_to_bash->valuestring ,sub_env,unpriviledged);
         cJSON_Delete(sub_env);
		  sub_env=NULL;
         if (result) {
            error("bash_it subs:%s",bash_std_buf);
            Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
            Write_dynamic_trace_color("\n\t ERROR: \"", NULL,red,trace);
            Write_dynamic_trace(bash_std_buf, NULL,trace);
            Write_dynamic_trace_color("\"\n", NULL,red,trace);
            break;
         }
         //debug("%d:replacing var %s=\"%s\" with %s\n",level,walk_to_bash->string,walk_to_bash->valuestring,bash_std_buf);
         // and replace with the new result.
         cJSON_ReplaceItemInObject(env_vars, walk_to_bash->string, cJSON_CreateString( bash_std_buf));
      }
      // and get the next child
      walk_to_bash=walk_to_bash->next;
   } while (walk_to_bash);

   if (level==0 && result)
   {
      Write_dynamic_trace_color("\t [Failed]\n", NULL,red, trace);
      Write_dynamic_trace_color("\n\t ERROR Could not enumerate variables:\n", NULL,red,trace);
      cJSON * walk_to_bash=env_vars_to_bash->child;
      do{
         Write_dynamic_trace("\t - ", NULL,trace);
         Write_dynamic_trace(walk_to_bash->string, NULL,trace);
         Write_dynamic_trace(": \"", NULL,trace);
         Write_dynamic_trace(walk_to_bash->valuestring, NULL,trace);
         Write_dynamic_trace("\"\n", NULL,trace);
         walk_to_bash=walk_to_bash->next;
      } while (walk_to_bash);
   }

   if (!result && level<2)  result=bash_subs_one_level(trace,env_vars, level+1 ,unpriviledged);
   else if (!result)
   {
      Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
      Write_dynamic_trace_color("\n\t ERROR: ", NULL,red, trace);
      Write_dynamic_trace("Could not enumerate variables within 3 iterations:\n", NULL,trace);
      cJSON * walk_to_bash=env_vars_to_bash->child;
      do{
         Write_dynamic_trace("\t - ", NULL,trace);
         Write_dynamic_trace(walk_to_bash->string, NULL,trace);
         Write_dynamic_trace(": \"", NULL,trace);
         Write_dynamic_trace(walk_to_bash->valuestring, NULL,trace);
         Write_dynamic_trace("\"\n", NULL,trace);
         walk_to_bash=walk_to_bash->next;
      } while (walk_to_bash);

      result=1;

   }

   cJSON_Delete(env_vars_to_bash);
	env_vars_to_bash=NULL;
   return result;
}





// sub special ones

int bash_subs_specials(struct trace_Struct * trace,cJSON *env_vars, int unpriviledged)
{
   // make a list of items with a $
   int result=0;
   cJSON *env_vars_to_bash= NULL;
   cJSON * var=NULL;
   cJSON_ArrayForEach(var,env_vars)
   {
      if (cJSON_IsArray(var))
      {
         cJSON*tmp=NULL;
         cJSON_ArrayForEach(tmp,var)
         {
            cJSON * item;
            if (cJSON_IsString(tmp))
               item=tmp;
            else if (cJSON_IsObject(tmp))
               item=tmp->child;
            else
            {
               Write_dyn_trace(trace, red, "don't have a clue what this is\n");
               result=1;
               break;
            }

            if  ( (item->valuestring && (strstr(item->valuestring,"$(") || strchr( item->valuestring,'`')) ) ||
                 (item->string      && (strstr(item->string,     "$(") || strchr( item->string,     '`')) )  )
            {
               if (env_vars_to_bash ) cJSON_Delete(env_vars_to_bash);
				env_vars_to_bash=NULL;
               Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
               Write_dynamic_trace_color("\n\t ERROR: ", NULL,red,trace);
               Write_dynamic_trace("illegal characters in variable: \"", NULL,trace);
               Write_dynamic_trace_color(var->string, NULL,yellow,trace);
               Write_dynamic_trace("\"\n", NULL,trace);
               result=1;
               break;
            }
            else if(strchr( item->valuestring,'$')&& !strchr( item->valuestring,' ') )
            {
               char * pos=NULL;
               size_t offset=0;
               while((pos=strchr(item->valuestring+offset,'$')) && pos[1]==' ' ) {offset=pos-item->valuestring+1;}
               if (pos)
               {
                  char bash_std_buf[1024];
                  cJSON *sub_env= find_sub_env(env_vars, item->valuestring);
                  if (!sub_env)
                  {
                     debug("specials: not found any matches: %s\n",item->valuestring);
                     if (item->valuestring) free(item->valuestring);
                     item->valuestring=strdup("");
                  }
                  else
                  {
                     result=bash_it(bash_std_buf,1024,item->valuestring ,sub_env, unpriviledged);
                     cJSON_Delete(sub_env);
                     if (result) {
                        error("bash_it specials:%s",bash_std_buf);
                        Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
                        Write_dynamic_trace_color("\n\t ERROR: \"", NULL,red,trace);
                        Write_dynamic_trace(bash_std_buf, NULL,trace);
                        Write_dynamic_trace_color("\"\n", NULL,red,trace);
                        result=1;
                        break;
                     }
                     if (item->valuestring) free(item->valuestring);
                     item->valuestring=strdup(bash_std_buf);
                  }
               }
            }
            else if(item->string && strchr( item->string,'$'))
            {
               char * pos=NULL;
               size_t offset=0;
               while((pos=strchr(item->string+offset,'$')) && pos[1]==' ' ) {offset=pos-item->string+1;}
               if (pos)
               {
                  char bash_std_buf[1024];
                  cJSON *sub_env= find_sub_env(env_vars, item->string);
                  if (!sub_env)
                  {
                     debug("specials: not found any matches: %s\n",item->string);
                     free(item->string);
                     item->string=strdup("");
                    
                  }
                  else
                  {
                     result=bash_it(bash_std_buf,1024,item->string ,sub_env,unpriviledged);
                     cJSON_Delete(sub_env);
					  sub_env=NULL;
                     if (result) {
                        error("bash_it:%s",bash_std_buf);
                        Write_dynamic_trace_color("\t [FAILED]\n", NULL,red, trace);
                        Write_dynamic_trace_color("\n\t ERROR: \"", NULL,red,trace);
                        Write_dynamic_trace(bash_std_buf, NULL,trace);
                        Write_dynamic_trace_color("\"\n", NULL,red,trace);
                        result=1;
                        break;
                     }
                     if (item->string)free(item->string);
                     item->string=strdup(bash_std_buf);
                  }
               }
            }
         }
      }
   }
   return result;
}

int post_process_deploy_path( cJSON * env_vars,struct trace_Struct *trace, const char * name , const char * default_source, const char * default_target, int unpriviledged)
{
   int result=0;
   cJSON  * temp=cJSON_GetObjectItem(env_vars, name);

   if (!cJSON_IsArray(temp))
   {   Write_dyn_trace_pad(trace, none, 76,"\n    + %s:",name);
      Write_dyn_trace(trace, red, "\n    ERROR: expected array-list of :  \n  \'[(\"<target_path>\":) \"<source_path>\",... ]\'\n");
      result=1;
   }
   else
   { int ill_res=0;
      cJSON*path=NULL;
      cJSON_ArrayForEach(path,temp)
      {
         if(!((cJSON_IsObject(path) && path->child->valuestring && path->child->string) || cJSON_IsString(path)))
         {
            result++;
         }
         else if (cJSON_IsObject(path))
         {
            ill_res+=check_path(path->child->string,0,unpriviledged) + check_path(path->child->valuestring,1,unpriviledged);
            if (!ill_res)
            {
               if (path->child->string && path->child->string[0]!='/')
               {
                  char * tmp=calloc(strlen(path->child->string)+2, 1);
                  sprintf(tmp, "/%s",path->child->string);
                  free(path->child->string);
                  path->child->string=tmp;
               }
               if (path->child->valuestring && path->child->valuestring[0]!='/')
               {
                  char * tmp=calloc(strlen(path->child->valuestring)+2, 1);
                  sprintf(tmp, "/%s",path->child->valuestring);
                  free(path->child->valuestring);
                  path->child->valuestring=tmp;
               }
            }
         }
         else if (cJSON_IsString(path))
         {
            ill_res+=check_path(path->valuestring,1,unpriviledged);
            if (!ill_res && path->valuestring[0]!='/')
            {
               char * tmp=calloc(strlen(path->valuestring)+2, 1);
               sprintf(tmp, "/%s",path->valuestring);
               free(path->valuestring);
               path->valuestring=tmp;
            }
         } else result++;

      }
      if (result)
      {
         Write_dyn_trace_pad(trace, none, 76,"\n    + %s:",name);
         Write_dyn_trace(trace, red, "\n    ERROR: expected array-list of :  \n  \'[(\"<target_path>\":) \"<source_path>\",... ]\'\n");
      }
      if (ill_res)
      {
         Write_dyn_trace_pad(trace, none, 76,"\n    + %s:",name);
         Write_dyn_trace(trace, red, "\n    ERROR: Illegal characters in path or \"../\" not allowed/\n");
         result=1;
      }
      // need to check if they all start with /, if not change!!
   }

   {
	   /*
       Write_dyn_trace(trace, green, "[ok]\n");
	   
       char * print_it=cJSON_PrintBuffered(cJSON_GetObjectItem(env_vars, name),1,0);
       if (print_it)
       {
         if (strcmp("\"\"",print_it)==0)
         {
         free(print_it);
         print_it=calloc(1,5);
         sprintf(print_it,"[none]");
       }
		
       if (!result)
       Write_dyn_trace(trace, bold_cyan, " %s\n",print_it);
       else
       Write_dyn_trace(trace, bold_magenta, "    - %s\n",print_it);
       free(print_it);
       }
	   */
   }
   return result;
}

int process_deploy_path( cJSON * env_vars,struct trace_Struct *trace, const char * name, const char * default_source, const char * default_target,int unpriviledged )
{
   int result=0;
   int need_target=1;
   if (!default_target) need_target=0;
   char * deploy_release_path=cJSON_get_key(env_vars, name);

   if (deploy_release_path)
   {
      debug("%s=>%s<\n",name,deploy_release_path);

      cJSON * test=yaml_sting_2_cJSON(NULL, deploy_release_path);
      if (test)
      {
         cJSON * temp=cJSON_GetArrayItem(test, 0);
         if (!cJSON_IsArray(temp))
         {
            if(cJSON_IsString(temp) )
            {
               cJSON * json_path_array=cJSON_CreateArray();
               if (need_target)
               {
                  cJSON* path_obj=cJSON_CreateObject();
                  cJSON_AddStringToObject(path_obj, default_target,temp->valuestring );
                  cJSON_AddItemToArray(json_path_array, path_obj);
               }
               else
               {
                  cJSON_AddItemToArray(json_path_array, cJSON_CreateString(temp->valuestring));
               }
               cJSON_ReplaceItemInObject(env_vars, name, json_path_array);
            }
            else if ((cJSON_IsObject(temp) && temp->valuestring) )
            {
               cJSON * json_path_array=cJSON_CreateArray();
               if (need_target)
               {
                  cJSON* path_obj=cJSON_CreateObject();
                  cJSON_AddStringToObject(path_obj, temp->string,temp->valuestring );
                  cJSON_AddItemToArray(json_path_array, path_obj);
               }
               else
               {
                  cJSON_AddItemToArray(json_path_array, cJSON_CreateString(temp->valuestring));
               }
               cJSON_ReplaceItemInObject(env_vars, name, json_path_array);
            }
            else if (cJSON_IsNumber(temp))
            {
               char buffer [33];
               snprintf(buffer, 33,"%d",temp->valueint);
               cJSON * json_path_array=cJSON_CreateArray();
               if (need_target)
               {
                  cJSON* path_obj=cJSON_CreateObject();
                  cJSON_AddStringToObject(path_obj, default_target,buffer );
                  cJSON_AddItemToArray(json_path_array, path_obj);
               }
               else
               {
                  cJSON_AddItemToArray(json_path_array, cJSON_CreateString(buffer));
               }
               cJSON_ReplaceItemInObject(env_vars, name, json_path_array);
            }
            else if (cJSON_IsBool(temp))
            {
               // no boolean reserved names allowed
               Write_dyn_trace_pad(trace, none,76, "\n    + %s:",name);
               Write_dyn_trace(trace, red, "ERROR: no boolean yaml reserved names allowed as %s\n",name);
               result=1;
            }
            else
            {Write_dyn_trace_pad(trace, none,76, "\n    + %s:",name);
               Write_dyn_trace(trace, red, "\n    ERROR: expected array-list of :  \n  \'[(\"<target_path>\":) \"<source_path>\",... ]\'\n");
               result=1;
            }
         }
         else
         {  // we have array, check if array is ok
            // for each and check and replace
            cJSON*path=NULL;
            cJSON_ArrayForEach(path,temp)
            {
               if((cJSON_IsObject(path) && path->child->valuestring && path->child->string))
               {
                  if (!need_target)
                  {  // Need a string, take valuestring from object and delete object!, discard key
                     cJSON * tmp=path->child;
                     path->child=NULL;
                     path->type=cJSON_String;
                     path->valuestring=strdup(tmp->valuestring);
                     cJSON_Delete(tmp);
                     debug("changed obj to string, discard key value\n");
                  }
                  result=0;
               }
               else if (cJSON_IsString(path)&& !cJSON_IsObject(path))
               {
                  // detach and replace
                  if (need_target)
                  { // change to object with default_target as key
                     // take value string out
                     char * tmp=path->valuestring;
                     path->valuestring=NULL;
                     path->type=cJSON_Object;
                     // add string_object to path
                     cJSON_AddStringToObject(path, default_target, tmp);
                     free(tmp);
                     debug("changed string to obj with default_target as key\n");
                  }
               }
               else
               {  Write_dyn_trace_pad(trace, none,76, "\n    + %s:",name);
                  Write_dyn_trace(trace, red, "\n    ERROR: expected array-list of :  \n  \'[(\"<target_path>\":) \"<source_path>\",... ]\'\n");
                  result=1;
               }
            }
            cJSON_ReplaceItemInObject(env_vars, name,cJSON_Duplicate(temp,1));
         }
      
         cJSON_Delete(test);
      }
      else if (deploy_release_path[0]==0)
      {// nothing so create a ["<target_path>": "<source_path>"
         cJSON * json_path_array=cJSON_CreateArray();
         if (need_target)
         {
            cJSON* path_obj=cJSON_CreateObject();
            cJSON_AddStringToObject(path_obj, default_target,default_source );
            cJSON_AddItemToArray(json_path_array, path_obj);
         }
         else
         {
            cJSON_AddItemToArray(json_path_array,cJSON_CreateString(default_source));
         }
         cJSON_ReplaceItemInObject(env_vars, name, json_path_array);
      }
      else
      {  Write_dyn_trace_pad(trace, none,76, "\n    + %s:",name);
         Write_dyn_trace(trace, red, "\n    ERROR: expected array-list of :  \n  \'[(\"<target_path>\":) \"<source_path>\",... ]\'\n");
         result=1;
         debug("Fail to yaml deploy release path\n");
      }
   }
   else
   {  // nothing so create a ["<target_path>": "<source_path>"
      cJSON * json_path_array=cJSON_CreateArray();
      if (need_target)
      {
         cJSON* path_obj=cJSON_CreateObject();
         cJSON_AddStringToObject(path_obj, default_target,default_source );
         cJSON_AddItemToArray(json_path_array, path_obj);
      }
      else
      {
         cJSON_AddItemToArray(json_path_array,cJSON_CreateString(default_source));
      }
      cJSON_AddItemToObject(env_vars, name, json_path_array);
   }
   if (!result)
   {
	   /*
       Write_dyn_trace(trace, green, "[ok]\n");
	   
       char * print_it=cJSON_PrintBuffered(cJSON_GetObjectItem(env_vars, name),1,0);
       if (print_it)
       {
       if (strcmp("\"\"",print_it)==0)
       {
       free(print_it);
       print_it=calloc(1,5);
       sprintf(print_it,"[\".\"]");
       }
		
       Write_dyn_trace(trace, bold_cyan, " %s\n",print_it);
       free(print_it);
       }
	   */
   }
   return result;
}

int post_process_deploy_href( cJSON * env_vars,struct trace_Struct *trace )
{
   int result=0;

   cJSON * temp=cJSON_GetObjectItem(env_vars, "DEPLOY_hrefs");
   if (!cJSON_IsArray(temp))
   {     Write_dyn_trace_pad(trace, none,76 ,"\n    + %s:","DEPLOY_hrefs");
      Write_dyn_trace(trace, red, "ERROR: expected array of \'[\"<name>\": \"<href>\",... ]\'\n");
      result=1;
   }
   else
   {
      cJSON*path=NULL;
      cJSON_ArrayForEach(path,temp)
      {
         if(!(cJSON_IsObject(path) && path->child->valuestring && path->child->string))
         {
            Write_dyn_trace_pad(trace, none,76 ,"\n    + %s:","DEPLOY_hrefs");
            Write_dyn_trace(trace, red, "ERROR: expected array of \'[\"<name>\": \"<href>\",... ]\'\n");
            result=1;
         }
      }
   }
   if (!result)
   {
      //Write_dyn_trace(trace, green, "[ok]\n");
      /*
       char * print_it=cJSON_PrintBuffered(cJSON_GetObjectItem(env_vars, "DEPLOY_hrefs"),1,0);
       if (print_it)
       {
       if (strcmp("\"\"",print_it)==0)
       {
       free(print_it);
       print_it=calloc(1,5);
       sprintf(print_it,"[none]");
       }
       Write_dyn_trace(trace, bold_cyan, " %s\n",print_it);
       free(print_it);
       }
       */
   }
   return result;
}
int process_deploy_href( cJSON * env_vars,struct trace_Struct *trace )
{
   int result=0;
   char * deploy_release_path=cJSON_get_key(env_vars, "DEPLOY_hrefs");

   if (deploy_release_path)
   {
      debug("%s=>%s<\n","DEPLOY_hrefs\n",deploy_release_path);

      cJSON * test=yaml_sting_2_cJSON(NULL, deploy_release_path);
      if (test)
      {
         cJSON * temp=cJSON_GetArrayItem(test, 0);
         if (!cJSON_IsArray(temp))
         {
            if(cJSON_IsObject(temp) && temp->valuestring && temp->string)
            {
               cJSON * json_path_array=cJSON_CreateArray();
               cJSON_AddItemToArray(json_path_array, cJSON_Duplicate(temp, 1));
               cJSON_DeleteItemFromObject(env_vars, "DEPLOY_hrefs");
               cJSON_AddItemToObject(env_vars, "DEPLOY_hrefs", json_path_array);
            }
            else
            {
               Write_dyn_trace_pad(trace, none,76, "\n    + DEPLOY_hrefs:");
               Write_dyn_trace(trace, red, "ERROR: expected array of \'[\"<name>\": \"<href>\",... ]\'\n");
               result=1;
            }
         }
         else
         {
            cJSON*path=NULL;
            cJSON_ArrayForEach(path,temp)
            {
               if(!(cJSON_IsObject(path) && path->child->valuestring && path->child->string))
               {
                  Write_dyn_trace_pad(trace, none,76, "\n    + DEPLOY_hrefs:");
                  Write_dyn_trace(trace, red, "ERROR: expected array of \'[\"<name>\": \"<href>\",... ]\'\n");
                  result=1;
               }
            }
            if (!result)
            {
               cJSON_DeleteItemFromObject(env_vars, "DEPLOY_hrefs");
               cJSON_AddItemToObject(env_vars, "DEPLOY_hrefs", cJSON_Duplicate(temp, 1));
            }
         }
         cJSON_Delete(test);
      }
      else if (deploy_release_path[0]==0)
      { // add empty array
         cJSON * json_path_array=cJSON_CreateArray();
         cJSON_DeleteItemFromObject(env_vars,"DEPLOY_hrefs" );
         cJSON_ReplaceItemInObject(env_vars, "DEPLOY_hrefs", json_path_array);
      }
      else
      {
         Write_dyn_trace_pad(trace, none,75, "    + DEPLOY_hrefs:");
         Write_dyn_trace(trace, red, "ERROR: expected array of \'[\"<name>\": \"<href>\",... ]\'\n");
         result=1;
         debug("Fail to yaml deploy_href\n");
      }
   }
   else
   {  // add empty array
      cJSON * json_path_array=cJSON_CreateArray();
      cJSON_AddItemToObject(env_vars,"DEPLOY_hrefs" , json_path_array);
   }
   if (!result)
   {
      //Write_dyn_trace(trace, green, "[ok]\n");
      /*
       char * print_it=cJSON_PrintBuffered(cJSON_GetObjectItem(env_vars, "DEPLOY_hrefs"),1,0);
       if (print_it)
       {
       if (strcmp("\"\"",print_it)==0)
       {
       free(print_it);
       print_it=calloc(1,5);
       sprintf(print_it,"[none]");
       }
       Write_dyn_trace(trace, bold_cyan, " %s\n",print_it);
       free(print_it);
       }
       */
   }
   return result;
}

int pre_project_path_slug_check(cJSON* env_vars, void * trace)
{
	int res=-1;
	debug ("pre-check Project path slug\n");
	const char * CI_PROJECT_PATH_SLUG=cJSON_get_key(env_vars, "CI_PROJECT_PATH_SLUG");
	const char * CI_SERVER_VERSION=cJSON_get_key(env_vars, "CI_SERVER_VERSION");
	int major_v=0;
	int minor_v=0;
	ci_extract_server_version(CI_SERVER_VERSION,&major_v,&minor_v,NULL);
	if (major_v>=10) // check, should be ok, if not error
	{
		res=validate_project_path_slug(env_vars, trace, 1);
		cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_True);
	}
	else if (major_v==9 && minor_v>=3) // don't known if it was manualy defined, if not it could be wrong, no suggestions/no errors here
	{
		res=0; // we don't generate error here, up to application
		if (validate_project_path_slug(env_vars, trace, 0)==0)
		{
			cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_True);
		}
		else cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_False);
	}
	else if (CI_PROJECT_PATH_SLUG)// version <9.3, if defined, it's manual and it should be reported with correction if wrong
	{
		if ((res=validate_project_path_slug(env_vars, trace, 1)))
		{
			cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_True);
		}
		else
		{
			cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_False);
		}
	}
	else // version <9.3, CI_PROJECT_PATH_SLUG not defined, ok, up to application to check
	{
		res=0;
		cJSON_AddBoolToObject(env_vars,"CI_PROJECT_PATH_SLUG_VERIFIED", cJSON_False);
	}
	return res;
}

int verify_project_name_path_namespace(cJSON* env_vars, void * trace)
{
	int res=1;
	debug ("check Project/namespace/path\n");
	//CI_PROJECT_PATH
	const char * CI_PROJECT_PATH=strstr(cJSON_get_key(env_vars,"CI_PROJECT_URL" ),cJSON_get_key(env_vars,"CI_PROJECT_PATH"));
	if (CI_PROJECT_PATH)
	{  // ok we found project path
		if (strstr(CI_PROJECT_PATH, cJSON_get_key(env_vars, "CI_PROJECT_NAMESPACE"))== CI_PROJECT_PATH)
		{
			//ok project path start // +1 is the /
			size_t namespace_len=strlen(cJSON_get_key(env_vars, "CI_PROJECT_NAMESPACE"));
			if(strstr(CI_PROJECT_PATH+namespace_len+1, cJSON_get_key(env_vars, "CI_PROJECT_NAME"))== CI_PROJECT_PATH+namespace_len+1)
			{
				// yeah All good
				res=0;
				debug("project name/namespace/path OK, not Frauded\n");
			}
			else error("project_name trouble\n");
		}
		else error("namespace not found in project_url\n");
	}
	else error("project path not found in project_url\n");

		return res;
}

int verify_project_url(cJSON*job,cJSON*env_vars,void*trace)
{  // deduct project_url from job_git_info_repo_url
	
	int res=1;
	char * repo_url_git_info=cJSON_get_key(cJSON_GetObjectItem(job, "git_info"), "repo_url");
	char * repo_url_env_var=cJSON_get_key(env_vars, "CI_REPOSITORY_URL");
	if (strcmp(repo_url_env_var,repo_url_git_info)!=0)
	{ // modify env var to reality
		cJSON_DeleteItemFromObject(env_vars, "CI_REPOSITORY_URL");
		cJSON_AddStringToObject(env_vars, "CI_REPOSITORY_URL", repo_url_git_info);
		debug ("Changed CI_REPOSITORY_URL ev: %p",env_vars);
	}
	// "https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.gioxa.com/deployctl/testci.git" get url / namespace / project
	char * deduct_project_url=NULL;
	char * p=strstr(repo_url_git_info,"://");
	if (p)
	{
		size_t scheme_len=(size_t)(p-repo_url_git_info);
		char * p=strstr(repo_url_git_info, cJSON_get_key(env_vars, "CI_REGISTRY_USER"));
		if (p)
		{
			p=strstr(p, cJSON_get_key(env_vars, "CI_REGISTRY_PASSWORD"));
			if (p && p[strlen(cJSON_get_key(env_vars, "CI_REGISTRY_PASSWORD"))]=='@')
			{
				p=p+strlen(cJSON_get_key(env_vars, "CI_REGISTRY_PASSWORD"))+1; //
				char *tmp=strstr(p,".git");
				if (tmp)
				{
					// we got it
					deduct_project_url=calloc(1,strlen(repo_url_git_info)); // should always fit
					debug ("scheme : %*.s len %d",5,repo_url_git_info,scheme_len);
					memcpy(deduct_project_url, repo_url_git_info, scheme_len+3 ); // ("http(s)://")
					memcpy(deduct_project_url+ strlen(deduct_project_url), p, (tmp-p) );
					
					debug("deducted CI_PROJECT_URL: %s\n",deduct_project_url);
				} else error("no .git\n");
			}else error("Passwd not in git_url\n");
		} else error("no CI_registry user in gitrepo");
	} else error("could not find : and d bs \n");
	if (deduct_project_url)
	{
		res=0;
		if(strcmp(deduct_project_url,cJSON_get_key(env_vars,"CI_PROJECT_URL" ))==0)
		{
			
			debug("CI_PROJECT_URL is OK, DONE env not frauded\n");
		}
		else
		{
			cJSON_DeleteItemFromObject(env_vars, "CI_PROJECT_URL");
			cJSON_AddStringToObject(env_vars, "CI_PROJECT_URL", deduct_project_url);
			alert("ci_project_url was frauded, corrected\n");
			
		}
		free(deduct_project_url);
	}
	return res;
}
