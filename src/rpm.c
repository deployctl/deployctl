//
//  rpm.c
//  deployctl
//
//  Created by Danny Goossen on 29/5/17.
//  Copyright (c) 2017 Danny Goossen. All rights reserved.
//

#include "deployd.h"

#ifdef __APPLE__
#include <rpm/rpm4compat.h>
#else
#include <rpm/rpmlegacy.h>
#include <rpm/rpmlib.h>
#include <rpm/header.h>
#include <rpm/rpmts.h>
#include <rpm/rpmdb.h>
#include <rpm/rpmio.h>
#endif
#include "rpm.h"

// TODO, feed back info on feedback in opague
// TODO Remove RPMLOG
/*
 Returns if susccessfull:
 {
	"name":	"test",
	"version":	"0.1",
	"arch":	"x86_64",
	"release":	"1",
	"distribution":	"el7",
	"SRPM":	false,
	"filename":	"test-0.1-1.el7.centos.x86_64.rpm",
	"filepath":	"./test_check_rpm/projectdir/rpm/"
 }

 */

#ifndef __APPLE__
static inline int headerGetEntry(Header h, rpm_tag_t tag, rpm_tagtype_t * type, void ** p, rpm_count_t * c) {
	//HE_t he = (HE_t)memset(alloca(sizeof(*he)), 0, sizeof(*he));
	struct  rpmtd_s td;
	td.tag=tag;
	int rc;
	/* Always ensure to initialize */
	*(void **)p = NULL;
	rc = headerGet(h, tag,(rpmtd)&td, 0);
	if (rc) {
		if (type) *type = td.type;
		if (p) *(void **) p = td.data;
		if (c) *c = td.count;
	}
	return rc;
}
#else
#define	_RPMVSF_NODIGESTS	\
( RPMVSF_NOSHA1HEADER |	\
RPMVSF_NOMD5HEADER |	\
RPMVSF_NOSHA1 |		\
RPMVSF_NOMD5 )

#define	_RPMVSF_NOSIGNATURES	\
( RPMVSF_NODSAHEADER |	\
RPMVSF_NORSAHEADER |	\
RPMVSF_NODSA |		\
RPMVSF_NORSA )
#endif


int get_rpm_fileinfo(void * opaque, const char * filepath, const char * filename,cJSON ** rpm_info)
{
	if (!filepath || !filename ) return RPM_ERR_BADARG;

	rpmts ts;
	FD_t fd;
	rpmRC rc;
	Header hdr;
	rpmVSFlags vsflags = 0;

	rc = rpmReadConfigFiles(NULL, NULL);
	if (rc != RPMRC_OK) {
		//rpmlog(RPMLOG_NOTICE, "Unable to read RPM configuration.\n");
      * rpm_info =cJSON_CreateObject();
      cJSON_AddStringToObject(*rpm_info, "error", "system rpm config");
      cJSON_AddStringToObject(*rpm_info, "type", "rpm");
      cJSON_AddStringToObject(*rpm_info, "filename", filename);
		return(RPM_ERR_SYSTEM);
	}
	char * file_path=alloca(strlen(filepath) +1+strlen(filename));
	sprintf(file_path, "%s%s",filepath,filename);
	fd = Fopen(file_path, "r.ufdio");
	if ((!fd) || Ferror(fd)) {
		//rpmlog(RPMLOG_NOTICE, "Failed to open package file (%s)\n", Fstrerror(fd));
		if (fd) {
			Fclose(fd);
		}
      * rpm_info =cJSON_CreateObject();
      cJSON_AddStringToObject(*rpm_info, "error", "failed open rpm");
      cJSON_AddStringToObject(*rpm_info, "type", "rpm");
      cJSON_AddStringToObject(*rpm_info, "filename", filename);
		return(RPM_ERR_OPEN);
	}

	ts = rpmtsCreate();

     vsflags |= _RPMVSF_NODIGESTS;
     vsflags |= _RPMVSF_NOSIGNATURES;
	  vsflags |= RPMVSF_NOHDRCHK;
	(void) rpmtsSetVSFlags(ts, vsflags);

	rc = rpmReadPackageFile(ts, fd, NULL, &hdr);
	if (rc != RPMRC_OK) {
		//rpmlog(RPMLOG_NOTICE, "Could not read package file %s\n",file_path);
		Fclose(fd);
		rpmtsFree(ts);
      * rpm_info =cJSON_CreateObject();
      cJSON_AddStringToObject(*rpm_info, "error", "could not read rpm");
      cJSON_AddStringToObject(*rpm_info, "type", "rpm");
      cJSON_AddStringToObject(*rpm_info, "filename", filename);
		return(RPM_ERR_READ);
	}
	Fclose(fd);

	Header h = headerLink( hdr );
	const char *n =NULL;
	const char *v=NULL;
	const char *r=NULL;
   const char *d=NULL;
	const char *a=NULL;
	const char *o=NULL;
	const char *s=NULL;
	headerGetEntry( h, RPMTAG_NAME, NULL, (void**)&n, NULL);
	headerGetEntry( h, RPMTAG_VERSION, NULL, (void**)&v, NULL);
	headerGetEntry( h, RPMTAG_RELEASE, NULL, (void**)&r, NULL);
	headerGetEntry( h, RPMTAG_ARCH, NULL, (void**)&a, NULL);
	headerGetEntry( h, RPMTAG_OS, NULL, (void**)&o, NULL);
   headerGetEntry( h, RPMTAG_DISTRIBUTION, NULL, (void**)&d, NULL);

	headerGetEntry( h, RPMTAG_SOURCERPM, NULL, (void**)&s, NULL);

	if (n){
		 * rpm_info =cJSON_CreateObject();
		if (n) cJSON_AddStringToObject(*rpm_info, "name", n);
		if (v) cJSON_AddStringToObject(*rpm_info, "version", v);
		if (a) cJSON_AddStringToObject(*rpm_info, "arch", a);
      if (d && strstr(d,"openSUSE")) cJSON_AddStringToObject(*rpm_info, "distribution", d);
      else if (r) {
			const char *ptr = strchr(r,'.');
			if (ptr){
				const char *centos =strstr(r,".centos");
				if (centos)
					cJSON_AddStringToObject_n(*rpm_info, "distribution", ptr+1,(size_t)(centos-ptr-1));
				else
					cJSON_AddStringToObject(*rpm_info, "distribution", ptr+1);
			}
		}
		cJSON_AddBoolToObject(*rpm_info, "SRPM", s==NULL);
		cJSON_AddStringToObject(*rpm_info, "filename", filename);
		if (filepath)cJSON_AddStringToObject(*rpm_info, "filepath", filepath);
      cJSON_AddStringToObject(*rpm_info, "fullpath", file_path);
      cJSON_AddStringToObject(*rpm_info, "type", "rpm");
		headerFree(hdr);
		rpmtsFree(ts);
      //fprintf(stderr,"RPM info OK\n");
		return RPM_OK;
	}
	else
	{
      * rpm_info =cJSON_CreateObject();
      cJSON_AddStringToObject(*rpm_info, "error", "invalid rpm-file");
      cJSON_AddStringToObject(*rpm_info, "type", "rpm");
      cJSON_AddStringToObject(*rpm_info, "filename", filename);
		//fprintf(stderr,"no valid RPM\n");
		headerFree(hdr);
		rpmtsFree(ts);
		return RPM_ERR_INVALID;
	}
	return(0);
}
