/*
 test_gitlab_api.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */



#include <check.h>
#include "../src/deployd.h"
#include "test_web_api.h"

webserver_t web;

START_TEST(check_gitlab_api)
{
   u_int16_t port=0;
   pid_t child_pid;
   setdebug();
   int result=0;
   const char * project_path="path/test";
   const char * project_id="1";
   const char * tag="111";
   const char * job_token="abcdefg";
   cJSON *test_json_api=cJSON_CreateObject();
   cJSON_AddItemToObject(test_json_api,"test_gitlab_api", cJSON_CreateString("succes"));



   // what content we want the api to serve if he got the request matching request
   char * temp=cJSON_Print(test_json_api);
   sprintf(web.reply_content,"%s",temp);
   free(temp);


   // what response headers we want to sent back, in case request matches
   sprintf(web.reply_header,"%s","HTTP/1.1 200 OK\r\nServer: testweb/0.0.1\r\nContent-Length:%d\r\n\r\n%s");


   // what reply on fail
   const char * m404_mesg="{\"message\":\"404 NOT FOUND\"}";
   sprintf(web.reply_fail,"HTTP/1.1 404 NOT FOUND\r\nServer: testweb/0.0.1\r\nContent-Length:%lu\r\n\r\n%s",strlen(m404_mesg),m404_mesg);

   // what we expect the api to get as request as condition not to reply fail
   sprintf(web.request,"GET /api/v4/projects/%s/repository/tags/%s HTTP/1.1\r\n" \
           "Host: localhost:%%d\r\n" \
           "Accept: */*\r\n" \
           "JOB_TOKEN: %s\r\n" \
           "\r\n" ,project_id,tag,job_token);

   // start test server
   result=webserver(&child_pid,&port,&web,0);
   ck_assert_int_eq(result, 0);
   ck_assert_int_ne(port, 0);
      debug("result create webserver server: %d, port=%d\n",result,port);

   // prepare environment for command:

   data_exchange_t data_exchange;
   parameter_t parameters;
   void * opaque=&data_exchange;

   int exitcode=0;
   char * newarg[4];
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;

   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   //ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;
   data_exchange.paramlist=(char **)newarg;
   data_exchange.parameters=&parameters;
   data_exchange.parameters->current_user=1;

   char project_url[1024];
   sprintf(project_url, "http://localhost:%d/%s",port,project_path);
   cJSON_AddItemToObject(env_json,"CI_PROJECT_URL"      , cJSON_CreateString(project_url));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_PATH" , cJSON_CreateString(project_path));
   cJSON_AddItemToObject(env_json,"CI_PROJECT_ID", cJSON_CreateString(project_id));
   cJSON_AddItemToObject(env_json,"CI_COMMIT_TAG", cJSON_CreateString(tag));
   cJSON_AddItemToObject(env_json,"CI_JOB_TOKEN", cJSON_CreateString(job_token));

   cJSON * gitlab_release=NULL;
   int result_getrelease=0;

   // -----------------------------------------------------------------------------
   // Test one, all conditions ok, should return 0 and have a valid gitlab_release
   // -----------------------------------------------------------------------------
   result_getrelease=get_release(opaque, &gitlab_release);
   ck_assert_int_eq(result_getrelease, 0);
   ck_assert_ptr_ne(gitlab_release, NULL);
   cJSON_Delete(gitlab_release);
   gitlab_release=NULL;
   ck_assert_ptr_eq(gitlab_release, NULL);

   // stop
   stop_test_web(child_pid);

   // -----------------------------------------------------------------------------
   // change response, to non jason, should return 1
   // -----------------------------------------------------------------------------

   sprintf(web.reply_content,"");

   // start test server
   result=webserver(&child_pid,&port,&web,0);
   ck_assert_int_eq(result, 0);
   ck_assert_int_ne(port, 0);
   debug("result create webserver server pid %d, result %d, port=%d\n",child_pid,result,port);

   // set correct port for request
   sprintf(project_url, "http://localhost:%d/%s",port,project_path);
   cJSON_ReplaceItemInObject(env_json,"CI_PROJECT_URL"      , cJSON_CreateString(project_url));

   result_getrelease=get_release(opaque, &gitlab_release);
   ck_assert_int_ne(result_getrelease, 0);
   ck_assert_ptr_eq(gitlab_release, NULL);

   // -----------------------------------------------------------------------------
   // Simulate wrong request => should be ok if we receive json message
   // -----------------------------------------------------------------------------

   cJSON_ReplaceItemInObject(env_json, "CI_COMMIT_TAG", cJSON_CreateString("wrong"));

   result_getrelease=get_release(opaque, &gitlab_release);
   ck_assert_int_eq(result_getrelease, 0);
   ck_assert_ptr_eq(gitlab_release, NULL);
   //cJSON_Delete(gitlab_release);
   gitlab_release=NULL;

   // -----------------------------------------------------------------------------
   //simulate connection to non existing server, stop webserver!!
   // -----------------------------------------------------------------------------

   stop_test_web(child_pid);

   result_getrelease=get_release(opaque, &gitlab_release);
   ck_assert_int_ne(result_getrelease, 0);
   ck_assert_ptr_eq(gitlab_release, NULL);
   free_dynamic_trace(&trace);

}
END_TEST

Suite * gitlab_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_error");
   /* Core test case */
   tc_core = tcase_create("Core");
   //tcase_add_checked_fixture(tc_core, setup, teardown);
   tcase_add_unchecked_fixture(tc_core, NULL, NULL);
   tcase_set_timeout(tc_core,15);
   tcase_add_test(tc_core, check_gitlab_api);
   suite_add_tcase(s, tc_core);
   return s;
}


int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = gitlab_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_NORMAL);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
