/*
 test_repo.c
 Created by Danny Goossen, Gioxa Ltd on 12/6/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>
#include "../src/deployd.h"

// struct to set return value of stub functions
typedef struct test_stubs_s
{

   int get_rpm_fileinfo;
   cJSON *rpm_info;

   cJSON * JSON_dir_list_x;
} test_stubs_t;


// stubfunctions:
int get_rpm_fileinfo(void * opaque, const char * filepath, const char * filename,cJSON ** rpm_info)
{
   return 0;
}

cJSON * JSON_dir_list_x( const char *path,const char *subpath,int recursive, const char * extension)
{
   return NULL;
}

int  update_details(void * userp)
{
   return 0;
}

//TODO update, with rpm tests and symlinks

//int * match_end_points(void * opaque, cJSON* repo_info, cJSON* end_points, cJSON ** matches, cJSON ** skips, cJSON** symlinks);

START_TEST(check_match_end_points)
{

   const char rpm_info_str[]= \
   "[" \
   // fully defined arch/dist => No symlink
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"A.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/A.el7.centos.x86_64.rpm\"}, " \

   // no arch but distributio => symlink to all /rpm/el6/*
   "{ \"arch\":	\"noarch\", \"distribution\":	\"el6\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"B.el6.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/B.el6.noarch.rpm\"}," \

   // no arch no dist => symlink to /rpm/*
   "{ \"arch\":	\"noarch\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"C.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/C.rpm\"}," \

   // arch, no dist => only symlink to all /rpm/*/x86_64
   "{ \"arch\":	\"x86_64\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"D.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/D.x86_64.rpm\"}," \

   // arch, no dist / but not in endpoints => into skips
   "{ \"arch\":	\"armhfp\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"E.armhfp.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/E.armhfp.rpm\"}," \

   // arch, dist / but not in endpoints => into skips
   "{ \"arch\":	\"noarch\",  \"distribution\":	\"el8\",\"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"F.noarch.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/F.noarch.rpm\"}," \

   // fully defined arch/dist , but error => into skips
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	false, \"type\":	\"rpm\"," \
   "\"filename\":	\"G.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"error\":	\"invalid rpm\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/G.el7.centos.x86_64.rpm\"}, " \

   // fully defined rpm /dist/arch but SRPM => skips
   "{ \"arch\":	\"x86_64\", \"distribution\":	\"el7\", \"SRPM\":	true, \"type\":	\"rpm\"," \
   "\"filename\":	\"S.el7.centos.x86_64.rpm\"," \
   "\"filepath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/\"," \
   "\"fullpath\":	\"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/S.el7.centos.x86_64.rpm\"}, " \

   // debian
   // fully defined deb/dist/release and in endpoints => To maches and no symlink
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", \"release\": \"trusty\", " \
   "\"filename\":	\"A.deb\"," \
   "\"filepath\":	\"/projectdir/deb/ubuntu/trusty\"," \
   "\"fullpath\":	\"/projectdir/deb/ubuntu/trusty/A.deb\"}, " \

   //  deb/dist/* and in endpoints => To maches, symlink /deb/ubuntu/*
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", " \
   "\"filename\":	\"B.deb\"," \
   "\"filepath\":	\"projectdir/deb/ubuntu/\"," \
   "\"fullpath\":	\"projectdir/deb/ubuntu/B.deb\"}, " \

   //  deb/* and in endpoints => To maches, symlink /deb/*
   "{  \"type\":	\"deb\",  " \
   "\"filename\":	\"C.deb\"," \
   "\"filepath\":	\"projectdir/deb/\"," \
   "\"fullpath\":	\"projectdir/deb/C.deb\"}, " \

   // fully defined deb/dist/release and not in endpoints => skips
   "{  \"type\":	\"deb\", \"distribution\":	\"raspbian\", \"release\": \"buster\", " \
   "\"filename\":	\"D.deb\"," \
   "\"filepath\":	\"/projectdir/deb/raspbian/buster\"," \
   "\"fullpath\":	\"/projectdir/deb/raspbian/buster/D.deb\"}, " \

   // fully defined deb/dist/release and wrong endpoint => skips
   "{  \"type\":	\"deb\", \"distribution\":	\"ubuntu\", \"release\": \"trusty\", " \
   "\"filename\":	\"E.deb\"," \
   "\"filepath\":	\"/projectdir/deb/ubuntu/trusty/x\"," \
   "\"error\":	\"wrong subpath\"," \
   "\"subpath\":	\"/deb/ubuntu/trusty/x\"," \
   "\"fullpath\":	\"/projectdir/deb/ubuntu/trusty/x/E.deb\"} " \

   "]";

   const char endpoints_str[]= \
   "[" \
   "{ \"/rpm/el7/x86_64\":	[\"rpm\", \"el7\", \"x86_64\"] }, " \
   "{ \"/rpm/el6/x86_64\":	[\"rpm\", \"el6\", \"x86_64\"] }, " \
   "{ \"/rpm/el6/i386\":	[\"rpm\", \"el6\", \"i386\"]}, " \
   "{ \"/deb/ubuntu/trusty\":	[\"deb\", \"ubuntu\", \"trusty\"] }, " \
   "{ \"/deb/ubuntu/zesty\":	[\"deb\", \"ubuntu\", \"zesty\"] }, " \
   "{ \"/deb/debian/jessie\":	[\"deb\", \"debian\", \"jessie\"] }, " \
   "{ \"/deb/debian/wheezy\":	[\"deb\", \"debian\", \"wheezy\"] }, " \
   "{ \"/deb/debian/stretch\":	[\"deb\", \"debian\", \"stretch\"] } " \
   "]" ;


   cJSON *  rpm_info=cJSON_Parse(rpm_info_str);

   const char repos_str[]= \
   "[" \
   "{\"rpm\":	[{ \"el7\":	[ {\"amd64\": \"x86_64\"}, { \"i386\":	[\"i386\", \"i686\"]}, {\"armhfp\":	\"armv7hl\"}]}," \
   "{ \"el6\":	[ \"x86_64\", { \"i386\":	[\"i386\", \"i686\"]}]} "\
   "]}, " \
   "{\"deb\":	[{ \"debian\":	[\"wheezy\", \"jessie\", \"stretch\"]}, " \
   "{ \"ubuntu\":	[\"trusty\", \"zesty\"]} ]" \
   "} " \
   "]";

   cJSON * repos=cJSON_Parse(repos_str);
   cJSON *  end_points=NULL; //cJSON_Parse(endpoints_str);
   cJSON * trace= cJSON_CreateArray();
   char * temp=extract_dir_structure(repos,&end_points,trace);
   if (trace) cJSON_Delete(trace);
   //cJSON *  end_points=cJSON_Parse(endpoints_str);
   cJSON * matches=NULL;
   cJSON * skips=NULL;
   cJSON * symlinks=NULL;

   match_end_points( NULL, rpm_info,end_points, &matches,&skips,&symlinks);

   ck_assert_ptr_ne(matches, NULL);
   ck_assert_ptr_ne(skips, NULL);
   ck_assert_ptr_ne(symlinks, NULL);

   ck_assert_int_eq(cJSON_GetArraySize(skips), 6);
   ck_assert_int_eq(cJSON_GetArraySize(symlinks), 16);
   ck_assert_int_eq(cJSON_GetArraySize(matches), 7);


   cJSON * skip= cJSON_GetArrayItem(skips, 0);
   ck_assert_ptr_ne(skip, NULL);
   ck_assert_str_eq(cJSON_get_key(skip,"filename"), "E.armhfp.rpm");
   ck_assert_str_eq(cJSON_get_key(skip,"type"), "rpm");
   ck_assert_str_eq(cJSON_get_key(skip,"arch"), "armhfp");


   cJSON * match= cJSON_GetArrayItem(matches, 3);
   ck_assert_ptr_ne(match, NULL);
   ck_assert_str_eq(cJSON_get_key(match,"fullpath"),"/home/gitlab-runner/builds/26fdc7cb/0/deployctl/cJSON/rpm/D.x86_64.rpm");
   ck_assert_str_eq(cJSON_get_key(match,"filename"), "D.x86_64.rpm");
   ck_assert_str_eq(cJSON_get_key(match,"endpoint"), "/rpm");

   cJSON * symlink= cJSON_GetArrayItem(symlinks, 9);
   ck_assert_ptr_ne(symlink, NULL);
   ck_assert_str_eq(cJSON_get_key(symlink,"source"),"/deb/ubuntu");
   ck_assert_str_eq(cJSON_get_key(symlink,"filename"), "B.deb");
   ck_assert_str_eq(cJSON_get_key(symlink,"endpoint"), "/deb/ubuntu/trusty");


   //char * skips_prt=cJSON_Print(skips);
   //char * matches_prt=cJSON_Print(matches);
   cJSON_Delete(matches);
   cJSON_Delete(skips);
   cJSON_Delete(symlinks);

   //fprintf(stderr,"skips:\n%s\n",skips_prt);
   //fprintf(stderr,"matches:\n%s\n",matches_prt);

   //free(matches_prt);
   //free(skips_prt);
   //ck_assert_int_eq(1, 1);
}
END_TEST

//char * extract_dir_structure( cJSON* repo, cJSON** end_points, cJSON * trace_endpoints)
START_TEST(check_extract_dir_structure)
{
   const char repo_json_str[]="{\"projects\":	[\"https://gitlab.gioxa.com/deployctl/deployctl\", \"https://gitlab.gioxa.com/deployctl/cJSON\"]," \
   "\"repos\":	"\
   "[" \
   "{\"rpm\":	[{ \"el7\":	[ {\"amd64\": \"x86_64\"}, { \"i386\":	[\"i386\", \"i686\"]}, {\"armhfp\":	\"armv7hl\"}]}," \
   "{ \"el6\":	[ \"x86_64\", { \"i386\":	[\"i386\", \"i686\"]}]} "\
   "]}, " \
   "{\"deb\":	[{ \"debian\":	[\"wheezy\", \"jessie\", \"stretch\"]}, " \
   "{ \"ubuntu\":	[\"trusty\", \"zesty\"]} ]" \
   "} " \
   "]}";

   cJSON *  repo_json=cJSON_Parse(repo_json_str);
   cJSON * repos=cJSON_GetObjectItem(repo_json,"repos");
   cJSON * endpoints=NULL;
   cJSON * trace= cJSON_CreateArray();
   char * temp=extract_dir_structure(repos,&endpoints,trace);
   ck_assert_pstr_ne(temp,NULL);
   ck_assert_str_eq(temp, "{rpm/{el7/{amd64,i386,armhfp},el6/{x86_64,i386}},deb/{debian/{wheezy,jessie,stretch},ubuntu/{trusty,zesty}}}");
   char * repo_print=cJSON_Print(repos);
   ck_assert_ptr_ne(endpoints, NULL);
   cJSON_Delete(trace);
   cJSON_Delete(repo_json);
   cJSON_Delete(endpoints);
   free(temp);
   free(repo_print);

}
END_TEST

//char * create_repo_json_js (void * opaque, char * download_url, char * repo_json)
START_TEST(check_create_repo_json_js)
{

}
END_TEST

//int init_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
START_TEST(check_init_repo_dirs)
{
}
END_TEST

// int get_repo_list(void * opaque, const char *projectpath, cJSON** repo_info)
START_TEST(check_get_repo_list)
{

}
END_TEST

// cJSON * create_repo_dirs(void * opaque, char * repo_PATH , cJSON* repos)
START_TEST(check_create_repo_dirs)
{
}
END_TEST

//int update_repo_dirs(void * opaque, char * basepath, cJSON* endpoints)
START_TEST(check_update_repo_dirs)
{

}
END_TEST

// char * print_skips(cJSON * skips)
START_TEST(check_print_skips)
{
}
END_TEST

Suite * test_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_repo");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_unchecked_fixture(tc_core, NULL,NULL);
	tcase_set_timeout(tc_core,15);
	tcase_add_test(tc_core, check_match_end_points);
   tcase_add_test(tc_core, check_extract_dir_structure);
   tcase_add_test(tc_core, check_get_repo_list);
   tcase_add_test(tc_core, check_create_repo_json_js);
   tcase_add_test(tc_core, check_init_repo_dirs);
   tcase_add_test(tc_core, check_create_repo_dirs);
   tcase_add_test(tc_core, check_update_repo_dirs);

   tcase_add_test(tc_core, check_print_skips);

	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_NORMAL);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
