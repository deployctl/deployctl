/*
 test_release_html.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <stdio.h>
//LCOV_EXCL_START
#include <check.h>
//LCOV_EXCL_STOP
#include "../src/deployd.h"
#include "utils.h"


/*
 {
 "name":"0.1.4",
 "message":"release 0.1.4",
 "commit":{
 "id":"13f1259c76ac5dd93ae4e9f7929174a47d6f6d9c",
 "message":"Update.gitlab-ci.yml",
 "parent_ids":["e8ceddaf1dcbda60be6908d88bd97f8e1be1b6cf"],
 "authored_date":"2017-02-28T18:56:43.000+07:00",
 "author_name":"Danny","author_email":"danny.goossen@gioxa.com",
 "committed_date":"2017-02-28T18:56:43.000+07:00",
 "committer_name":"Danny","committer_email":"danny.goossen@gioxa.com"
 },
 "release":{"tag_name":"0.1.4","description":"# New release\r\n\r\n\r\n"}
 }
 */
cJSON * create_test_grelease(void)
{
   cJSON * g=cJSON_CreateObject();
      cJSON_AddItemToObject(g,"name" , cJSON_CreateString("0.1.4"));
      cJSON_AddItemToObject(g,"message" , cJSON_CreateString("releasee 0.1.4"));
   cJSON * commit=cJSON_CreateObject();
      cJSON_AddItemToObject(commit,"id" , cJSON_CreateString("13f1259c76ac5dd93ae4e9f7929174a47d6f6d9c"));
      cJSON_AddItemToObject(commit,"message" , cJSON_CreateString(""));
      cJSON_AddItemToObject(commit,"parent_ids" , cJSON_CreateString("e8ceddaf1dcbda60be6908d88bd97f8e1be1b6cf"));
      cJSON_AddItemToObject(commit,"authored_date" , cJSON_CreateString("2017-02-28T18:56:43.000+07:00"));
      cJSON_AddItemToObject(commit,"author_name" , cJSON_CreateString("Danny"));
      cJSON_AddItemToObject(commit,"author_email" , cJSON_CreateString("danny.goossen@gioxa.com"));

      cJSON_AddItemToObject(commit,"committed_date" , cJSON_CreateString("2017-02-28T18:56:43.000+07:00"));
      cJSON_AddItemToObject(commit,"committer_name" , cJSON_CreateString(""));
      cJSON_AddItemToObject(commit,"committer_email" , cJSON_CreateString("danny.goossen@gioxa.com"));
   cJSON * release=cJSON_CreateObject();
      cJSON_AddItemToObject(release,"tag_name" , cJSON_CreateString("0.1.4"));
      cJSON_AddItemToObject(release,"description" , cJSON_CreateString("# New release\r\n\r\n\r\n"));
   cJSON_AddItemToObject(g,"release" , release);
   return g;
}

// check_ci_extract_server_version

START_TEST(check_ci_extract_server_version)
{
	debug("Start Test check_ci_extract_server_version\n");
	int ma,mi,rv;
	ck_assert_int_eq(0,ci_extract_server_version("10.0.0", &ma, &mi, &rv));
	ck_assert_int_eq(10,ma);
	ck_assert_int_eq(0,mi);
	ck_assert_int_eq(0,rv);
}
END_TEST


START_TEST(check_slugit)
{

   debug("Start Test slugit\n");

   char * out=NULL;
   // test normal
   out=slug_it("1%3*5",0);
   ck_assert_str_eq(out,"1-3-5");
   free(out);
   // test fix len input
   out=slug_it("1%3*5",3);
   ck_assert_str_eq(out,"1-3");
   free(out);
   //test consequtive karakters
   out=slug_it("%#2-4%%%%$#7+",0);
   ck_assert_str_eq(out,"2-4-7");
   free(out);
   // test all non valid
   out=slug_it("%$#$%^&*@!",0);
   ck_assert_ptr_eq(out,NULL);
   // test empty input
   out=slug_it(NULL,0);
   ck_assert_ptr_eq(out,0);
}
END_TEST

//tcase_add_test(tc_core, check_upper_string);
START_TEST(check_upper_string)
{
   char test[]="lower";
   upper_string(test);
   ck_assert_str_eq(test, "LOWER");
   bzero(test, 5);
   upper_string(test);
   ck_assert_str_eq(test, "");
   upper_string(NULL);
}
END_TEST

START_TEST(check_is_clean_tag)
{
   // returns 1 on clean, 0 on not clean
   //clean
   ck_assert_int_eq(is_clean_tag(NULL,"ok", (const char *[]) {"BETA","TEST",NULL}),1);

   // not clean
   ck_assert_int_eq(is_clean_tag(NULL,"beta-1", (const char *[]) {"BETA","TEST",NULL}),0);
   ck_assert_int_eq(is_clean_tag(NULL,"1-BETA", (const char *[]) {"BETA","TEST",NULL}),0);
   ck_assert_int_eq(is_clean_tag(NULL,"testz3245", (const char *[]) {"BETA","TEST",NULL}),0);
   ck_assert_int_eq(is_clean_tag(NULL,"x2345TEST", (const char *[]) {"BETA","TEST",NULL}),0);

   // no list or list poits to NULL, clean
   ck_assert_int_eq(is_clean_tag(NULL,"x2345TEST", NULL),1);
   ck_assert_int_eq(is_clean_tag(NULL,"x2345TEST", (const char *[]) {NULL}),1);

   // empty tag is not clean
    ck_assert_int_eq(is_clean_tag(NULL,NULL, (const char *[]) {"BETA","TEST",NULL}),0);
}
END_TEST

START_TEST(check_validate_project_path_slug)
{
  struct trace_Struct * trace=NULL;
  init_dynamic_trace( &trace,"token","url",1);

   // OK situation
   cJSON *environment = cJSON_CreateObject();
    data_exchange_t data_exchange;
    data_exchange.env_json=environment;
   ck_assert_ptr_ne(environment,NULL);
    cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("deployctl/test_deploy_release" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("deployctl-test-deploy-release" ));
   int result=validate_project_path_slug(environment,trace,1);
   ck_assert_int_eq( result,0);
   ck_assert_int_eq(strlen(cJSON_get_key(trace->params, "trace")), 0);
   cJSON_Delete(environment);

   // wrong SLUG

 clear_dynamic_trace( trace);
   environment = cJSON_CreateObject();
    data_exchange.env_json=environment;
   ck_assert_ptr_ne(environment,NULL);
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("deployctl/test_deploy_release" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("deployctl_release" ));
   result=validate_project_path_slug(environment,trace,1);
   ck_assert_int_ne( result,0);
  ck_assert_int_gt(strlen(cJSON_get_key(trace->params, "trace")), 0);
   cJSON_Delete(environment);

   // results in empty slug
   clear_dynamic_trace( trace);
   environment = cJSON_CreateObject();
    data_exchange.env_json=environment;
   ck_assert_ptr_ne(environment,NULL);
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("*******" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("hhhhhhh" ));
   result=validate_project_path_slug(environment,trace,1);
   ck_assert_int_ne( result,0);
   ck_assert_int_gt(strlen(cJSON_get_key(trace->params, "trace")), 0);
   cJSON_Delete(environment);

   // empty projectpath to simulate problems
   clear_dynamic_trace( trace);
   environment = cJSON_CreateObject();
    data_exchange.env_json=environment;
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("hallo" ));
   result=validate_project_path_slug(environment,trace,1);
   ck_assert_int_ne( result,0);
   ck_assert_int_gt(strlen(cJSON_get_key(trace->params, "trace")), 0);
   cJSON_Delete(environment);

   // empty projectpathslug to simulate problems
   clear_dynamic_trace( trace);
   environment = cJSON_CreateObject();
    data_exchange.env_json=environment;
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("hallo" ));
   result=validate_project_path_slug(environment,trace,1);
   ck_assert_int_ne( result,0);
   ck_assert_int_gt(strlen(cJSON_get_key(trace->params, "trace")), 0);
   cJSON_Delete(environment);
   free_dynamic_trace( &trace);
}
END_TEST

// TODO change !!
START_TEST(check_read_release)
{
   char basePATH[1024];
   sprintf(basePATH,"./test_check_read_release");


   //clean
   _rmdir(basePATH,"");
   _mkdir("public",basePATH);

   cJSON * g=cJSON_CreateObject();
   cJSON_AddItemToObject(g,"name" , cJSON_CreateString("0.1.4"));
   char * release_print=cJSON_Print(g);
   debug("going to write release.json\n");
   write_test_file(basePATH,"/public", "release.json", release_print);
   free(release_print);
   cJSON_Delete(g);

   char testPATH[1024];
   cJSON * release;
   // ok, normal
   sprintf(testPATH,"%s/public/release.json",basePATH);
   release=read_release_json(NULL,testPATH);
   ck_assert_ptr_ne(release,NULL);
   cJSON_Delete(release);

   // file not exists
   sprintf(testPATH,"%s/public/notexist.json",basePATH);
   release=read_release_json(NULL,testPATH);
   ck_assert_ptr_eq(release,NULL);

   // not a jason file
   write_test_file(basePATH,"/public", "release.json","# hallo I'm not a json file");
   sprintf(testPATH,"%s/public/release.json",basePATH);
   release=read_release_json(NULL,testPATH);
   ck_assert_ptr_eq(release,NULL);
   _rmdir(basePATH,"");
}
END_TEST


START_TEST(check_create_this_release_json)
{
   // all releases file
// todo check files and links !!!!
   const char * projectdir="create_this_release_json_project_dir";

   _rmdir(projectdir,"");
   _mkdir(projectdir,"");
   //setdebug();

   debug("START TEST create_release\n");
   // setdebug();
   char filesPATH[1024];
   char basePATH[1024];
   char full_files_path[1024];
   sprintf(filesPATH,"/1-1-1/files");
   sprintf(basePATH,"test_check_create_this_release_json");
   sprintf(full_files_path,"/public%s",filesPATH);

   //clean
   _rmdir(basePATH,"");
   _mkdir(basePATH, full_files_path);
   write_test_file(basePATH, full_files_path, "file1.txt", "fileone");
   // this release info for gitlab api
   cJSON * grelease=create_test_grelease();
   ck_assert_ptr_ne(grelease,NULL);

   cJSON * environment = cJSON_CreateObject();
   data_exchange_t data_exchange;
   data_exchange.env_json=environment;
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("test/test" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("test-test"));
   cJSON_AddItemToObject(environment,"CI_PROJECT_URL" , cJSON_CreateString("https://test.com"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_REF_NAME" , cJSON_CreateString("0.1.0"));
   cJSON_AddItemToObject(environment,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("production"));
   cJSON_AddItemToObject(environment,"CI_PROJECT_NAME" , cJSON_CreateString("test"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_SHA" , cJSON_CreateString("abcdabcd"));
   cJSON_AddItemToObject(environment,"GITLAB_USER_EMAIL" , cJSON_CreateString("test@test.com"));
   cJSON_AddItemToObject(environment,"GITLAB_USER_ID" , cJSON_CreateString("1"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_TAG" , cJSON_CreateString("0.1.0"));
   cJSON_AddItemToObject(environment,"CI_PIPELINE_ID" , cJSON_CreateString("1"));
   cJSON_AddItemToObject(environment,"CI_JOB_ID" , cJSON_CreateString("1"));


   cJSON *  therelease= NULL;
   cJSON * release_tag=NULL;
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);

   ck_assert_ptr_ne( therelease, NULL);
   cJSON_Delete(therelease);

   ck_assert_ptr_ne(grelease,NULL);
   ck_assert_ptr_ne(environment,NULL);


   //missing release message
   char * grelease_str=cJSON_PrintBuffered(grelease, 1, 1);
   debug("original\n---------\n%s\n",grelease_str);
   free(grelease_str);

   cJSON_DeleteItemFromObject(grelease,"message");

   grelease_str=cJSON_PrintBuffered(grelease, 1, 1);
   debug("after delete message\n---------\n%s\n",grelease_str);
   free(grelease_str);

   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);

   // missing release description
   release_tag =cJSON_GetObjectItem(grelease,"release");
   cJSON_DeleteItemFromObject(release_tag,"description");
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);

   // missing release
   cJSON_DeleteItemFromObject(grelease,"release");
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);

   //  not a production release
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);

   // todo check above !!

   // not a production release and no environment set
   cJSON_DeleteItemFromObject(environment,"CI_ENVIRONMENT_NAME");
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);

   // no commit tag
   cJSON_DeleteItemFromObject(environment,"CI_COMMIT_TAG");
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);
   cJSON_Delete(therelease);


   // now add GIT_VERSION, same as first, should return w #
   cJSON_AddItemToObject(environment,"CI_GIT_VERSION" , cJSON_CreateString("abcdabcd"));
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);

   debug("\nJason Release \n>%s<\n",cJSON_Print(therelease));
   ck_assert_str_eq(cJSON_get_key(cJSON_GetObjectItem(therelease, "tag"), "name"),"#abcdabcd" );
   cJSON_Delete(therelease);

   // now add GIT_VERSION, not same as first, should return without #
   cJSON_ReplaceItemInObject(environment,"CI_GIT_VERSION" , cJSON_CreateString("not_same"));
   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);

   debug("\nJason Release \n>%s<\n",cJSON_Print(therelease));
   ck_assert_str_eq(cJSON_get_key(cJSON_GetObjectItem(therelease, "tag"), "name"),"not_same" );
   cJSON_Delete(therelease);


   // now add project path and a release file
   cJSON_DeleteItemFromObject(environment,"CI_GIT_VERSION" );
   cJSON_AddItemToObject(environment,"CI_PROJECT_DIR" , cJSON_CreateString(projectdir));

   write_test_file(projectdir,"",".GIT_VERSION" ,"12345678");

   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);

   debug("\nJason Release \n>%s<\n",cJSON_Print(therelease));
   ck_assert_str_eq(cJSON_get_key(cJSON_GetObjectItem(therelease, "tag"), "name"),"12345678" );
   cJSON_Delete(therelease);

   write_test_file(projectdir,"",".GIT_VERSION" ,"abcdabcd");

   therelease=create_thisrelease_json( &data_exchange, grelease, 1, basePATH,filesPATH);
   ck_assert_ptr_ne(therelease,NULL);

   debug("\nJason Release \n>%s<\n",cJSON_Print(therelease));
   ck_assert_str_eq(cJSON_get_key(cJSON_GetObjectItem(therelease, "tag"), "name"),"#abcdabcd" );
   cJSON_Delete(therelease);

   cJSON_Delete(grelease);
   cJSON_Delete(environment);
   _rmdir(projectdir,"");
   _rmdir(basePATH,"");
}
END_TEST

START_TEST(check_html_body_release)
{
   char *html= html_body_release("# h1\nhallo\n");
   ck_assert_ptr_ne(html,NULL);
   if (html) free(html);
}
END_TEST

START_TEST(check_JSON_dir_list)
{
   char filesPATH[1024];
   char basePATH[1024];
   char linkPATH[1024];
   sprintf(filesPATH,"/1-1-1/files");
   sprintf(basePATH,"./test_check_JSON_dir_list");
   _rmdir(basePATH,"");
   char full_filesPATH[1024];
   sprintf(full_filesPATH,"/public%s",filesPATH);
   _mkdir(full_filesPATH,basePATH);

   write_test_file(basePATH, full_filesPATH, "file1.txt", "file1");
   write_test_file(basePATH, full_filesPATH, "link1.url", "/link123\t 53%");
   cJSON * dirlist2=NULL;
   dirlist2 = JSON_dir_list(basePATH,filesPATH );
   ck_assert_ptr_ne(dirlist2,NULL);
   debug("dirlist2 Pointer %p\n",dirlist2);
   cJSON_IsArray(dirlist2);
   ck_assert_int_ne(cJSON_IsArray(dirlist2),0);
   ck_assert_int_eq( cJSON_GetArraySize(dirlist2),1);

   cJSON * item=cJSON_GetArrayItem(dirlist2,0);
   ck_assert_ptr_ne(item,NULL);
   ck_assert_str_eq(cJSON_get_key(item,"name"),"file1.txt");
   sprintf(linkPATH,"%s/%s",filesPATH,cJSON_get_key(item,"name"));
   ck_assert_str_eq(cJSON_get_key(item,"link"),linkPATH);
   cJSON * size_obj= cJSON_GetObjectItem(item, "size");
   ck_assert_ptr_ne(size_obj, NULL);
   ck_assert_int_eq ((int)size_obj->valuedouble,5);
   // delete results
   cJSON_Delete(dirlist2);

   // error conditions: non existing dirlist
   dirlist2=JSON_dir_list("/mynonexistingdir","" );
   ck_assert_ptr_eq(dirlist2,NULL);

   // error NULL parameters
   dirlist2=JSON_dir_list(NULL,NULL);
   ck_assert_ptr_eq(dirlist2,NULL);
   _rmdir(basePATH,"");
}
END_TEST

START_TEST(check_JSON_link_list)
{
   cJSON * dirlist2=NULL;
   char filesPATH[1024];
   char basePATH[1024];
   sprintf(filesPATH,"/1-1-1/files");
   sprintf(basePATH,"./test_check_JSON_link_list");
   _rmdir(basePATH,"");
   char full_filesPATH[1024];
   sprintf(full_filesPATH,"/public%s",filesPATH);
   _mkdir(full_filesPATH,basePATH);

   const char * data[]={"link123,53%","http://link123","https://link123","/link123\n",NULL};
   const char * links[]={"/1-1-1/files/link123","http://link123","https://link123","/1-1-1/files/link123",NULL};
   const char * metrics[]={"53%","","","",NULL};

   write_test_file(basePATH, full_filesPATH, "file1.txt","nothing");
   write_test_file(basePATH, full_filesPATH, "link_0.url", data[0]);
   write_test_file(basePATH, full_filesPATH, "link_1.url", data[1]);
   write_test_file(basePATH, full_filesPATH, "link_2.url", data[2]);
   write_test_file(basePATH, full_filesPATH, "link_3.url", data[3]);

   // Do the work
   dirlist2 = JSON_link_list(basePATH,filesPATH );

   ck_assert_ptr_ne(dirlist2,NULL);
   debug("dirlist2 Pointer %p\n",dirlist2);
   ck_assert_int_ne(cJSON_IsArray(dirlist2),0);
   ck_assert_int_eq( cJSON_GetArraySize(dirlist2),4);

   fprintf(stderr,"start json dir list test\n");
   // loop results
   cJSON * pos=NULL;
   const char * name=NULL;
   const char * link=NULL;
   const char * metric=NULL;
   int link_no=0;
   cJSON_ArrayForEach(pos,dirlist2)
   {
      name = cJSON_get_key(pos,"name");
      metric=cJSON_get_key(pos,"metric");
      link=  cJSON_get_key(pos,"link");
      ck_assert_ptr_nonnull(name);
      ck_assert_ptr_nonnull(link);
      ck_assert_int_eq(strlen(name), 6);
      fprintf(stderr,"name: >%s<\n",name);
      link_no=name[5]-'0';
      fprintf(stderr,"Links:%d, %s >%s< == >%s< , metric %s\n",link_no,name,link,links[link_no], metric);
      ck_assert_str_eq(link,links[link_no]);

      if (metrics[link_no]!=NULL && strlen(metrics[link_no])>0)
         ck_assert_str_eq(metric,metrics[link_no]);
      else
         ck_assert_ptr_eq(metric,NULL);
   }

   // delete results
   cJSON_Delete(dirlist2);


   // error conditions: non existing dirlist
   dirlist2=JSON_dir_list("/mynonexistingdir","" );
   ck_assert_ptr_eq(dirlist2,NULL);

   // error NULL parameters
   dirlist2=JSON_dir_list(NULL,NULL);
   ck_assert_ptr_eq(dirlist2,NULL);
   _rmdir(basePATH,"");
}
END_TEST



//tcase_add_test(tc_core, check_check_if_git_version_file);
START_TEST( check_check_if_git_version_file)
{
   char * test=NULL;
   const char * projectdir="./test_check_git_file_project_dir";
   const char * filename=".GIT_VERSION";

   _rmdir(projectdir,"");
   // dir does not exist, should return NULL
   test= check_if_git_version_file(projectdir);
   ck_assert_ptr_eq(test, NULL);

   // have dir but no file, should return 0
   _mkdir(projectdir,"");
   test= check_if_git_version_file(projectdir);
   ck_assert_ptr_eq(test, NULL);

   // a good value is length > 2 and < 30
   ck_assert_int_eq(write_test_file(projectdir,"",filename,"agoodvalue"),strlen("agoodvalue"));
   debug("check if value\n");
   test= check_if_git_version_file(projectdir);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;

   // should not return have \n or \a
   ck_assert_int_eq(write_test_file(projectdir,"",filename,"agoodvalue\n"),strlen("agoodvalue\n"));
    test= check_if_git_version_file(projectdir);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;

   ck_assert_int_eq(write_test_file(projectdir,"",filename,"agoodvalue\r"),strlen("agoodvalue\r"));
   test= check_if_git_version_file(projectdir);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;

   // a too short value should return NULL
   ck_assert_int_eq(write_test_file(projectdir,"",filename,"1"),strlen("1"));
   test= check_if_git_version_file(projectdir);
   ck_assert_ptr_eq(test, NULL);

   ck_assert_int_eq(write_test_file(projectdir,"",filename,"awaytooooooooooooooooooooooloooooooonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngvalueshoulreturnNULL"), \
         strlen("awaytooooooooooooooooooooooloooooooonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngvalueshoulreturnNULL"));
   test= check_if_git_version_file(projectdir);
   ck_assert_ptr_eq(test, NULL);


  _rmdir(projectdir,"");
   // strip \n

}
END_TEST

//tcase_add_test(tc_core, check_check_last_tag);
START_TEST( check_check_last_tag)
{

   const char * basePATH="test_check_last_tag";
   // clean start
   _rmdir(basePATH,"");

   char * test=NULL;

   // no correct dir should return NULL
   test=check_last_tag(NULL, basePATH);
   ck_assert_ptr_eq(test, NULL);

   _mkdir( "public",basePATH);
   // have dir, no file should return NULL
   test=check_last_tag(NULL, basePATH);
   ck_assert_ptr_eq(test, NULL);

   // a good value is length > 2 and < 30
   ck_assert_int_eq(write_test_file(basePATH, "/public","latest.tag","agoodvalue"),strlen("agoodvalue"));
   debug("check if value\n");
   test= check_last_tag(NULL, basePATH);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;

   // result should not return \n
   ck_assert_int_eq(write_test_file(basePATH, "/public","latest.tag","agoodvalue\n"),strlen("agoodvalue\n"));
   test= check_last_tag(NULL, basePATH);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;


   // result should not return have \r
   ck_assert_int_eq(write_test_file(basePATH, "/public","latest.tag","agoodvalue\r"),strlen("agoodvalue\r"));
   test= check_last_tag(NULL, basePATH);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_str_eq(test, "agoodvalue");
   free(test);
   test=NULL;


   // too short value should return NULL
     ck_assert_int_eq(write_test_file(basePATH, "/public","latest.tag","1"),strlen("1"));
   test= check_last_tag(NULL, basePATH);
   ck_assert_ptr_eq(test, NULL);

   // toooo long value should return NULL
   ck_assert_int_eq(write_test_file(basePATH, "/public","latest.tag","awaytooooooooooooooooooooooloooooooonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngvalueshoulreturnNULL"), \
                    strlen("awaytooooooooooooooooooooooloooooooonnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnngvalueshoulreturnNULL"));
   test= check_last_tag(NULL, basePATH);
   ck_assert_ptr_eq(test, NULL);

   _rmdir(basePATH,"");

}
END_TEST

// merges existing rleases with release, remove dup
START_TEST(check_create_releases_json)
{

   debug("check_create_releases_json\n");
   cJSON * environment = cJSON_CreateObject();

   data_exchange_t data_exchange;
   data_exchange.env_json=environment;
   cJSON_AddItemToObject(environment,"CI_COMMIT_REF_NAME" , cJSON_CreateString("0.1.0"));

   // minimum requirement: { "tag" : { "name": "0.0.1"}}
   cJSON * this_release = cJSON_CreateObject();
   cJSON * tag = cJSON_CreateObject();
   cJSON_AddItemToObject(tag,"name" , cJSON_CreateString("0.1.0"));
   cJSON_AddItemToObject(this_release,"tag" , tag);

   //so start empty
   char * test=NULL;
   cJSON * releases=NULL;
   debug("do the crea releases_json\n");
   test=create_releases_json(&data_exchange, this_release, &releases, 1);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_ptr_ne(releases, NULL);
   debug("\nresutl\n>%s<\n",test);
   if (test) free(test);

   // try to make dup
   test=create_releases_json(&data_exchange, this_release, &releases, 1);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_ptr_ne(releases, NULL);
   // size of array should be 1, no dups!!!!
   cJSON * arr=cJSON_GetObjectItem(releases, "releases");
   ck_assert_int_eq(1, cJSON_GetArraySize(arr));
   debug("\nresutl\n>%s<\n",test);
   if (test) free(test);
   test=NULL;

   cJSON_DeleteItemFromObject(environment,"CI_COMMIT_REF_NAME" );

   // no commit and here, delete releases

   test=create_releases_json(&data_exchange, this_release, &releases, 1);
   ck_assert_ptr_ne(test, NULL);
   ck_assert_ptr_ne(releases, NULL);

   cJSON_Delete(releases);
   cJSON_Delete(this_release);
   cJSON_Delete(environment);
}
END_TEST

//tcase_add_test(tc_core, check_create_release_json_js);
START_TEST( check_create_release_json_js)
{
   char *test=NULL;

   debug("\nSTART TEST create_release_json_js\n");

   cJSON * environment = cJSON_CreateObject();

   data_exchange_t data_exchange;
   data_exchange.env_json=environment;
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH" , cJSON_CreateString("test/test" ));
   cJSON_AddItemToObject(environment,"CI_PROJECT_PATH_SLUG" , cJSON_CreateString("test-test"));
   cJSON_AddItemToObject(environment,"CI_PROJECT_URL" , cJSON_CreateString("https://test.com"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_REF_NAME" , cJSON_CreateString("0.1.0"));
   cJSON_AddItemToObject(environment,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("production"));
   cJSON_AddItemToObject(environment,"CI_PROJECT_NAME" , cJSON_CreateString("test"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_SHA" , cJSON_CreateString("abcdabcd"));
   cJSON_AddItemToObject(environment,"GITLAB_USER_EMAIL" , cJSON_CreateString("test@test.com"));
   cJSON_AddItemToObject(environment,"GITLAB_USER_ID" , cJSON_CreateString("1"));
   cJSON_AddItemToObject(environment,"CI_COMMIT_TAG" , cJSON_CreateString("0.1.0"));
   cJSON_AddItemToObject(environment,"CI_PIPELINE_ID" , cJSON_CreateString("1"));
   cJSON_AddItemToObject(environment,"CI_JOB_ID" , cJSON_CreateString("1"));

   char test1[1024];
   sprintf(test1, "var target = \"Releases\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\n",PACKAGE_VERSION);
   test=create_release_json_js(&data_exchange, NULL, NULL, 0, NULL);
   ck_assert_ptr_ne( test, NULL);
   ck_assert_str_eq(test, test1);
   debug("\nall NULL\n>%s<\n",test);
   free(test);

   sprintf(test1, "var target = \"Releases\";\nvar download_url = \"Download\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\n",PACKAGE_VERSION);
   test=create_release_json_js(&data_exchange, "Download", NULL, 0, NULL);
   ck_assert_ptr_ne( test, NULL);
   debug("\nset download\n>%s<\n",test);
   ck_assert_str_eq(test, test1);
   free(test);

   // no tag means it's releases, so need last tag if present
   test=create_release_json_js(&data_exchange, NULL, NULL, 0, "0.1.0");
   ck_assert_ptr_ne( test, NULL);
   debug("\nset tag, no tag\n>%s<\n",test);
   sprintf(test1, "var target = \"Releases\";\nvar last_tag = \"0.1.0\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\n",PACKAGE_VERSION);
   ck_assert_str_eq(test, test1);

   free(test);

   // tag => should not display last_tag
   test=create_release_json_js(&data_exchange, NULL, NULL, 1, "0.1.0");
   ck_assert_ptr_ne( test, NULL);
   debug("\nset tag and tag\n>%s<\n",test);
   sprintf(test1, "var target = \"Release\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\n",PACKAGE_VERSION);
   ck_assert_str_eq(test, test1);

   free(test);

   test=create_release_json_js(&data_exchange, NULL, "Release", 1, NULL);
   ck_assert_ptr_ne( test, NULL);
   debug("\nset release\n>%s<\n",test);
   sprintf(test1, "var target = \"Release\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\nvar ReleaseData = Release;\n",PACKAGE_VERSION);
   ck_assert_str_eq(test, test1);

   free(test);

   cJSON_ReplaceItemInObject(environment,"CI_ENVIRONMENT_NAME" , cJSON_CreateString("review/master" ));
   test=create_release_json_js(&data_exchange, NULL, "NULL", 1, NULL);
   ck_assert_ptr_ne( test, NULL);
   debug("\nset release\n>%s<\n",test);
   sprintf(test1, "var target = \"review/master\";\nvar deployctl_version = \"%s\";\n" \
           "var project_info = {\"name\" : \"test\",\n\"path\" : \"test/test\",\n\"link\" : \"https://test.com\"};\nvar ReleaseData = NULL;\n",PACKAGE_VERSION);
   ck_assert_str_eq(test, test1);
   free(test);

   cJSON_Delete(environment);
}
END_TEST

Suite * release_html_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_release_html");
   /* Core test case */
   tc_core = tcase_create("Core");
   //tcase_add_checked_fixture(tc_core, NULL,NULL);
   tcase_add_unchecked_fixture(tc_core,NULL,NULL);
   tcase_set_timeout(tc_core,5);

   tcase_add_test(tc_core, check_ci_extract_server_version);
   tcase_add_test(tc_core, check_create_releases_json);

   tcase_add_test(tc_core, check_slugit);
   tcase_add_test(tc_core, check_validate_project_path_slug);
   tcase_add_test(tc_core, check_read_release);
   tcase_add_test(tc_core, check_html_body_release);
   tcase_add_test(tc_core, check_JSON_dir_list);
   tcase_add_test(tc_core, check_JSON_link_list);
   tcase_add_test(tc_core, check_create_this_release_json);

   tcase_add_test(tc_core, check_upper_string);
   tcase_add_test(tc_core, check_is_clean_tag);

   tcase_add_test(tc_core, check_create_release_json_js);

   tcase_add_test(tc_core, check_check_if_git_version_file);
   tcase_add_test(tc_core, check_check_last_tag);

   suite_add_tcase(s, tc_core);
   return s;
}

int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;
   s = release_html_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE );
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
