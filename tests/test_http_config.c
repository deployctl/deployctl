/*
 test_http_config.c
 Created by Danny Goossen, Gioxa Ltd on 9/4/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#include <check.h>
#include "../src/deployd.h"


typedef struct test_stubs_s
{
   int url_verify;
   int verify_http_config;
   int reload_http_config;
   int letsencrypt;
} test_stubs_t;

// stubfunctions for http_config:
int url_verify(char * commit_sha,char * baseHREF,struct trace_Struct * trace)
{
   ;
   // answer according feedback->tests
   return( ((test_stubs_t *)trace->tests)->url_verify);
}

int  update_details(void * userp)
{
   return 0;
}
/*
To test:

int url_check(void * opaque, char * basePATH,char * baseHREF);
int delete_http_config(void * opaque,char * basePATH);
int write_http_config(void * opaque, int is_https,char * basePATH, char * server_name);

int check_namespace(void * opaque,char * filepath);
int write_namespace(void * opaque, char * filepath );
*/


//int url_check(void * opaque, char * basePATH,char * baseHREF);
START_TEST(check_url_check )
{
   cJSON * env_json=  cJSON_CreateObject();
    ck_assert_ptr_ne(env_json,NULL);
    parameter_t parameters;
   data_exchange_t data_exchange;
    void * opaque=&data_exchange;
    char * newarg[4];


    data_exchange.needenvp=0;
    data_exchange.gid=getgid();
    data_exchange.uid=geteuid() ;
    data_exchange.env_json=env_json;
    data_exchange.paramlist=(char **)newarg;
    data_exchange.timeout=1;
   parameters.timeout=data_exchange.timeout;

   data_exchange.parameters=&parameters;
   test_stubs_t tests;
   int temp=0;
   struct trace_Struct * trace=NULL;
   init_dynamic_trace(&trace,"","",1);
  data_exchange.trace=trace;
    data_exchange.trace->tests=&tests;
  int exitcode=0;

   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char basePATH[1024];
   sprintf(basePATH,"%s/testurl_check",cwd);
   //parameters.prefix=cwd;
   cJSON_AddItemToObject(env_json,"CI_COMMIT_SHA" , cJSON_CreateString("CI_COMMIT_SHA_data" ));
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char this_command[256];
   data_exchange.this_command=this_command;
   sprintf(this_command,"rmdir");

   // OK
   tests.url_verify=0;

   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   char pubpath[1024];
    sprintf(pubpath,"%s/public",basePATH);
   newarg[0]="/bin/mkdir";
   newarg[1]="-p";
   newarg[2]=(char *)pubpath;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   exitcode=url_check(opaque, basePATH,"mydomain.com");
   ck_assert_int_eq(exitcode,0);

   // can't write file
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   exitcode=url_check(opaque, basePATH,"mydomain.com");
   ck_assert_int_ne(exitcode,0);

   // url_verify fail
   exitcode=0;
   tests.url_verify=1;
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   newarg[0]="/bin/mkdir";
   newarg[1]="-p";
   newarg[2]=(char *)pubpath;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   exitcode=url_check(opaque, basePATH,"mydomain.com");
   ck_assert_int_ne(exitcode,0);
    // cleanup
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);
   cJSON_Delete(env_json);
   free_dynamic_trace(&trace);
}
END_TEST

//int write_http_config(void * opaque, int is_https,char * basePATH, char * server_name);
//int delete_http_config(void * opaque,char * basePATH);
START_TEST(check_http_config_op)
{
   parameter_t parameters;
   data_exchange_t data_exchange;

   void * opaque=&data_exchange;
   char output_buf[0x10000];
   int exitcode=0;
   char * newarg[4];
   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;
   data_exchange.env_json=NULL;
   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   parameters.timeout=data_exchange.timeout;
   data_exchange.parameters=&parameters;
  
   struct trace_Struct * trace=NULL;
   init_dynamic_trace(&trace,"","",1);
   data_exchange.trace=trace;
   int temp=0;
   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char basePATH[1024];
   sprintf(basePATH,"%s/http_config_op",cwd);
   //parameters.prefix=cwd;
   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char this_command[256];
   data_exchange.this_command=this_command;
   sprintf(this_command,"rmdir");
   char filepath[1024];
   sprintf(filepath,"%s/server.conf",basePATH);
   // OK
   // http OK
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   newarg[0]="/bin/mkdir";
   newarg[1]="-p";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   exitcode=write_http_config(opaque, 0,basePATH, "mydomain.com");
   ck_assert_int_eq(exitcode,0);
   ck_assert_int_ne(access( filepath, F_OK ),-1);
   // TODO diff with testfiles/server.conf.mydomain.com.http

   // https OK, with already existing http config
   exitcode=write_http_config(opaque, 1,basePATH, "mydomain.com");
   ck_assert_int_eq(exitcode,0);
   ck_assert_int_ne(access( filepath, F_OK ),-1);
   // TODO diff with testfiles/server.conf.mydomain.com.https

   // delete config_file
   exitcode=delete_http_config(opaque,basePATH);
   ck_assert_int_eq(exitcode,0);
    ck_assert_int_eq(access( filepath, F_OK ),-1);

   // can't write file
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);
    // base path does not exists
   exitcode=write_http_config(opaque, 0,basePATH, "mydomain.com");
   ck_assert_int_ne(exitcode,0);
   ck_assert_int_eq(access( filepath, F_OK ),-1);
   free_dynamic_trace(&trace);
}
END_TEST

//int write_namespace(void * opaque, char * filepath );
//int check_namespace(void * opaque,char * filepath);
START_TEST(check_namespace_op)
{
   //ck_assert_ptr_ne(env_json,NULL);
   parameter_t parameters;
   data_exchange_t data_exchange;
   void * opaque=&data_exchange;
   int exitcode=0;
   char * newarg[4];
   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;
   data_exchange.env_json=cJSON_CreateObject();
   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   parameters.timeout=data_exchange.timeout;
   data_exchange.parameters=&parameters;
   struct trace_Struct * trace=NULL;
   init_dynamic_trace(&trace,"","",1);
   data_exchange.trace=trace;
   int temp=0;

   char cwd[1024];
   ck_assert_ptr_ne(getcwd(cwd, sizeof(cwd)),NULL);
   char basePATH[1024];
   sprintf(basePATH,"%s/namespace_check",cwd);
   //parameters.prefix=cwd;
   ck_assert_ptr_ne(data_exchange.env_json,NULL);
   cJSON_AddItemToObject(data_exchange.env_json,"CI_PROJECT_URL" , cJSON_CreateString("CI_PROJECT_URL_data" ));

   ((data_exchange_t *)opaque)->paramlist=(char **)newarg;
   char this_command[256];
   data_exchange.this_command=this_command;
   sprintf(this_command,"rmdir");
   char filepath[1024];
   sprintf(filepath,"%s/.namespace",basePATH);

   //OK
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   newarg[0]="/bin/mkdir";
   newarg[1]="-p";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);

   // if file does not exists: ok
   exitcode=check_namespace(opaque, basePATH);
   //printf("%s\n",output_buf);
   ck_assert_int_eq(exitcode,0);
   //reset output_buf & exitcode
        clear_dynamic_trace(trace);
   exitcode=0;
   // write name space, existing dir and no file
   exitcode=write_namespace(opaque, basePATH);
   ck_assert_int_eq(exitcode,0);
   //printf("%s\n",output_buf);
   ck_assert_int_ne(access( filepath, F_OK ),-1);
   //reset output_buf & exitcode
        clear_dynamic_trace(trace);
   exitcode=0;
   // if file exists and content same as CI_PROJECT_URL=> ok
   exitcode=check_namespace(opaque, basePATH);
   // printf("%s\n",output_buf);
   ck_assert_int_eq(exitcode,0);
   //reset output_buf & exitcode
       clear_dynamic_trace(trace);
   exitcode=0;
   cJSON_Delete(data_exchange.env_json);
   data_exchange.env_json=cJSON_CreateObject();
   cJSON_AddItemToObject(data_exchange.env_json,"CI_PROJECT_URL" , cJSON_CreateString("CI_PROJECT_URL_data_wrong" ));

   // if file exists and content not same as CI_PROJECT_URL=> NOK
   exitcode=check_namespace(opaque, basePATH);
   //printf("%s\n",output_buf);
   ck_assert_int_ne(exitcode,0);
   //reset output_buf & exitcode
        clear_dynamic_trace(trace);
   exitcode=0;

   // can't write file
   newarg[0]="/bin/rm";
   newarg[1]="-rf";
   newarg[2]=(char *)basePATH;
   newarg[3]=NULL;
   temp=cmd_exec(opaque);
   exitcode=check_namespace(opaque, basePATH);
   //printf("%s\n",output_buf);
   ck_assert_int_eq(exitcode,0);
   exitcode=0;
      clear_dynamic_trace(trace);
   // dir does not exists should be OK, not for name space to check
   exitcode=check_namespace(opaque, basePATH);
   //printf("%s\n",output_buf);
   ck_assert_int_eq(exitcode,0);
   //reset output_buf & exitcode
    clear_dynamic_trace(trace);
   exitcode=0;

   cJSON_Delete(data_exchange.env_json);
   free_dynamic_trace(&trace);
}
END_TEST

Suite * http_config_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_http_config");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, NULL,NULL);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,2);
   tcase_add_test(tc_core, check_url_check);
   tcase_add_test(tc_core, check_http_config_op);
   tcase_add_test(tc_core, check_namespace_op);

   suite_add_tcase(s, tc_core);
   return s;
}

int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = http_config_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE );
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
