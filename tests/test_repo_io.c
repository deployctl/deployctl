/*
 test_repo.c
 Created by Danny Goossen, Gioxa Ltd on 12/6/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>

#include "../src/deployd.h"
#include "utils.h"

//TODO update

//cJSON * JSON_dir_list_x( const char *path,const char *subpath,int recursive, const char * extension);

START_TEST(check_JSON_dir_list_x)
{

}
END_TEST

START_TEST(check_read_json)
{
   char basePATH[1024];
   sprintf(basePATH,"./test_check_read_cjson");


   //clean
   _rmdir(basePATH,"");
   _mkdir("/public",basePATH);

   cJSON * g=cJSON_CreateObject();
   cJSON_AddItemToObject(g,"name" , cJSON_CreateString("0.1.4"));
   char * release_print=cJSON_Print(g);
   debug("going to write release.json\n");
   write_test_file(basePATH,"/public", "release.json", release_print);
   free(release_print);
   cJSON_Delete(g);

   cJSON * release;
   // ok, normal

   // cJSON * read_json(void * opaque, const char * base_path,const char * sub_path,const char * filename)

   release=read_json(NULL,basePATH,"/public/","release.json");
   ck_assert_ptr_ne(release,NULL);
   cJSON_Delete(release);

   // safety checks:

   release=read_json(NULL,basePATH,"/public/",NULL);
   ck_assert_ptr_eq(release,NULL);

   release=read_json(NULL,basePATH,"/public/","");
   ck_assert_ptr_eq(release,NULL);

   release=read_json(NULL,basePATH,NULL,"release.json");
   ck_assert_ptr_eq(release,NULL);


   release=read_json(NULL,NULL,"/public/","release.json");
   ck_assert_ptr_eq(release,NULL);


   // file not exists
   release=read_json(NULL,basePATH,"/public/","notexist.json");
   ck_assert_ptr_eq(release,NULL);

   // not a jason file
   write_test_file(basePATH,"/public", "release.json","# hallo I'm not a json file");
   release=read_json(NULL,basePATH,"/public/","release.json");
   ck_assert_ptr_eq(release,NULL);
   _rmdir(basePATH,"");
}
END_TEST


Suite * test_suite(void)
{
	Suite *s;
	TCase *tc_core;
	//TCase *tc_progress;
	s = suite_create("test_repo_io");
	/* Core test case */
	tc_core = tcase_create("Core");
	//tcase_add_checked_fixture(tc_core, setup, teardown);
	tcase_add_unchecked_fixture(tc_core, NULL,NULL);
	tcase_set_timeout(tc_core,15);

   tcase_add_test(tc_core, check_read_json);
   tcase_add_test(tc_core, check_JSON_dir_list_x);


	suite_add_tcase(s, tc_core);
	return s;
}


int main(void)
{
	int number_failed;
	Suite *s;
	SRunner *sr;

	s = test_suite();
	sr = srunner_create(s);
	srunner_run_all(sr, CK_VERBOSE);
	number_failed = srunner_ntests_failed(sr);
	srunner_free(sr);
	return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
