/*
 test_dyn_buffer.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>

#include "../src/deployd.h"

struct MemoryStruct mem;

static void setup(void)
{
   init_dynamicbuf( &mem);
}

static void teardown(void)
{
   free(mem.memory);
}

START_TEST(check_WriteMemoryCallback)
{
    printf("check_WriteMemoryCallback\n");
   size_t res=0;
   res=WriteMemoryCallback("0123456789", 10, 1,&mem);
   ck_assert_int_eq(res, 10);
   ck_assert_ptr_ne(mem.memory, NULL);
   ck_assert_int_eq(mem.size, 10);
   ck_assert_str_eq("0123456789", mem.memory);
   res=WriteMemoryCallback("0123456789", 3, 2,&mem);
   ck_assert_int_eq(res, 6);
   ck_assert_int_eq(mem.size, 16);
   ck_assert_str_eq("0123456789012345", mem.memory);
}
END_TEST

START_TEST(check_Writedynamicbuf)
{
   printf("check_Writedynamicbuf\n");
   size_t res=0;
   res=Writedynamicbuf("0123456789", &mem);
   ck_assert_int_eq(res, 10);
   ck_assert_ptr_ne(mem.memory, NULL);
   ck_assert_int_eq(mem.size, 10);
   ck_assert_str_eq("0123456789", mem.memory);
   res=Writedynamicbuf("01234", &mem);
   ck_assert_int_eq(res, 5);
   ck_assert_int_eq(mem.size, 15);
   ck_assert_str_eq("012345678901234", mem.memory);
}
END_TEST

START_TEST(check_Writedynamicbuf_n)
{
   printf("check_Writedynamicbuf_n");
   size_t res=0;
   res=Writedynamicbuf_n("0123456789", 10, &mem);
   ck_assert_int_eq(res, 10);
   ck_assert_ptr_ne(mem.memory, NULL);
   ck_assert_int_eq(mem.size, 10);
   ck_assert_str_eq("0123456789", mem.memory);
   res=Writedynamicbuf_n("0123456789", 5, &mem);
   ck_assert_int_eq(res, 5);
   ck_assert_int_eq(mem.size, 15);
   ck_assert_str_eq("012345678901234", mem.memory);
}
END_TEST

START_TEST(check_init_dynamicbuf)
{
   printf("check_init_dynamicbuf\n");
   ck_assert_ptr_ne(mem.memory, NULL);
   ck_assert_int_eq(mem.size, 0);
   ck_assert_int_eq( mem.memory[0],0);
}
END_TEST

Suite * dyn_buffer_suite(void)
{

   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_dyn_buffer");
   /* Core test case */
   tc_core = tcase_create("Core");
   //tcase_add_checked_fixture(tc_core, setup, teardown);
   tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,15);
   tcase_add_test(tc_core, check_init_dynamicbuf);
   tcase_add_test(tc_core, check_Writedynamicbuf);
   tcase_add_test(tc_core, check_Writedynamicbuf_n);
   tcase_add_test(tc_core, check_WriteMemoryCallback);
   suite_add_tcase(s, tc_core);
   return s;
}


int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = dyn_buffer_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
