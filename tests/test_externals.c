/*
 test_externals.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */


#include <check.h>

#include "../src/deployd.h"
#include "test_web_api.h"

/*
int url_verify(char * commit_sha,char * baseHREF,feedback_t * feedback);
int verify_http_config(void * opaque);
int reload_http_config(void * opaque);

*/
int cmd_exec(void * opaque);
int cmd_write(void * opaque);

typedef struct test_stubs_s
{
   char * arg0;
   char * arg1;
   char * arg2;
   char * arg3;
   int exitcode;
} test_stubs_t;


// stubfunctions:
int cmd_exec(void * opaque)
{
   test_stubs_t * teststubs= (test_stubs_t *)((data_exchange_t *)opaque)->trace->tests;

   ck_assert_str_eq(teststubs->arg0, ((data_exchange_t *)opaque)->paramlist[0]);
   ck_assert_str_eq(teststubs->arg1, ((data_exchange_t *)opaque)->paramlist[1]);
   ck_assert_str_eq(teststubs->arg2, ((data_exchange_t *)opaque)->paramlist[2]);
   return(teststubs->exitcode);
}

//WIP
// idea is to do the url_verify
// start webserver, give it what to anser and on which port for what url
// call url_verify("1234567890","http://127.0.0.1:12345",feedback_t * feedback)
// stop webserver


START_TEST(check_url_verify)
{
      u_int16_t port=0;
      pid_t child_pid;

      int result=0;
      webserver_t web;

      // what content we want the api to serve if he got the request matching request
   char * commit_sha="12345678";
   sprintf(web.reply_content,"%s",commit_sha);

      // what response headers we want to sent back, in case request matches
      sprintf(web.reply_header,"%s","HTTP/1.1 200 OK\r\nServer: testweb/0.0.1\r\nContent-Length:%d\r\n\r\n%s");


      // what reply on fail
      const char * m404_mesg="{\"message\":\"404 NOT FOUND\"}";
      sprintf(web.reply_fail,"HTTP/1.1 404 NOT FOUND\r\nServer: testweb/0.0.1\r\nContent-Length:%lu\r\n\r\n%s",strlen(m404_mesg),m404_mesg);

      // what we expect the api to get as request as condition not to reply fail
      sprintf(web.request,"GET /.commit_sha.txt HTTP/1.1\r\n" \
              "Host: localhost:%%d\r\n" \
              "Accept: */*\r\n" \
              "\r\n" );

      // start test server
      result=webserver(&child_pid,&port,&web,0);
      ck_assert_int_eq(result, 0);
      ck_assert_int_ne(port, 0);
      debug("result create webserver server: %d, port=%d\n",result,port);

      // prepare environment for command:


   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
      char baseHREF[1024];

      sprintf(baseHREF, "http://localhost:%d",port);


   int result_test=0;
   	// -----------------------------------------------------------------------------
      // Test one, all conditions ok, should return 0 and have a valid gitlab_release
      // -----------------------------------------------------------------------------
   result_test=url_verify( commit_sha,baseHREF,trace);
   	ck_assert_int_eq(result_test, 0);

      // stop
      stop_test_web(child_pid);

      // -----------------------------------------------------------------------------
      // change to non valid response
      // -----------------------------------------------------------------------------

      sprintf(web.reply_content,"");

      // start test server
      result=webserver(&child_pid,&port,&web,0);
      ck_assert_int_eq(result, 0);
      ck_assert_int_ne(port, 0);
      debug("result create webserver server: %d, port=%d\n",result,port);

      // set correct port for request
      sprintf(baseHREF, "http://localhost:%d",port);
   result_test=url_verify( commit_sha,baseHREF,trace);
   ck_assert_int_ne(result_test, 0);

      // -----------------------------------------------------------------------------
      //simulate connection to non existing server, stop webserver!!
      // -----------------------------------------------------------------------------
    stop_test_web(child_pid);
   result_test=url_verify( commit_sha,baseHREF,trace);
   ck_assert_int_ne(result_test, 0);

   free_dynamic_trace(&trace);

}
END_TEST

START_TEST(check_verify_http_config)
{

   data_exchange_t data_exchange;
   parameter_t parameters;
   void * opaque=&data_exchange;
   char * newarg[4];

  
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;

   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests
   test_stubs.exitcode=0;
   test_stubs.arg0="/bin/sh";
   test_stubs.arg1="-c";
   test_stubs.arg2="sudo /usr/sbin/nginx -t -q";
   int result=verify_http_config(opaque);

   ck_assert_int_eq(result, 0);
   free_dynamic_trace(&trace);
}
END_TEST
START_TEST(check_reload_http_config)
{

   data_exchange_t data_exchange;
   parameter_t parameters;
   void * opaque=&data_exchange;

   char * newarg[4];
   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);
   data_exchange.trace=trace;

   data_exchange.needenvp=0;
   data_exchange.gid=getgid();
   data_exchange.uid=geteuid() ;

   cJSON * env_json=  cJSON_CreateObject();
   ck_assert_ptr_ne(env_json,NULL);
   data_exchange.env_json=env_json;

   data_exchange.paramlist=(char **)newarg;
   data_exchange.timeout=1;
   data_exchange.parameters=&parameters;
   test_stubs_t test_stubs;
   trace->tests=&test_stubs;
   // set results for tests
   test_stubs.exitcode=0;
   test_stubs.arg0="/bin/sh";
   test_stubs.arg1="-c";
   test_stubs.arg2="sudo /usr/sbin/nginx -s reload";
   test_stubs.arg3=NULL;
   int result=reload_http_config(opaque);

   ck_assert_int_eq(result, 0);
   free_dynamic_trace(&trace);
}
END_TEST




/*----------------------------------------------------------------------------------
 * get basepath of repo by url of repo
 * returns 0 on success and basepath
 *-----------------------------------------------------------------------------------*/
/* on curl repo.json, it returns=>
{ "projects":	["https://gitlab.gioxa.com/deployctl/deployctl", "https://gitlab.gioxa.com/deployctl/cJSON"]
   "repos":	[{
             "rpm":	[{
                      "el7":	["x86_64", "x86_64"]
                      }]
             }],
   "endpoints":	[{
                   "/rpm/el7/x86_64":	["rpm", "el7", "x86_64"]
                   }],
   "base_path":	"/opt/deploy/domain/repo.deployctl.com",
   "base_href":	"http://repo.deployctl.com"
}
*/

//int url_repo_base_path  (char ** basepath,char * baseHREF,feedback_t * feedback)
START_TEST(check_url_repo_base_path)
{
   u_int16_t port=0;
   pid_t child_pid;

   int result=0;
   webserver_t web;

   // what content we want the api to serve if he got the request matching request
   const char * base_path_in="/test/url/basepath";

   // create cJSON reply, we only need to extract base_path
   cJSON * reply_json=cJSON_CreateObject();
   cJSON_AddStringToObject(reply_json, "base_path",base_path_in );
   char * reply_str=cJSON_Print(reply_json);
   ck_assert_ptr_ne(reply_str, NULL);
   sprintf(web.reply_content,"%s",reply_str);
   free(reply_str);
   reply_str=NULL;
   cJSON_Delete(reply_json);
   reply_json=NULL;

   // what response headers we want to sent back, in case request matches
   sprintf(web.reply_header,"%s","HTTP/1.1 200 OK\r\nServer: testweb/0.0.1\r\nContent-Length:%d\r\n\r\n%s");

   // what reply on fail
   const char * m404_mesg="{\"message\":\"404 NOT FOUND\"}";
   sprintf(web.reply_fail,"HTTP/1.1 404 NOT FOUND\r\nServer: testweb/0.0.1\r\nContent-Length:%lu\r\n\r\n%s",strlen(m404_mesg),m404_mesg);

   // what we expect the api to get as request as condition not to reply fail
   sprintf(web.request,"GET /repo.json HTTP/1.1\r\n" \
           "Host: localhost:%%d\r\n" \
           "Accept: */*\r\n" \
           "\r\n" );

   // start test server
   result=webserver(&child_pid,&port,&web,0);
   ck_assert_int_eq(result, 0);
   ck_assert_int_ne(port, 0);
   debug("result create webserver server: %d, port=%d\n",result,port);

   // prepare environment for command:

   struct trace_Struct * trace=NULL;
   init_dynamic_trace( &trace,"token","url",1);

   char baseHREF[1024];

   sprintf(baseHREF, "http://localhost:%d",port);


   int result_test=0;
   char * basepath=NULL;
   // -----------------------------------------------------------------------------
   // Test one, all conditions ok, should return 0 and have a valid gitlab_release
   // -----------------------------------------------------------------------------
   result_test= url_repo_base_path  ( &basepath,baseHREF,trace);
   ck_assert_int_eq(result_test, 0);
   ck_assert_str_eq(basepath, base_path_in);
   free(basepath);

   // stop
   stop_test_web(child_pid);

   // -----------------------------------------------------------------------------
   // change to non valid response
   // -----------------------------------------------------------------------------

   sprintf(web.reply_content,"");

   // start test server
   result=webserver(&child_pid,&port,&web,0);
   ck_assert_int_eq(result, 0);
   ck_assert_int_ne(port, 0);
   debug("result create webserver server: %d, port=%d\n",result,port);

   // set correct port for request
   sprintf(baseHREF, "http://localhost:%d",port);

    result_test= url_repo_base_path  ( &basepath,baseHREF,trace);
   ck_assert_int_ne(result_test, 0);

   // -----------------------------------------------------------------------------
   //simulate connection to non existing server, stop webserver!!
   // -----------------------------------------------------------------------------
   stop_test_web(child_pid);
   result_test= url_repo_base_path  ( &basepath,baseHREF,trace);
   ck_assert_int_ne(result_test, 0);
   free_dynamic_trace(&trace);
}
END_TEST


Suite * externals_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_externals");
   /* Core test case */
   tc_core = tcase_create("Core");
   tcase_add_checked_fixture(tc_core, NULL,NULL);
   //tcase_add_unchecked_fixture(tc_core, setup, teardown);
   tcase_set_timeout(tc_core,65);
   tcase_add_test(tc_core, check_url_verify);
	tcase_add_test(tc_core, check_verify_http_config);
   tcase_add_test(tc_core, check_reload_http_config);
   tcase_add_test(tc_core, check_url_repo_base_path);

   suite_add_tcase(s, tc_core);
   return s;
}

int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = externals_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_VERBOSE );
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
