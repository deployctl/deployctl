/*
 test_error.c
 Created by Danny Goossen, Gioxa Ltd on 26/3/17.

 MIT License

 Copyright (c) 2017 deployctl, Gioxa Ltd.

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

 */

#include <check.h>
#include "../src/deployd.h"


START_TEST(check_normal)
{
  ck_assert_int_eq(getsyslog(),0);
  ck_assert_int_eq(getdebug(),0);
  ck_assert_int_eq(getverbose(),0);
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  setsyslog();
  ck_assert_int_eq(getsyslog(),1);
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  clrsyslog();
}
END_TEST
START_TEST(check_verbose)
{
  setverbose();
  ck_assert_int_eq(getverbose(),1);
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  setsyslog();
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  clrsyslog();
  clrverbose();
  ck_assert_int_eq(getverbose(),0);
}
END_TEST

START_TEST(check_debug)
{
  setdebug();
  ck_assert_int_eq(getdebug(),1);
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  setsyslog();
  info("test1");
  debug("test2");
  alert("test3");
  error("test4");
  sock_error("test4");
  sock_error_no("test5",1);
  clrsyslog();
  clrdebug();
  ck_assert_int_eq(getdebug(),0);
}
END_TEST

Suite * socket_suite(void)
{
   Suite *s;
   TCase *tc_core;
   //TCase *tc_progress;
   s = suite_create("test_error");
   /* Core test case */
   tc_core = tcase_create("Core");
   //tcase_add_checked_fixture(tc_core, setup, teardown);
   tcase_add_unchecked_fixture(tc_core, NULL,NULL);
   tcase_set_timeout(tc_core,15);
   tcase_add_test(tc_core, check_normal);
   tcase_add_test(tc_core, check_verbose);
  tcase_add_test(tc_core, check_debug);
   suite_add_tcase(s, tc_core);
   return s;
}


int main(void)
{
   int number_failed;
   Suite *s;
   SRunner *sr;

   s = socket_suite();
   sr = srunner_create(s);
   srunner_run_all(sr, CK_NORMAL);
   number_failed = srunner_ntests_failed(sr);
   srunner_free(sr);
   return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
